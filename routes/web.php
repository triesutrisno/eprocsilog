<?php

use Illuminate\Support\Facades\Route;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::namespace('layouts')->group(function () {
    Route::resource('/', 'MainController');
});

Route::namespace('login')->group(function () {
    Route::get('/login', 'LoginController@index')->name('login');
    Route::post('/login', 'LoginController@login');
});

Route::namespace('keluar')->group(function () {
    Route::get('/keluar', 'KeluarController@index');
});

Route::namespace('kebijakan')->group(function () {
    Route::resource('/kebijakan', 'KebijakanController');
});

Route::namespace('register')->group(function () {
    Route::resource('/register', 'RegisterController');
    Route::post('/register', 'RegisterController@store');
    Route::post('/kotaperusahaan', 'RegisterController@kabupaten');
    Route::post('/kecamatanperusahaan', 'RegisterController@kecamatan');
    //Route::resource('/register/simpan','RegisterController@store');
    Route::get('/active/{email}', 'RegisterController@active');
});

Route::namespace('faq')->group(function () {
    Route::resource('/faq', 'FaqController');
});

Route::group(['middleware' => ['checkUser']], function () {

    Route::get('/berita-acara', 'BeritaAcaraController@index');
    Route::get('/berita-acara-getList', 'BeritaAcaraController@getList')->name('berita_acara.getList');
    Route::get('/berita-acara-show/{id}', 'BeritaAcaraController@show')->name('berita_acara.show');
    Route::get('/berita-acara-print/{id}', 'BeritaAcaraController@print')->name('berita_acara.print');
    Route::get('/berita-acara-excel-spj/{id}', 'BeritaAcaraController@excelSPJ')->name('berita_acara.excelSpj');
    Route::get('/berita-acara-create', 'BeritaAcaraController@create')->name('berita_acara.create');
    Route::post('/berita-acara-store', 'BeritaAcaraController@store')->name('berita_acara.store');
    Route::post('/berita-acara-delete', 'BeritaAcaraController@delete')->name('berita_acara.delete');
    Route::post('/berita-acara-approve', 'BeritaAcaraController@approve')->name('berita_acara.approve');
    Route::get('/berita-acara-reject/{id}', 'BeritaAcaraController@reject')->name('berita_acara.reject');
    Route::get('/berita-acara-delete/{id}', 'BeritaAcaraController@delete')->name('berita_acara.delete');
    Route::post('/berita-acara-generate-no-pp', 'BeritaAcaraController@generateNoPP')->name('berita_acara.generateNoPP');
    Route::post('/berita-acara-set-inputted-sap', 'BeritaAcaraController@setInputtedSAP')->name('berita_acara.setInputtedSAP');
    Route::post('/berita-acara-generate-surat-klaim', 'BeritaAcaraController@generateSuratKlaim')->name('berita_acara.generateSuratKlaim');
    Route::get('/berita-acara-print-surat-klaim/{id}', 'BeritaAcaraController@printSuratKlaim')->name('berita_acara.printSuratKlaim');
    Route::get('/berita-acara-approve-surat-klaim/{id}', 'BeritaAcaraController@approveSuratKlaim')->name('berita_acara.approveSuratKlaim');
    Route::get('/berita-acara-reject-surat-klaim/{id}', 'BeritaAcaraController@rejectSuratKlaim')->name('berita_acara.rejectSuratKlaim');

    Route::get('/invoice-monitor', 'InvoiceMonitorController@index');
    Route::get('/invoice-monitor-getList', 'InvoiceMonitorController@getList')->name('invoice_monitor.getList');


    Route::get('/berita-acara-sbi', 'BeritaAcaraSBIController@index');
    Route::get('/berita-acara-sbi-getList', 'BeritaAcaraSBIController@getList')->name('berita_acara_sbi.getList');
    Route::get('/berita-acara-sbi-show/{id}', 'BeritaAcaraSBIController@show')->name('berita_acara_sbi.show');
    Route::get('/berita-acara-sbi-print/{id}', 'BeritaAcaraSBIController@print')->name('berita_acara_sbi.print');
    Route::get('/berita-acara-sbi-excel-spj/{id}', 'BeritaAcaraSBIController@excelSPJ')->name('berita_acara_sbi.excelSpj');
    Route::get('/berita-acara-sbi-create', 'BeritaAcaraSBIController@create')->name('berita_acara_sbi.create');
    Route::post('/berita-acara-sbi-store', 'BeritaAcaraSBIController@store')->name('berita_acara_sbi.store');
    Route::post('/berita-acara-sbi-delete', 'BeritaAcaraSBIController@delete')->name('berita_acara_sbi.delete');
    Route::post('/berita-acara-sbi-approve', 'BeritaAcaraSBIController@approve')->name('berita_acara_sbi.approve');
    Route::get('/berita-acara-sbi-reject/{id}', 'BeritaAcaraSBIController@reject')->name('berita_acara_sbi.reject');
    Route::get('/berita-acara-sbi-delete/{id}', 'BeritaAcaraSBIController@delete')->name('berita_acara_sbi.delete');
    Route::post('/berita-acara-sbi-generate-no-pp', 'BeritaAcaraSBIController@generateNoPP')->name('berita_acara_sbi.generateNoPP');
    Route::post('/berita-acara-sbi-set-inputted-sap', 'BeritaAcaraSBIController@setInputtedSAP')->name('berita_acara_sbi.setInputtedSAP');
    Route::post('/berita-acara-sbi-generate-surat-klaim', 'BeritaAcaraSBIController@generateSuratKlaim')->name('berita_acara_sbi.generateSuratKlaim');
    Route::get('/berita-acara-sbi-print-surat-klaim/{id}', 'BeritaAcaraSBIController@printSuratKlaim')->name('berita_acara_sbi.printSuratKlaim');
    Route::get('/berita-acara-sbi-approve-surat-klaim/{id}', 'BeritaAcaraSBIController@approveSuratKlaim')->name('berita_acara_sbi.approveSuratKlaim');
    Route::get('/berita-acara-sbi-reject-surat-klaim/{id}', 'BeritaAcaraSBIController@rejectSuratKlaim')->name('berita_acara_sbi.rejectSuratKlaim');

    
    Route::get('/berita-acara-sp-zak', 'BeritaAcaraSPZakController@index');
    Route::get('/berita-acara-sp-zak-getList', 'BeritaAcaraSPZakController@getList')->name('berita_acara_sp_zak.getList');
    Route::get('/berita-acara-sp-zak-show/{id}', 'BeritaAcaraSPZakController@show')->name('berita_acara_sp_zak.show');
    Route::get('/berita-acara-sp-zak-print/{id}', 'BeritaAcaraSPZakController@print')->name('berita_acara_sp_zak.print');
    Route::get('/berita-acara-sp-zak-excel-spj/{id}', 'BeritaAcaraSPZakController@excelSPJ')->name('berita_acara_sp_zak.excelSpj');
    Route::get('/berita-acara-sp-zak-create', 'BeritaAcaraSPZakController@create')->name('berita_acara_sp_zak.create');
    Route::post('/berita-acara-sp-zak-store', 'BeritaAcaraSPZakController@store')->name('berita_acara_sp_zak.store');
    Route::post('/berita-acara-sp-zak-delete', 'BeritaAcaraSPZakController@delete')->name('berita_acara_sp_zak.delete');
    Route::post('/berita-acara-sp-zak-approve', 'BeritaAcaraSPZakController@approve')->name('berita_acara_sp_zak.approve');
    Route::get('/berita-acara-sp-zak-reject/{id}', 'BeritaAcaraSPZakController@reject')->name('berita_acara_sp_zak.reject');
    Route::get('/berita-acara-sp-zak-delete/{id}', 'BeritaAcaraSPZakController@delete')->name('berita_acara_sp_zak.delete');
    Route::post('/berita-acara-sp-zak-generate-no-pp', 'BeritaAcaraSPZakController@generateNoPP')->name('berita_acara_sp_zak.generateNoPP');
    Route::post('/berita-acara-sp-zak-set-inputted-sap', 'BeritaAcaraSPZakController@setInputtedSAP')->name('berita_acara_sp_zak.setInputtedSAP');
    Route::post('/berita-acara-sp-zak-generate-surat-klaim', 'BeritaAcaraSPZakController@generateSuratKlaim')->name('berita_acara_sp_zak.generateSuratKlaim');
    Route::get('/berita-acara-sp-zak-print-surat-klaim/{id}', 'BeritaAcaraSPZakController@printSuratKlaim')->name('berita_acara_sp_zak.printSuratKlaim');
    Route::get('/berita-acara-sp-zak-approve-surat-klaim/{id}', 'BeritaAcaraSPZakController@approveSuratKlaim')->name('berita_acara_sp_zak.approveSuratKlaim');
    Route::get('/berita-acara-sp-zak-reject-surat-klaim/{id}', 'BeritaAcaraSPZakController@rejectSuratKlaim')->name('berita_acara_sp_zak.rejectSuratKlaim');

    Route::get('/berita-acara-sp-curah', 'BeritaAcaraSPCurahController@index');
    Route::get('/berita-acara-sp-curah-getList', 'BeritaAcaraSPCurahController@getList')->name('berita_acara_sp_curah.getList');
    Route::get('/berita-acara-sp-curah-show/{id}', 'BeritaAcaraSPCurahController@show')->name('berita_acara_sp_curah.show');
    Route::get('/berita-acara-sp-curah-print/{id}', 'BeritaAcaraSPCurahController@print')->name('berita_acara_sp_curah.print');
    Route::get('/berita-acara-sp-curah-excel-spj/{id}', 'BeritaAcaraSPCurahController@excelSPJ')->name('berita_acara_sp_curah.excelSpj');
    Route::get('/berita-acara-sp-curah-create', 'BeritaAcaraSPCurahController@create')->name('berita_acara_sp_curah.create');
    Route::post('/berita-acara-sp-curah-store', 'BeritaAcaraSPCurahController@store')->name('berita_acara_sp_curah.store');
    Route::post('/berita-acara-sp-curah-delete', 'BeritaAcaraSPCurahController@delete')->name('berita_acara_sp_curah.delete');
    Route::post('/berita-acara-sp-curah-approve', 'BeritaAcaraSPCurahController@approve')->name('berita_acara_sp_curah.approve');
    Route::get('/berita-acara-sp-curah-reject/{id}', 'BeritaAcaraSPCurahController@reject')->name('berita_acara_sp_curah.reject');
    Route::get('/berita-acara-sp-curah-delete/{id}', 'BeritaAcaraSPCurahController@delete')->name('berita_acara_sp_curah.delete');
    Route::post('/berita-acara-sp-curah-generate-no-pp', 'BeritaAcaraSPCurahController@generateNoPP')->name('berita_acara_sp_curah.generateNoPP');
    Route::post('/berita-acara-sp-curah-set-inputted-sap', 'BeritaAcaraSPCurahController@setInputtedSAP')->name('berita_acara_sp_curah.setInputtedSAP');
    Route::post('/berita-acara-sp-curah-generate-surat-klaim', 'BeritaAcaraSPCurahController@generateSuratKlaim')->name('berita_acara_sp_curah.generateSuratKlaim');
    Route::get('/berita-acara-sp-curah-print-surat-klaim/{id}', 'BeritaAcaraSPCurahController@printSuratKlaim')->name('berita_acara_sp_curah.printSuratKlaim');
    Route::get('/berita-acara-sp-curah-approve-surat-klaim/{id}', 'BeritaAcaraSPCurahController@approveSuratKlaim')->name('berita_acara_sp_curah.approveSuratKlaim');
    Route::get('/berita-acara-sp-curah-reject-surat-klaim/{id}', 'BeritaAcaraSPCurahController@rejectSuratKlaim')->name('berita_acara_sp_curah.rejectSuratKlaim');

});

Route::group(['middleware' => ['checkUser', 'checkRole:administrator']], function () {
    Route::namespace('supplier')->group(function () {
        Route::get('/daftarvendor', 'SupplierController@index');
    });
    Route::namespace('supplier')->group(function () {
        Route::get('/detailsupplier/{id}', 'SupplierController@show');
    });
    Route::namespace('supplier')->group(function () {
        Route::get('/detailsupplier/{id}/{pesan2}', 'SupplierController@show');
    });
    Route::namespace('supplier')->group(function () {
        Route::get('/verified/{email}', 'SupplierController@verified');
    });

    Route::namespace('supplier')->group(function () {
        Route::get('/notverified/{email}', 'SupplierController@notverified');
    });

    Route::namespace('supplier')->group(function () {
        Route::post('/postnotverified', 'SupplierController@postnotverified');
    });

    Route::namespace('kategori')->group(function () {
        Route::get('/kategori', 'KategoriController@index');
        Route::get('/kategori/add', 'KategoriController@create');
        Route::post('/kategori/addpost', 'KategoriController@store');
        Route::get('/kategori/edit/{id}', 'KategoriController@edit');
        Route::post('/kategori/editpost/{id}', 'KategoriController@update');
        Route::post('/kategori/delete/{id}', 'KategoriController@destroy');
    });

    Route::namespace('kualifikasi')->group(function () {
        Route::get('/kualifikasi', 'KualifikasiController@index');
        Route::get('/kualifikasi/add', 'KualifikasiController@create');
        Route::post('/kualifikasi/addpost', 'KualifikasiController@store');
        Route::get('/kualifikasi/edit/{id}', 'KualifikasiController@edit');
        Route::post('/kualifikasi/editpost/{id}', 'KualifikasiController@update');
        Route::post('/kualifikasi/delete/{id}', 'KualifikasiController@destroy');
    });

    Route::namespace('nextnumber')->group(function () {
        Route::get('/nextnumber', 'NextnumberController@index');
        Route::get('/nextnumber/add', 'NextnumberController@create');
        Route::post('/nextnumber/addpost', 'NextnumberController@store');
        Route::get('/nextnumber/edit/{id}', 'NextnumberController@edit');
        Route::post('/nextnumber/editpost/{id}', 'NextnumberController@update');
        Route::post('/nextnumber/delete/{id}', 'NextnumberController@destroy');
    });


    Route::get('/invoice-monitor-upload', 'InvoiceMonitorController@uploadForm')->name('invoice_monitor.upload_form');
    Route::post('/invoice-monitor-upload', 'InvoiceMonitorController@upload')->name('invoice_monitor.upload');
    Route::get('/invoice-monitor-all', 'InvoiceMonitorController@indexAll')->name('invoice_monitor.index_all');
    Route::get('/invoice-monitor-all-getList', 'InvoiceMonitorController@getListAll')->name('invoice_monitor.getListAll');
});

Route::group(['middleware' => ['checkUser', 'checkRole:supplier']], function () {



    Route::group(['middleware' => 'checkVar'], function () {

        Route::namespace('supplier')->group(function () {
            Route::get('/datapribadi/{email}', 'SupplierController@datapribadi');
        });
        Route::namespace('supplier')->group(function () {
            Route::post('/datapribadi/{email}/update', 'SupplierController@update');
        });
        Route::namespace('supplier')->group(function () {
            Route::get('/datapribadi/{email}/{pesan2}', 'SupplierController@datapribadi');
        });
        Route::namespace('tender')->group(function () {
            Route::get('/kegiatanpengadaan/{email}', 'TenderdetailController@index');
        });
        Route::namespace('pp')->group(function () {
            Route::get('/permintaanpembayaran/{email}', 'PermintaanpembayaranController@index');
        });
        Route::namespace('pp')->group(function () {
            Route::get('/permintaanpembayaran/{email}/add', 'PermintaanpembayaranController@create');
        });
        Route::namespace('pp')->group(function () {
            Route::post('/permintaanpembayaran/{email}', 'PermintaanpembayaranController@store');
        });
        Route::namespace('pp')->group(function () {
            Route::get('/permintaanpembayaran/{email}/edit/{id}', 'PermintaanpembayaranController@edit');
        });
        Route::namespace('pp')->group(function () {
            Route::post('/editpermintaanpembayaran/{email}/{id}', 'PermintaanpembayaranController@update');
        });
        Route::namespace('pp')->group(function () {
            Route::post('/permintaanpembayaran/{email}/delete/{id}', 'PermintaanpembayaranController@destroy');
        });
        Route::namespace('pp')->group(function () {
            Route::get('/permintaanpembayaran/{email}/detail/{id}', 'PermintaanpembayaranController@detail');
        });
        Route::namespace('prematchingtruk')->group(function () {
            Route::get('/prematchingtruk/{email}', 'PrematchingtrukController@index');
        });
        Route::namespace('prematchingtruk')->group(function () {
            Route::get('/prematchingtruk/{email}/add', 'PrematchingtrukController@create');
        });
        Route::namespace('prematchingtruk')->group(function () {
            Route::get('/prematchingtruk/{email}/prematch/{id}', 'PrematchingtrukController@prematch');
        });
        Route::namespace('prematchingtruk')->group(function () {
            Route::post('/prematchingtruk/{email}/add', 'PrematchingtrukController@store');
        });
        Route::namespace('prematchingtruk')->group(function () {
            Route::patch('/prematchingtruk-delete/{email}/{id}/{doc}/{dct}/{lnid}', 'PrematchingtrukController@destroy');
        });
        Route::namespace('monitoringdo')->group(function () {
            Route::get('/monitoringdo/{email}', 'MonitoringdoController@index');
        });
        Route::namespace('monitoringdo')->group(function () {
            Route::post('/monitoringdo/{email}', 'MonitoringdoController@index');
        });

        Route::namespace('monitoringarmada')->group(function () {
            Route::get('/monitoringarmada/{email}', 'MonitoringarmadaController@index');
        });
        Route::namespace('monitoringarmada')->group(function () {
            Route::post('/monitoringarmada/{email}', 'MonitoringarmadaController@index');
        });
    });

    Route::namespace('tender')->group(function () {
        Route::get('/kegiatanpengadaan/detail/{tenderPenawaranId}', 'TenderdetailController@show');
    });
    Route::namespace('tender')->group(function () {
        Route::get('/kegiatanpengadaan/penawaran/{tenderPenawaranId}', 'TenderdetailController@edit');
    });
    Route::namespace('tender')->group(function () {
        Route::post('/kegiatanpengadaan/penawaranpost/{tenderPenawaranId}', 'TenderdetailController@update');
    });
    Route::namespace('tender')->group(function () {
        Route::get('/kegiatanpengadaan/negosiasi/{tenderPenawaranId}', 'TenderdetailController@negosiasi');
    });
    Route::namespace('tender')->group(function () {
        Route::post('/kegiatanpengadaan/negosiasipost/{tenderPenawaranId}', 'TenderdetailController@negosiasipost');
    });
    Route::namespace('tender')->group(function () {
        Route::get('/kegiatanpengadaan/detailtender/{tenderPenawaranId}', 'TenderdetailController@detail');
    });
    Route::namespace('pp')->group(function () {
        Route::get('/getOP', 'PermintaanpembayaranController@getop');
    });
});

Route::group(['middleware' => ['checkUser', 'checkRole:administrator,admin']], function () {

    Route::namespace('tender')->group(function () {
        Route::get('/tender', 'TenderController@index');
        Route::get('/tender/add', 'TenderController@create');
        Route::post('/tender/addpost', 'TenderController@store');
        Route::get('/tender/edit/{id}', 'TenderController@edit');
        Route::post('/tender/editpost/{id}', 'TenderController@update');
        Route::post('/tender/delete/{id}', 'TenderController@destroy');
        Route::get('/tender/detail/{id}', 'TenderController@show');
        Route::post('/tender/kirimemail/{id}', 'TenderController@kirimemail');
        Route::get('/tender/detailtender/{id}', 'TenderController@detail');
        Route::post('/tender/waktunegoall/{id}', 'TenderController@waktunegoall');
        Route::get('/tender/pemenang/{id}/{id2}', 'TenderController@pemenang');
        Route::post('/tender/pemenangpost/{id}', 'TenderController@pemenangpost');
    });

    Route::namespace('pp')->group(function () {
        Route::get('/permintaanpembayaran', 'PermintaanpembayaranController@home');
        Route::get('/permintaanpembayaran/detail/{id}', 'PermintaanpembayaranController@show');
        Route::post('/permintaanpembayaran/komplit/{id}', 'PermintaanpembayaranController@complite');
        Route::get('/permintaanpembayaran/nokomplit/{id}', 'PermintaanpembayaranController@nocomplite');
        Route::post('/permintaanpembayaran/nokomplit/{id}', 'PermintaanpembayaranController@nocomplitepost');
        Route::get('/permintaanpembayaran/flash/{id}/{kode}/{pesan}', 'PermintaanpembayaranController@flash')->name('flash');
        Route::get('/permintaanpembayaran/addpp/{id}', 'PermintaanpembayaranController@addnopp');
        Route::post('/permintaanpembayaran/addpppost/{id}', 'PermintaanpembayaranController@addnopppost');
    });
});

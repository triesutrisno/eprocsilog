<?php

namespace App\Http\Util;


class DataTablesHelper
{
    static function formatDataOutput($columns, $data)
    {
        $out = array();
        for ($i = 0, $ien = count($data); $i < $ien; $i++) {
            $row = array();
            for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
                $column = $columns[$j];
                $dt = array_key_exists('dt', $column) ? $column['dt'] : $j;
                // Is there a formatter?
                if (isset($column['formatter'])) {
                    $row[$dt] = $column['formatter']($data[$i][$column['db']], $data[$i]);
                } else {
                    $row[$dt] = $data[$i][$columns[$j]['db']];
                }
            }
            $out[] = $row;
        }
        return $out;
    }

    static function formatDataOutputCustom($columns, $data)
    {
        $out = array();
        for ($i = 0, $ien = count($data); $i < $ien; $i++) {
            $row = array();
            for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
                $column = $columns[$j];
                // Is there a formatter?
                if (isset($column['formatter'])) {
                    $row[$column['dt']] = $column['formatter']($data[$i][$column['db']], $data[$i]);
                } else {
                    if (count(explode('.', $columns[$j]['db'])) > 1) {
                        $datum = explode('.', $columns[$j]['db']);
                        $row[$column['dt']] = $data[$i][$datum[0]][$datum[1]];
                    } else {
                        $row[$column['dt']] = $data[$i][$columns[$j]['db']];
                    }
                }
            }
            $out[] = $row;
        }
        return $out;
    }

    public static function formatNumber($value)
    {
        return number_format($value, 0, ',', '.');
    }

    static function generateRequestParam($request, $columns)
    {

        $start = $request->get('start');
        $per_page = $request->get('length');

        $param['q'] = $request->get('search')['value'];
        $param['per_page'] = $per_page;
        $param['page'] = ceil($start / $per_page) + 1;

        //order
        $orderParam = $request->get('order');
        // dd(collect($columns)->where('dt', 4)->first()->db);
        // $column = $columns[$orderParam[0]['column']];
        $column = collect($columns)->where('dt', $orderParam[0]['column'])->first();
        $param['order_by'] = isset($column['param']) ? $column['param'] : $column['db'];
        $param['order_type'] = $orderParam[0]['dir'];

        //search
        $columnParam = $request->get('columns');
        for ($i = 0; $i < sizeof($columnParam); $i++) {
            $column = $columnParam[$i];
            $searchValue = $column['search']['value'];
            if (!is_null($searchValue)) {
                // dd($columns[$i]);
                if (isset($columns[$i]['param'])) {
                    $param[$columns[$i]['param']] = $searchValue;
                } else {
                    $param[$columns[$i]['db']] = $searchValue;
                }

                // dd($param);
            }
        }

        // dd($param);
        return $param;
    }

    static function formatRequestParam($request)
    {
        $start = $request->get('start');
        $per_page = $request->get('length');

        $param['q'] = $request->get('search')['value'];
        $param['per_page'] = $per_page;
        $param['page'] = ceil($start / $per_page) + 1;

        $columns = $request->get('columns');

        //order
        $order = $request->get('order');
        $param['order_by'] = $columns[$order[0]['column']]['data'];
        $param['order_type'] = $order[0]['dir'];

        //         //search
        //         $columnParam = $request->get('columns');
        //         for ($i=0; $i<sizeof($columnParam); $i++){
        //             $column = $columnParam[$i];
        //             $searchValue = $column['search']['value'];
        //             if(!is_null($searchValue)){
        //                 if (isset($columns[$i]['param']) ) {
        //                     $param[$columns[$i]['param']] = $searchValue;
        //                 } else {
        //                     $param[$columns[$i]['db']] = $searchValue;
        //                 }
        //             }
        //         }

        // //        dd($param);
        return $param;
    }
}

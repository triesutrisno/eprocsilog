<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,...$roles)
    {
        #dd(session('user')->role);
        if(in_array(session('user')->role,$roles)){
            return $next($request);
        }
        session()->flush();
        return redirect('/login');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class Checkvariabel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($request->route()->email);
        //dd(session('user')->username);
        if(session('user')->username == $request->route()->email){
            return $next($request);
        }
        session()->flush();
        return redirect('/login');
    }
}

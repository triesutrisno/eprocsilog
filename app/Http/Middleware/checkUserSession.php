<?php

namespace App\Http\Middleware;

use Closure;

class checkUserSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($request->session());
        if (!$request->session()->exists('user')) {
            // user value cannot be found in session
            session()->flush();
            return redirect('/login');
        }
        
        return $next($request);
    }
}

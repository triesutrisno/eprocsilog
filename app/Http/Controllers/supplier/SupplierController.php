<?php

namespace App\Http\Controllers\supplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {            
        $urle = env('API_BASE_URL')."displaysupplier";
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = "";
            $pesan = "";
        }
        return view('supplier.index', ['model'=>$model, 'pesan'=>$pesan]);
    }    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $pesan2=NULL)
    {
        $urle = env('API_BASE_URL')."detailsupplier";
        $response = Http::post($urle,[
            'idsupp' => $id,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = "";
            $pesan = "";
        }
        return view('supplier.detail', ['model'=>$model, 'pesan'=>$pesan, 'pesan2'=>$pesan2]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $email)
    {
        #dd($request->unit);
        $request->validate([
            'setuju' => 'required',
        ]);      
        if($email == session('user')->username){          
            $pathEmail = $email;
            // Bekerja dengan upload file
            $lampiranSiup = $request->file('lampiranSiup') != NULL ? $request->file('lampiranSiup')->store($pathEmail.'/lampiranSiup') : "";
            $lampiranNPWP = $request->file('lampiranNPWP') != NULL ? $request->file('lampiranNPWP')->store($pathEmail.'/lampiranNPWP') : "";
            $lampiranNPPKP = $request->file('lampiranNPPKP') != NULL ? $request->file('lampiranNPPKP')->store($pathEmail.'/lampiranNPPKP') : "";
            $lampiranTDP = $request->file('lampiranTDP') != NULL ? $request->file('lampiranTDP')->store($pathEmail.'/lampiranTDP') : "";
            $pemimpinKTPFile = $request->file('pemimpinKTPFile') != NULL ? $request->file('pemimpinKTPFile')->store($pathEmail.'/pemimpinKTPFile') : "";
            $PemilikKTPFile = $request->file('PemilikKTPFile') != NULL ? $request->file('PemilikKTPFile')->store($pathEmail.'/PemilikKTPFile') : "";        
            $supBank1File = $request->file('supBank1File') != NULL ? $request->file('supBank1File')->store($pathEmail.'/supBank1File') : "";              
            $supBank2File = $request->file('supBank2File') != NULL ? $request->file('supBank2File')->store($pathEmail.'/supBank2File') : "";              
            // end bekerja dengan upload file

            $urle = env('API_BASE_URL')."updatedatapribadi/".$email;
            $response = Http::post($urle, [
                'supNama' => strtoupper($request->namaPerusahaan),
                'supWebsite' => $request->websitePerusahaan,
                'supTlp' => $request->noTlp,
                'supAlamat' => $request->alamatPerusahaan,
                'mpropId' => $request->propinsiPerusahaan,
                'mkabId' => $request->kotaPerusahaan,
                'mkecId' => $request->kecamatanPerusahaan,
                'mkatId' => $request->kategoriPerusahaan,
                'mkualId' => $request->kualifikasiPerusahaan,
                'supSIUP' => $request->siupPerusahaan,
                'supSIUPFile' => $lampiranSiup,
                'supNPWP' => $request->npwpPerusahaan,
                'supNPWPFile' => $lampiranNPWP,
                'supNPPKP' => $request->nppkpPerusahaan,
                'supNPPKPFile' => $lampiranNPPKP,
                'supTDP' => $request->tdpPerusahaan,
                'supTDPFile' => $lampiranTDP,
                'supAkteNoteris' => $request->aktaNotarisPerusahaan,
                'supDomisili' => $request->skdPerusahaan,
                'supBank1' => $request->nmBank1,
                'supNorek1' => $request->norekBank1,
                'supBank1File' => $supBank1File,
                'supBank2' => $request->nmBank2,
                'supNorek2' => $request->norekBank2,
                'supBank2File' => $supBank2File,
                'pemimpinNama' => strtoupper($request->pemipinPerusahaan),
                'pemimpinJabatan' => $request->jabatanPemimpin,
                'pemimpinEmail' => $request->emailPemimpin,
                'pemimpinTlp' => $request->noTlpPemimpin,
                'pemimpinHp' => $request->noHpPemimpin,
                'pemimpinAlamat' => $request->alamatPemimpin,
                'pemimpinKTP' => $request->ktpPemimpin,
                'pemimpinKTPFile' => $pemimpinKTPFile,
                'PemilikNama' => strtoupper($request->pemilikPerusahaan),
                'PemilikJabatan' => $request->jabatanPemilik,
                'PemilikEmail' => $request->emailPemilik,
                'PemilikTlp' => $request->noTlpPemilik,
                'PemilikHp' => $request->noHpPemilik,
                'PemilikAlamat' => $request->alamatPemilik,
                'PemilikKTP' => $request->ktpPemilik,
                'PemilikKTPFile' => $PemilikKTPFile,
                'password' => $request->password,
                'supSetuju' => $request->setuju,
                'unit' => $request->unit,
            ]);

            $dtAPi = json_decode($response->getBody()->getContents(),true);
            $responStatus = $response->getStatusCode();
            //dd($dtAPi);
            if($responStatus=='200'){
                //dd($request->file('lampiranSiup')->store('lampiranSiup'));
                if($dtAPi['kode']=="90"){
                     // Bekerja dengan upload file
                    Storage::delete($lampiranSiup);
                    Storage::delete($lampiranNPWP);
                    Storage::delete($lampiranNPPKP);
                    Storage::delete($lampiranTDP);
                    Storage::delete($pemimpinKTPFile);
                    Storage::delete($PemilikKTPFile);        
                    // end bekerja dengan upload file
                }
                $pesan = "Data berhasil diupdate !";
                #return redirect('datapribadi/'.$email)->with(['pesan2'=> $pesan]);
                return redirect("/datapribadi/".$email."/".$pesan);
                #return view('supplier.datapribadi', ['model'=>$dtAPi['supp'], 'message'=>$pesan]);
            }else{
                $pesan = "Data gagal diupdate !";
                #return redirect('datapribadi/'.$email)->with(['pesan2'=> $pesan]);
                return redirect("/datapribadi/".$email."/".$pesan);
            }
        }else{
            $urle = env('API_BASE_URL')."logout";
            $response = Http::post($urle,[
                'email' => $email,
            ]);
            $dtAPi = json_decode($response->getBody()->getContents(),true);  
            $responStatus = $response->getStatusCode();  
            //dd($responStatus);
            //if($responStatus=='200'){
                //Auth::logout(session('user'));
                session()->flush();
                return redirect('/login');
            //}   
        }
    }    
    public function verified($email)
    {
        $urle = env('API_BASE_URL')."verified";
        $response = Http::post($urle,[
            'email' => $email,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi['data'];
            $pesan = $dtAPi['pesan'];
            $id = $dtAPi['data'][0]['supId'];
        }else{
            $model = "";
            $pesan = "";
            $id = "";
        }
        //dd($id);
        #return view('supplier.detail', ['model'=>$model, 'pesan'=>$pesan]);
        return redirect("/detailsupplier/".$id."/".$pesan);
    }
    
    public function notverified($email)
    {
        return view('supplier.notverified',['email'=>$email]);
    }
    
    public function postnotverified(Request $request)
    {        
        $urle = env('API_BASE_URL')."notverified";
        $response = Http::post($urle,[
            'email' => $request->email,
            'pesan' => $request->pesanKegagalan,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            if($dtAPi['kode']=='99'){
                $model = $dtAPi['data'];
                $pesan = $dtAPi['pesan'];
                $id = $dtAPi['data'][0]['supId'];
                return redirect("/detailsupplier/".$id."/".$pesan);
            }else{
                $model = "";
                $pesan = $dtAPi['pesan'];
                $id = "";
                return redirect("/daftarvendor");
            }
        }else{
            $model = "";
            $pesan = "";
            $id = "";
            return redirect("/daftarvendor");
        }
        
    }
    
    public function datapribadi($email, $pesan2=NULL)
    {
        $urle = env('API_BASE_URL')."datapribadi";
        $response = Http::post($urle,[
            'email' => $email,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi['dtSupp'];
            $dtProp = $dtAPi['dtProp'];
            $dtKat = $dtAPi['dtKat'];
            $dtKual = $dtAPi['dtKual'];                
            $dtUnit = $dtAPi['dtUnit'];
            $pesan = "";
        }else{
            $model = "";
            $dtProp = "";            
            $dtKat = "";
            $dtKual = "";                
            $dtUnit = "";
            $pesan = "";
        }
        return view('supplier.datapribadi', ['model'=>$model, 'pesan'=>$pesan, 'dtProp'=>$dtProp, 'dtKat'=>$dtKat, 'dtKual'=>$dtKual, 'dtUnit'=>$dtUnit, 'pesan2'=>$pesan2]);
    }
}

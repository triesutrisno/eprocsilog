<?php

namespace App\Http\Controllers\monitoringarmada;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;


class MonitoringarmadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        
        //add date param
        $param["nopin"]  = $request->nopin != NULL ? $request->nopin : "";
        $param["nopol"] = $request->nopol != NULL ? $request->nopol : "";
        $param["driver"] = $request->driver != NULL ? $request->driver : "";
        $param["pemilikArmada"] = $request->pemilikArmada != NULL ? $request->pemilikArmada : "";
        $param["status"]  = $request->status != NULL ? $request->status : "";
        $param["statusTruk"] = $request->statusTruk != NULL ? $request->statusTruk : "";   
        $param["statusMuatan"] = $request->statusMuatan != NULL ? $request->statusMuatan : "";        
        #$param["email"] = session('user')->username;
        #dd($param);
        
        $urle = env('API_BASE_URL')."monitoringarmada";
        $response = Http::post($urle,[
            'email' => session('user')->username,
            'param' => $param,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        #dd($dtAPi);
        $responStatus = $response->getStatusCode();
        
        if($responStatus==200){
            $model = $dtAPi;
            $message = '';
        } else {
            $model = "";
            $message = $resp['message'];
        }
        
        return view('monitoringarmada.index',['model'=>$model, 'message'=>$message, 'param'=>$param, 'kode'=>'', 'pesan'=>'']);
    }
}

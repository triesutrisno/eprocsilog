<?php

namespace App\Http\Controllers\kebijakan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KebijakanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kebijakan.index');
    }
}

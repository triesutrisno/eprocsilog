<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Util\DataTablesHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class InvoiceMonitorController extends Controller
{

    public function index()
    {
        return view('invoice_monitor.index');
    }

    public function getList(Request $request)
    {

        $url = config('custom.api_base_url') . "invoice_monitor";

        $columns = array(
            array('db' => 'id_invoice_monitor'),
            array('db' => 'kode_sap_vendor'),
            array('db' => 'nama_perusahaan'),
            array('db' => 'nama_pelanggan'),
            array('db' => 'no_kwitansi'),
            array('db' => 'nominal'),
            array('db' => 'tgl_kwitansi'),
            array('db' => 'posisi_tagihan'),
            array('db' => 'tgl_rencana_pembayaran'),
            array('db' => 'created_at'),
            array('db' => 'updated_at'),
        );

        $skippedColumn = [];
        $lastColumnIndex = 0;
        for ($i = 0; $i < (sizeOf($columns) + sizeOf($skippedColumn)); $i++) {
            if (!in_array($i, $skippedColumn)) {
                if ($lastColumnIndex < sizeOf($columns)) {
                    $columns[$lastColumnIndex]['dt'] = $i;
                    $lastColumnIndex++;
                }
            }
        }

        $param = DataTablesHelper::generateRequestParam($request, $columns);
        $param["no_kwitansi"] = $request->no_kwitansi ?? "";
        $param["nama_perusahaan"] = $request->nama_perusahaan ?? "";
        $param["tgl_kwitansi"] = $request->tgl_kwitansi ?? "";
        $param["id_user"] = $request->session()->get('user')->id;
        // dd($param);

        $response = Http::get($url, $param);
        $resp = json_decode($response->getBody(), true);
        // echo($response->getBody());
        // die();
        // dd($resp);

        if ($response->successful()) {

            $result =  array(
                "recordsTotal"    => $resp['meta']['total'],
                "recordsFiltered" => $resp['meta']['total'],
                "data"            => DataTablesHelper::formatDataOutput($columns, $resp['data'])
            );

            return $result;
        } else {
            return $resp['message'];
        }
    }

    public function uploadForm()
    {
        return view('invoice_monitor.upload');
    }


    public function upload(Request $request)
    {
        // dd($request->session()->get('user'));
        $request->validate([
            'uploaded_file' => 'required|mimes:xls,xlsx',
        ]);

        $url = env('API_BASE_URL') . "invoice_monitor-upload";

        $param = $request->all();
        // $dates = explode(" - ", $request->get('periode_spj'));
        // $param['periode_spj_awal'] = Carbon::createFromFormat('m/d/Y', $dates[0]);
        // $param['periode_spj_akhir'] = Carbon::createFromFormat('m/d/Y', $dates[1]);
        $param['id_user'] = $request->session()->get('user')->id;
        $uploadedFile = $request->file('uploaded_file');
        $filename = $uploadedFile->getClientOriginalName();

        // dd($param);

        $response = Http::attach(
            'uploaded_file',
            file_get_contents($uploadedFile->getRealPath()),
            $filename
        )
            ->post($url, $param);
        $resp = json_decode($response->getBody(), false);
        // echo($response->getBody());
        // die();

        // dd($resp);

        $status = "";
        $message = "";
        if ($response->successful()) {
            $status = $resp->status;
            $message = $resp->message;
        } else {
            $status = "error";
            $message = $resp->message;
        }

        return redirect()->to('/invoice-monitor-upload')->with($status, $message);
    }

    public function indexAll()
    {
        return view('invoice_monitor.index_all');
    }

    public function getListAll(Request $request)
    {

        $url = config('custom.api_base_url') . "invoice_monitor-all";

        $columns = array(
            array('db' => 'id_invoice_monitor'),
            array('db' => 'kode_sap_vendor'),
            array('db' => 'nama_perusahaan'),
            array('db' => 'nama_pelanggan'),
            array('db' => 'no_kwitansi'),
            array('db' => 'nominal'),
            array('db' => 'tgl_kwitansi'),
            array('db' => 'posisi_tagihan'),
            array('db' => 'tgl_rencana_pembayaran'),
            array('db' => 'created_at'),
            array('db' => 'updated_at'),
        );

        $skippedColumn = [];
        $lastColumnIndex = 0;
        for ($i = 0; $i < (sizeOf($columns) + sizeOf($skippedColumn)); $i++) {
            if (!in_array($i, $skippedColumn)) {
                if ($lastColumnIndex < sizeOf($columns)) {
                    $columns[$lastColumnIndex]['dt'] = $i;
                    $lastColumnIndex++;
                }
            }
        }

        $param = DataTablesHelper::generateRequestParam($request, $columns);

        $param["no_kwitansi"] = $request->no_kwitansi ?? "";
        $param["nama_perusahaan"] = $request->nama_perusahaan ?? "";
        $param["tgl_kwitansi"] = $request->tgl_kwitansi ?? "";
        $param["id_user"] = $request->session()->get('user')->id;
        // dd($param);

        $response = Http::get($url, $param);
        $resp = json_decode($response->getBody(), true);
        // echo($response->getBody());
        // die();
        // dd($resp);

        if ($response->successful()) {

            $result =  array(
                "recordsTotal"    => $resp['meta']['total'],
                "recordsFiltered" => $resp['meta']['total'],
                "data"            => DataTablesHelper::formatDataOutput($columns, $resp['data'])
            );

            return $result;
        } else {
            return $resp['message'];
        }
    }

}

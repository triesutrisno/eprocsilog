<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Util\DataTablesHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BeritaAcaraController extends Controller
{

    public function index()
    {
        return view('berita_acara.index');
    }

    public function getList(Request $request)
    {

        $url = config('custom.api_base_url') . "berita_acara";

        $columns = array(
            array('db' => 'id_berita_acara'),
            array('db' => 'no_berita_acara'),
            array('db' => 'nama_supplier'),
            array('db' => 'jenis_pelanggan'),
            array('db' => 'no_kontrak_perjanjian'),
            array('db' => 'periode_spj'),
            array('db' => 'jenis_plat'),
            array('db' => 'plant'),
            array('db' => 'status_barang'),
            array('db' => 'approve_status'),
            array('db' => 'approve_status_label', 'param' => 'approve_status'),
            array('db' => 'approved_by'),
            array('db' => 'approved_by_name'),
            array('db' => 'approved_at'),
            array('db' => 'total_spj'),
            array('db' => 'total_approved'),
            array('db' => 'created_at'),
            array('db' => 'no_pp'),
            array('db' => 'is_inputted_sap'),
            array('db' => 'no_surat_klaim'),
            array('db' => 'surat_klaim_approve_status_label', 'param' => 'surat_klaim_approve_status'),
        );

        $skippedColumn = [];
        $lastColumnIndex = 0;
        for ($i = 0; $i < (sizeOf($columns) + sizeOf($skippedColumn)); $i++) {
            if (!in_array($i, $skippedColumn)) {
                if ($lastColumnIndex < sizeOf($columns)) {
                    $columns[$lastColumnIndex]['dt'] = $i;
                    $lastColumnIndex++;
                }
            }
        }

        $param = DataTablesHelper::generateRequestParam($request, $columns);

        $param["no_berita_acara"]  = $request->no_berita_acara ?? "";
        $param["nama_supplier"]  = $request->nama_supplier ?? "";
        $param["no_kontrak_perjanjian"] = $request->no_kontrak_perjanjian ?? "";
        $param["approve_status"] = $request->status ?? "";
        $param["tgl_approve"] = $request->tgl_approve ?? "";
        $param["status_barang"] = $request->status_barang ?? "";
        $param["no_spj"] = $request->no_spj ?? "";
        $param["no_pp"] = $request->no_pp ?? "";
        $param["status_input_sap"] = $request->status_input_sap ?? "";
        $param["status_generate_klaim"] = $request->status_generate_klaim ?? "";
        $param["status_approve_klaim"] = $request->status_approve_klaim ?? "";

        $param["id_user"] = $request->session()->get('user')->id;
        // dd($param);

        $response = Http::get($url, $param);
        $resp = json_decode($response->getBody(), true);
        // echo($response->getBody());
        // die();
        // dd($resp);

        if ($response->successful()) {

            $result =  array(
                "recordsTotal"    => $resp['meta']['total'],
                "recordsFiltered" => $resp['meta']['total'],
                "data"            => DataTablesHelper::formatDataOutput($columns, $resp['data'])
            );

            return $result;
        } else {
            return $resp['message'];
        }
    }

    public function create()
    {
        return view('berita_acara.create');
    }


    public function store(Request $request)
    {
        // dd($request->session()->get('user'));
        $request->validate([
            'no_kontrak_perjanjian' => 'required',
            'jenis_pelanggan' => 'required',
            'nama_pic_supplier' => 'required',
            'berita_acara_spj' => 'required|mimes:xls,xlsx',
        ]);

        $url = env('API_BASE_URL') . "berita_acara";

        $param = $request->all();
        $dates = explode(" - ", $request->get('periode_spj'));
        $param['periode_spj_awal'] = Carbon::createFromFormat('m/d/Y', $dates[0]);
        $param['periode_spj_akhir'] = Carbon::createFromFormat('m/d/Y', $dates[1]);
        $param['id_user'] = $request->session()->get('user')->id;
        $berita_acara_spj = $request->file('berita_acara_spj');
        $filename = $berita_acara_spj->getClientOriginalName();

        $response = Http::attach(
            'berita_acara_spj',
            file_get_contents($berita_acara_spj->getRealPath()),
            $filename
        )
            ->post($url, $param);
        $resp = json_decode($response->getBody(), false);
        // echo($response->getBody());
        // die();

        // dd($resp);

        $status = "";
        $message = "";
        if ($response->successful()) {
            $status = $resp->status;
            $message = $resp->message;
        } else {
            $status = "error";
            $message = $resp->message;
        }

        return redirect()->to('berita-acara')->with($status, $message);
    }

    public function show($id)
    {
        $url = config('custom.api_base_url') . "berita_acara_show";
        $response = Http::get($url, ['id_berita_acara' => $id]);
        $resp = json_decode($response->getBody(), false);

        // dd($resp);

        if ($response->successful()) {

            return view('berita_acara.show', ['data' => $resp]);
        } else {
            return $resp['message'];
        }
    }

    public function delete(Request $request, $id)
    {
        $url = config('custom.api_base_url') . "berita_acara_delete";
        $response = Http::post($url, [
            'id_berita_acara' => $id,
            'id_user' => $request->session()->get('user')->id,

        ]);

        $resp = json_decode($response->getBody(), false);

        $status = "";
        $message = "";
        if ($response->successful()) {
            $status = $resp->status;
            $message = $resp->message;
        } else {
            $status = "error";
            $message = $resp->message;
        }

        return back()->with($status, $message);
    }

    public function approve(Request $request)
    {
        // dd($request->get('data'));
        $url = config('custom.api_base_url') . "berita_acara_approve";
        $response = Http::post($url, [
            'id_berita_acara' => $request->get('id_berita_acara'),
            'ids' => $request->get('data'),
            'id_user' => $request->session()->get('user')->id,

        ]);

        $resp = json_decode($response->getBody(), false);

        // dd($resp);
        // dd(isset($resp['success']));

        $status = "";
        $message = "";
        if ($response->successful()) {
            $status = $resp->status;
            $message = $resp->message;
        } else {
            $status = "error";
            $message = $resp->message;
        }

        // dd($status. " ". $message);
        return back()->with($status, $message);
    }

    public function reject(Request $request, $id)
    {
        $url = config('custom.api_base_url') . "berita_acara_reject";
        $response = Http::post($url, [
            'id_berita_acara' => $id,
            'id_user' => $request->session()->get('user')->id,

        ]);

        $resp = json_decode($response->getBody(), false);

        $status = "";
        $message = "";
        if ($response->successful()) {
            $status = $resp->status;
            $message = $resp->message;
        } else {
            $status = "error";
            $message = $resp->message;
        }

        return back()->with($status, $message);
    }

    public function print(Request $request, $id)
    {
        $url = config('custom.api_base_url') . "berita_acara_print";
        $response = Http::get($url, [
            'id_berita_acara' => $id,
            'id_user' => $request->session()->get('user')->id,
        ]);

        $resp = json_decode($response->getBody(), false);
        // dd($resp);
        return view('berita_acara.print', ['data' => $resp]);
    }

    public function excelSPJ(Request $request, $id)
    {
        $url = config('custom.api_base_url') . "berita_acara_excel_spj";

        // $response = Http::get($url, [
        //     'id_berita_acara' => $id,
        //     'id_user' => $request->session()->get('user')->id,
        // ]);
        // echo($response->getBody());
        // die();

        return response()->streamDownload(function () use ($url, $id, $request) {
            $response = Http::withHeaders([
                'accept' => 'application/octet-stream',
            ])->get($url, [
                'id_berita_acara' => $id,
                'id_user' => $request->session()->get('user')->id,
            ]);
            echo $response->body();
        }, 'Berita Acara SPJ.xlsx');
    }


    public function generateNoPP(Request $request)
    {
        $url = config('custom.api_base_url') . "berita_acara_generate_no_pp";
        $response = Http::post($url, [
            'id_berita_acara' => $request->get('id_berita_acara'),
            'id_user' => $request->session()->get('user')->id,

        ]);

        $resp = json_decode($response->getBody(), false);

        $status = "";
        $message = "";
        if ($response->successful()) {
            $status = $resp->status;
            $message = $resp->message;
        } else {
            $status = "error";
            $message = $resp->message;
        }

        // dd($status. " ". $message);
        return back()->with($status, $message);
    }

    public function setInputtedSAP(Request $request)
    {
        $url = config('custom.api_base_url') . "berita_acara_set_inputted_sap";
        $response = Http::post($url, [
            'id_berita_acara' => $request->get('id_berita_acara'),
            'id_user' => $request->session()->get('user')->id,

        ]);

        $resp = json_decode($response->getBody(), false);

        $status = "";
        $message = "";
        if ($response->successful()) {
            $status = $resp->status;
            $message = $resp->message;
        } else {
            $status = "error";
            $message = $resp->message;
        }

        return back()->with($status, $message);
    }


    public function generateSuratKlaim(Request $request)
    {
        $url = config('custom.api_base_url') . "berita_acara_generate_surat_klaim";
        $response = Http::post($url, [
            'id_berita_acara' => $request->get('id_berita_acara'),
            'surat_klaim_harga_kantong' => $request->get('surat_klaim_harga_kantong'),
            'surat_klaim_harga_semen' => $request->get('surat_klaim_harga_semen'),
            'surat_klaim_keterangan' => $request->get('surat_klaim_keterangan'),
            'id_user' => $request->session()->get('user')->id,
        ]);

        $resp = json_decode($response->getBody(), false);

        $status = "";
        $message = "";
        if ($response->successful()) {
            $status = $resp->status;
            $message = $resp->message;
        } else {
            $status = "error";
            $message = $resp->message;
        }

        return back()->with($status, $message);
    }

    public function printSuratKlaim(Request $request, $id)
    {
        $url = config('custom.api_base_url') . "berita_acara_print_surat_klaim";
        $response = Http::get($url, [
            'id_berita_acara' => $id,
            'id_user' => $request->session()->get('user')->id,
        ]);

        $resp = json_decode($response->getBody(), false);
        // dd($resp);
        return view('berita_acara.print_surat_klaim', ['data' => $resp]);
    }

    public function approveSuratKlaim(Request $request, $id)
    {
        $url = config('custom.api_base_url') . "berita_acara_approve_surat_klaim";
        $response = Http::get($url, [
            'id_berita_acara' => $id,
            'id_user' => $request->session()->get('user')->id,

        ]);

        $resp = json_decode($response->getBody(), false);

        $status = "";
        $message = "";
        if ($response->successful()) {
            $status = $resp->status;
            $message = $resp->message;
        } else {
            $status = "error";
            $message = $resp->message;
        }

        return back()->with($status, $message);
    }

    public function rejectSuratKlaim(Request $request, $id)
    {
        $url = config('custom.api_base_url') . "berita_acara_reject_surat_klaim";
        $response = Http::get($url, [
            'id_berita_acara' => $id,
            'id_user' => $request->session()->get('user')->id,

        ]);

        $resp = json_decode($response->getBody(), false);

        $status = "";
        $message = "";
        if ($response->successful()) {
            $status = $resp->status;
            $message = $resp->message;
        } else {
            $status = "error";
            $message = $resp->message;
        }

        return back()->with($status, $message);
    }
}

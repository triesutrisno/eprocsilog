<?php

namespace App\Http\Controllers\register;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Storage;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new Client();
        $urle = env('API_BASE_URL')."dtpropinsi";
        $response = $client->request('POST', $urle);        
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        if($responStatus=='200'){
            if($dtAPi['kode']=="99"){
                $dtProp = $dtAPi['dtProp'];
                $dtKat = $dtAPi['dtKat'];
                $dtKual = $dtAPi['dtKual'];                
                $dtUnit = $dtAPi['dtUnit'];
            }else{
                $dtProp = "";
                $dtKat = "";
                $dtKual = "";
                $dtUnit = "";
            }
        }else{
            $dtProp = "";
            $dtKat = "";
            $dtKual = "";
            $dtUnit = "";
        }
        return view('register.index', ['dtProp'=>$dtProp, 'dtKat'=>$dtKat, 'dtKual'=>$dtKual, 'dtUnit'=>$dtUnit]);
    }
    
    public function store(Request $request)
    {
        //dd($request);
        $request->validate([
            'setuju' => 'required',
        ]);      
        
        $pathEmail = $request->emailPerusahaan;        
        // Bekerja dengan upload file
        $lampiranSiup = $request->file('lampiranSiup') != NULL ? $request->file('lampiranSiup')->store($pathEmail.'/lampiranSiup') : "";
        $lampiranNPWP = $request->file('lampiranNPWP') != NULL ? $request->file('lampiranNPWP')->store($pathEmail.'/lampiranNPWP') : "";
        $lampiranNPPKP = $request->file('lampiranNPPKP') != NULL ? $request->file('lampiranNPPKP')->store($pathEmail.'/lampiranNPPKP') : "";
        $lampiranTDP = $request->file('lampiranTDP') != NULL ? $request->file('lampiranTDP')->store($pathEmail.'/lampiranTDP') : "";
        $pemimpinKTPFile = $request->file('pemimpinKTPFile') != NULL ? $request->file('pemimpinKTPFile')->store($pathEmail.'/pemimpinKTPFile') : "";
        $PemilikKTPFile = $request->file('PemilikKTPFile') != NULL ? $request->file('PemilikKTPFile')->store($pathEmail.'/PemilikKTPFile') : "";       
        $supBank1File = $request->file('supBank1File') != NULL ? $request->file('supBank1File')->store($pathEmail.'/supBank1File') : "";              
        $supBank2File = $request->file('supBank2File') != NULL ? $request->file('supBank2File')->store($pathEmail.'/supBank2File') : "";        
        // end bekerja dengan upload file
        
        $client = new Client();        
        $urle = env('API_BASE_URL')."registersupplier";
        $response = $client->request('POST', $urle, [
            'form_params' => [
                'paktaIntegritas' => $request->paktaIntegritas,
                'supNama' => strtoupper($request->namaPerusahaan),
                'supWebsite' => $request->websitePerusahaan,
                'supEmail' => $request->emailPerusahaan,
                'supTlp' => $request->noTlp,
                'supAlamat' => $request->alamatPerusahaan,
                'mpropId' => $request->propinsiPerusahaan,
                'mkabId' => $request->kotaPerusahaan,
                'mkecId' => $request->kecamatanPerusahaan,
                'mkatId' => $request->kategoriPerusahaan,
                'mkualId' => $request->kualifikasiPerusahaan,
                'supSIUP' => $request->siupPerusahaan,
                'supSIUPFile' => $lampiranSiup,
                'supNPWP' => $request->npwpPerusahaan,
                'supNPWPFile' => $lampiranNPWP,
                'supNPPKP' => $request->nppkpPerusahaan,
                'supNPPKPFile' => $lampiranNPPKP,
                'supTDP' => $request->tdpPerusahaan,
                'supTDPFile' => $lampiranTDP,
                'supAkteNoteris' => $request->aktaNotarisPerusahaan,
                'supDomisili' => $request->skdPerusahaan,
                'supBank1' => $request->nmBank1,
                'supNorek1' => $request->norekBank1,
                'supBank1File' => $supBank1File,
                'supBank2' => $request->nmBank2,
                'supNorek2' => $request->norekBank2,
                'supBank2File' => $supBank2File,
                'pemimpinNama' => strtoupper($request->pemipinPerusahaan),
                'pemimpinJabatan' => $request->jabatanPemimpin,
                'pemimpinEmail' => $request->emailPemimpin,
                'pemimpinTlp' => $request->noTlpPemimpin,
                'pemimpinHp' => $request->noHpPemimpin,
                'pemimpinAlamat' => $request->alamatPemimpin,
                'pemimpinKTP' => $request->ktpPemimpin,
                'pemimpinKTPFile' => $pemimpinKTPFile,
                'PemilikNama' => strtoupper($request->pemilikPerusahaan),
                'PemilikJabatan' => $request->jabatanPemilik,
                'PemilikEmail' => $request->emailPemilik,
                'PemilikTlp' => $request->noTlpPemilik,
                'PemilikHp' => $request->noHpPemilik,
                'PemilikAlamat' => $request->alamatPemilik,
                'PemilikKTP' => $request->ktpPemilik,
                'PemilikKTPFile' => $PemilikKTPFile,
                'password' => $request->password,
                'supSetuju' => $request->setuju,
                'unit' => $request->unit,
            ]
        ]);
        
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            //dd($request->file('lampiranSiup')->store('lampiranSiup'));
            if($dtAPi['kode']=="90"){
                // Bekerja dengan upload file
                Storage::delete($lampiranSiup);
                Storage::delete($lampiranNPWP);
                Storage::delete($lampiranNPPKP);
                Storage::delete($lampiranTDP);
                Storage::delete($pemimpinKTPFile);
                Storage::delete($PemilikKTPFile);        
                // end bekerja dengan upload file
            }            
            $model = $dtAPi;     
            $message = "";
        }else{            
            //dd($dtAPi['pesan']);
            $model = "";     
            $message = $dtAPi['message']; 
            // Bekerja dengan upload file
            Storage::delete($lampiranSiup);
            Storage::delete($lampiranNPWP);
            Storage::delete($lampiranNPPKP);
            Storage::delete($lampiranTDP);
            Storage::delete($pemimpinKTPFile);
            Storage::delete($PemilikKTPFile);        
            // end bekerja dengan upload file
        }        
        return view('register.responseregister', ['model'=>$model, 'message'=>$message]);
    }
    
    public function kabupaten(Request $request){
        #$select     = $request->select;
        #$value      = $request->value;
        #$dependent  = $request->dependent;
        
        $client = new Client();
        $urle = env('API_BASE_URL')."dtkabupaten";
        $response = $client->request('POST', $urle, [
            'form_params' => [
                'value' => $request->value,
            ]
        ]);        
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        if($responStatus=='200'){
            if($dtAPi['kode']=="99"){
                $output = "<option value=''>Silakan Pilih</option>";
                foreach($dtAPi['dtKab'] as $dtKab){
                    $output .= "<option value='".$dtKab['mkabId']."'>".$dtKab['mkabNama']."</option>";
                }
            }else{
                $output = "";
            }
        }else{
            $output = "";
        }
        echo $output;
    }
    
    public function kecamatan(Request $request){
        #$select     = $request->select;
        #$value      = $request->value;
        #$dependent  = $request->dependent;
        
        $client = new Client();
        $urle = env('API_BASE_URL')."dtkecamatan";
        $response = $client->request('POST', $urle, [
            'form_params' => [
                'value' => $request->value,
            ]
        ]);        
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        if($responStatus=='200'){
            if($dtAPi['kode']=="99"){
                $output = "<option value=''>Silakan Pilih</option>";
                foreach($dtAPi['dtKec'] as $dtKec){
                    $output .= "<option value='".$dtKec['mkecId']."'>".$dtKec['mkecNama']."</option>";
                }
            }else{
                $output = "";
            }
        }else{
            $output = "";
        }
        echo $output;
    }
    
    public function active($email){
        $client = new Client();
        $urle = env('API_BASE_URL')."active";
        $response = $client->request('POST', $urle, [
            'form_params' => [
                'email' => $email,
            ]
        ]);        
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        #dd($dtAPi);
        if($responStatus=='200'){
            if($dtAPi['kode']=="99"){
                $model = $dtAPi;
                $message =  "Verifikasi email sukses, Terima kasih and telah melakukan verfikasi email. Kami akan melakukan proses velidasi data anda sebagai calon vendor PT Semen Indonesia Logistik Group";
            }else{
                $model = $dtAPi;
                $message = $dtAPi['pesan']."Verifikasi email gagal, Mohon maaf atas ketidak nyamanan ini. Silakan lakukan verikasi email beberapa saat lagi !";
            }
        }else{
            $model = "";
            $message = "";
        }
        return view('register.responseactive', ['model'=>$model, 'message'=>$message]);
    }
}

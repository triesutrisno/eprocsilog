<?php

namespace App\Http\Controllers\pp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Storage;

class PermintaanpembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $urle = env('API_BASE_URL')."permintaanpembayaran";
        $response = Http::post($urle,[
            'email' => session('user')->username,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
        }else{
            $model = array();
        }
        //dd($kode2);
        
	return view('pp.index',['model' => $model]);
    }
    
    public function home()
    {
        $urle = env('API_BASE_URL')."permintaanpembayaran";
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
        }else{
            $model = array();
        }
        //dd($kode2);
        
	return view('pp.home',['model' => $model]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        return view('pp.create');
    }
    
    public function getop(Request $request)
    {
        //dd($request->all());
        $urle = "http://172.20.145.36/eproc/getOP.php";
        $respons = Http::withHeaders([
                        'Content-Type' => 'application/json',
                        'token' => 'tiketing.silog.co.id'
                    ])
                    ->post($urle,[
                        'doco' => $request->doco,
                        'dcto' => $request->dcto
        ]);
        $dtAPi = json_decode($respons->getBody()->getContents(),true);  
        $responStatus= $respons->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $table = "<table class='display nowrap table table-hover table-striped table-bordered' cellspacing='0' width='100%' id='myTable'>";
            $table .= "<thead>";
            $table .= "<tr>";
            $table .= "<th align='center'>No</th>";
            $table .= "<th>No Dokument</th>";
            $table .= "<th>Type</th>";
            $table .= "<th>Line</th>";
            $table .= "<th>Kode Item</th>";
            $table .= "<th>Nama Item</th>";
            $table .= "<th>Qty</th>";
            $table .= "<th>Curr</th>";
            $table .= "<th>Satuan</th>";
            $table .= "<th>Nilai Total</th>";
            $table .= "<th>Nilai Open</th>";
            $table .= "<th>Nilai Pengajuan</th>";
            $table .= "</tr>";
            $table .= "</thead>";
            $jml = count($dtAPi['data']);
            $no=1;
            //dd($jml);
            if($jml>0){
                foreach($dtAPi['data'] as $key => $val){   
                    $var = $val['itemKode']."-".$val['line']."-".$val['curr']."-".$val['qty']."-".$val['satuan']."-".$val['totalNilai']."-".$val['itemNama'];
                    $table .= "<tr class='myTr'>";
                    $table .= "<td align='center'>".$no."</td>";
                    $table .= "<td>".$val['noDoc']."</td>";
                    $table .= "<td>".$val['typeDoc']."</td>";
                    $table .= "<td>".$val['line']."</td>";
                    $table .= "<td>".$val['itemKode']."</td>";
                    $table .= "<td>".$val['itemNama']."</td>";
                    $table .= "<td>".$val['qty']."</td>";
                    $table .= "<td>".$val['curr']."</td>";
                    $table .= "<td>".$val['satuan']."</td>";
                    $table .= "<td>".number_format($val['totalNilai'],2,',','.')."</td>";
                    $table .= "<td id='".$key."' data-id='".$val['openNilai']."'>".number_format($val['openNilai'],2,',','.')."</td>";
                    if($val['openNilai']>0){
                        $table .= "<td><input type='text' id='$var' name='nilaiRequest[$var]' class='form-control' onchange='cekNominal(this.value,".$key.")'></td>";
                    }else{
                        $table .= "<td>0</td>";
                    }
                    $table .= "</tr>";
                    $no++;
                }
            }else{
                $table .= "<tr>";
                $table .= "<td colspan='12' align='center'>Data tidak ada !</td>";
                $table .= "</tr>";
            }
            $table .= "</table>";
        }else{
            $table = "<table>";
            $table .= "<thead>";
            $table .= "<tr>";
            $table .= "<th align='center'>No</th>";
            $table .= "<th>No Dokument</th>";
            $table .= "<th>Type</th>";
            $table .= "<th>Line</th>";
            $table .= "<th>Kode Item</th>";
            $table .= "<th>Nama Item</th>";
            $table .= "<th>Qty</th>";
            $table .= "<th>Curr</th>";
            $table .= "<th>Satuan</th>";
            $table .= "<th>Nilai Total</th>";
            $table .= "<th>Nilai Open</th>";
            $table .= "<th>Nilai Pengajuan</th>";
            $table .= "</tr>";
            $table .= "</thead>";
        }
        //return $table;
        echo $table;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pathFile = "pp/".date('Ymd')."/".$request->comp;
        $ppFile = $request->file('ppFile') != NULL ? $request->file('ppFile')->store($pathFile) : "";
        //dd($ppFile);
        //dd($request->all());
        $urle = env('API_BASE_URL')."postpermintaanpembayaran";        
        $response = Http::post($urle,[
            'comp' => $request->comp,
            'noDokumen' => $request->noDokumen,
            'typeDokumen' => $request->typeDokumen,
            'keterangan' => $request->keterangan,            
            'noInvoice' => $request->noInvoice,            
            'ppFile' => $ppFile,
            'userEmail' => session('user')->username,
            'nilaiRequest' => $request->nilaiRequest,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            if($dtAPi['kode']=="90"){
                // Bekerja dengan upload file
                Storage::delete($ppFile);
                // end bekerja dengan upload file
            } 
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
            // Bekerja dengan upload file
            Storage::delete($ppFile);
            // end bekerja dengan upload file
        }
        
        return redirect('/permintaanpembayaran/'.session('user')->username)->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $urle = env('API_BASE_URL')."permintaanpembayaranshow/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $jml = count($dtAPi['pp'][0]['ppdetail']);
            return view('pp.show',['model' => $model, 'jml'=>$jml]);
        }else{
            return redirect('/permintaanpembayaran/')->with(['kode'=>'90', 'pesan'=>'Mohon maaf ada kesalahan sistem']);
        }
    }
    
    public function detail($email,$id)
    {
        $urle = env('API_BASE_URL')."permintaanpembayarandetail/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $jml = count($dtAPi['pp'][0]['ppdetail']);
            return view('pp.detail',['model' => $model, 'jml'=>$jml]);
        }else{
            return redirect('/permintaanpembayaran/'.session('user')->username)->with(['kode'=>'90', 'pesan'=>'Mohon maaf ada kesalahan sistem']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($email,$id)
    {        
        $urle = env('API_BASE_URL')."permintaanpembayaranedit/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi['pp'][0]['status']);
        
        if($responStatus=='200'){
            $model = $dtAPi;
        }else{
            return redirect('/permintaanpembayaran/'.session('user')->username)->with(['kode'=>'90', 'pesan'=>'Mohon maaf ada kesalahan sistem']);
        }
        if($model['pp'][0]['status']=='1'){     
            $urle2 = "http://172.20.145.36/eproc/getOPEdit.php";
            $respons2 = Http::withHeaders([
                            'Content-Type' => 'application/json',
                            'token' => 'tiketing.silog.co.id'
                        ])
                        ->post($urle2,[
                            'doco' => $model['pp'][0]['noDokumen'],
                            'dcto' => $model['pp'][0]['typeDokumen'],
                            'id' => $id
            ]);
            $dtAPi2 = json_decode($respons2->getBody()->getContents(),true);  
            $responStatus2= $respons2->getStatusCode();
            if($responStatus2=='200'){
                $model2 = $dtAPi2['data'];
            }else{
                $model2 = "";
            }
            
            $jml = count($dtAPi['pp'][0]['ppdetail']);
            return view('pp.edit',['model' => $model, 'model2' => $model2, 'jml'=>$jml]);
        }else{
            return redirect('/permintaanpembayaran/'.session('user')->username)->with(['kode'=>'90', 'pesan'=>'Mohon maaf data tidak bisa diupdate']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $email, $id)
    {
        $pathFile = "pp/".date('Ymd')."/".$request->comp;
        $ppFile = $request->file('ppFile') != NULL ? $request->file('ppFile')->store($pathFile) : "";
        //dd($ppFile);
        //dd($request->all());
        $urle = env('API_BASE_URL')."postpermintaanpembayaranedit";    
        $response = Http::post($urle,[
            'ppId' => $id,
            'comp' => $request->comp,
            'keterangan' => $request->keterangan,            
            'noInvoice' => $request->noInvoice,            
            'ppFile' => $ppFile,
            'nilaiRequest' => $request->nilaiRequest,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            /*if($dtAPi['kode']=="90"){
                if($ppFile!=""){
                    // Bekerja dengan upload file
                    Storage::delete($ppFile);
                    // end bekerja dengan upload file
                }
            } */
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
            // Bekerja dengan upload file
            Storage::delete($ppFile);
            // end bekerja dengan upload file
        }
        
        return redirect('/permintaanpembayaran/'.session('user')->username)->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($email,$id)
    {
        //dd($id);
        $urle = env('API_BASE_URL')."permintaanpembayaranhapus/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($id);
        if($responStatus=='200'){
            $kode   = $dtAPi['kode'];
            $pesan  = $dtAPi['pesan'];
        }else{
            $kode   = "";
            $pesan  = "";
        }
        return redirect('/permintaanpembayaran/'.session('user')->username)->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }
    
    public function complite($id){
        $urle = env('API_BASE_URL')."permintaanpembayarancomplite/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($id);
        if($responStatus=='200'){
            $kode   = $dtAPi['kode'];
            $pesan  = $dtAPi['pesan'];
        }else{
            $kode   = "";
            $pesan  = "";
        }
        return ['kode'=>$kode, 'pesan'=>$pesan];
        //return redirect('/permintaanpembayaran/')->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }
    
    public function nocomplite($id){
        $urle = env('API_BASE_URL')."permintaanpembayarannocomplite/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi['pp'];
        }else{
            return redirect('/permintaanpembayaran/')->with(['kode'=>'90', 'pesan'=>'Mohon maaf ada kesalahan sistem']);
        }
        
        if($model[0]['status']=='1'){            
            //$jml = count($dtAPi['pp'][0]['ppdetail']);
            return view('pp.nokomplit',['model' => $model]);
        }else{
            return redirect('/permintaanpembayaran/')->with(['kode'=>'90', 'pesan'=>'Mohon maaf data tidak bisa proses']);
        }
    }
    
    public function nocomplitepost(Request $request,$id){
        $urle = env('API_BASE_URL')."permintaanpembayarannocomplitepost";
        $response = Http::post($urle,[
            'ppId' => $id,
            'keterangan' => $request->keterangan,
            'email' => $request->email,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
        }
        
        return redirect('/permintaanpembayaran/')->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }
    
    public function flash($id,$kode,$pesan){
        //dd($id);
        return redirect('/permintaanpembayaran/detail/'.$id)->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }
    
    public function addnopp($id){
        $urle = env('API_BASE_URL')."permintaanpembayarannocomplite/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi['pp'];
        }else{
            return redirect('/permintaanpembayaran/')->with(['kode'=>'90', 'pesan'=>'Mohon maaf ada kesalahan sistem']);
        }
        
        if($model[0]['status']=='2'){            
            //$jml = count($dtAPi['pp'][0]['ppdetail']);
            return view('pp.addnopp',['model' => $model]);
        }else{
            return redirect('/permintaanpembayaran/')->with(['kode'=>'90', 'pesan'=>'Mohon maaf data tidak bisa proses']);
        }
    }
    
    public function addnopppost(Request $request,$id){
        $urle = env('API_BASE_URL')."permintaanpembayaranaddnopppost";
        $response = Http::post($urle,[
            'ppId' => $id,
            'noPP' => $request->nopp,
            'typeDokPP' => $request->typeDokPP,
        ]);
        //dd($request->all());
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
        }
        
        return redirect('/permintaanpembayaran/')->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }
}

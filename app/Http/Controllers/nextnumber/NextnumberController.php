<?php

namespace App\Http\Controllers\nextnumber;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class NextnumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $urle = env('API_BASE_URL')."nextnumber";
        //dd($urle);
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            #$pesan = "";
        }else{
            $model = array();
            #$pesan = "";
        }
        if(session('pesan2')!=""){
            $kode   = session('kode2');
            $pesan  = session('pesan2');
        }else{
            $kode   = "";
            $pesan  = "";
        }
        //dd($kode);
	return view('nextnumber.index',['model' => $model, 'kode'=>$kode, 'pesan'=>$pesan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $urlUnit = env('API_BASE_URL')."dtUnit";
        $responsUnit = Http::post($urlUnit);
        $dtAPiUnit = json_decode($responsUnit->getBody()->getContents(),true);  
        $responStatusUnit= $responsUnit->getStatusCode();
        //dd($dtAPiCurr);
        if($responStatusUnit=='200'){
            $dtUnit = $dtAPiUnit;
        }else{
            $dtUnit = array();
        }
        return view('nextnumber.create',['dtUnit'=>$dtUnit]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $urle = env('API_BASE_URL')."nextnumberpost";
        $response = Http::post($urle,[
            'comp' => $request->comp,
            'tahun' => $request->tahun,
            'kode' => $request->kode,
            'jenis' => $request->jenis,
            'keterangan' => $request->keterangan,
            'urutan' => $request->urutan,
            'status' => $request->status,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $kode  = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
        }
        
        return redirect('/nextnumber')->with(['kode2'=>$kode,'pesan2'=>$pesan]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $urle = env('API_BASE_URL')."nextnumberedit/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        
        $urlUnit = env('API_BASE_URL')."dtUnit";
        $responsUnit = Http::post($urlUnit);
        $dtAPiUnit = json_decode($responsUnit->getBody()->getContents(),true);  
        $responStatusUnit= $responsUnit->getStatusCode();
        //dd($dtAPiCurr);
        if($responStatusUnit=='200'){
            $dtUnit = $dtAPiUnit;
        }else{
            $dtUnit = array();
        }
        
	return view('nextnumber.edit',['model' => $model, 'dtUnit'=>$dtUnit, 'pesan'=>$pesan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $urle = env('API_BASE_URL')."nextnumbereditpost";
        $response = Http::post($urle,[
            'comp' => $request->comp,
            'tahun' => $request->tahun,
            'kode' => $request->kode,
            'jenis' => $request->jenis,
            'keterangan' => $request->keterangan,
            'urutan' => $request->urutan,
            'status' => $request->status,
            'id' => $id,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $kode   = $dtAPi['kode'];
            $pesan  = $dtAPi['pesan'];
        }else{
            $kode   = "";
            $pesan  = "";
        }
        
        return redirect('/nextnumber')->with(['kode2'=>$kode, 'pesan2'=>$pesan]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $urle = env('API_BASE_URL')."nextnumberhapus/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $kode   = $dtAPi['kode'];
            $pesan  = $dtAPi['pesan'];
        }else{
            $kode   = "";
            $pesan  = "";
        }
        return redirect('/nextnumber')->with(['kode2'=>$kode, 'pesan2'=>$pesan]);
    }
}

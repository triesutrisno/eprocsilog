<?php

namespace App\Http\Controllers\Tender;

use App\Http\Controllers\Controller;
use App\Http\Models\Tender\Tenderdetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Storage;

class TenderdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(session('user')->username);
        $urle = env('API_BASE_URL')."tenderdetail";
        $response = Http::post($urle,[
            'email' => session('user')->username,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        
        if(session('pesan2')!=""){
            $kode2 = session('kode2');
            $pesan2 = session('pesan2');
        }else{
            $kode2 = "";
            $pesan2 = "";
        }
        //dd($kode2);
        
	return view('tenderdetail.index',['model' => $model, 'pesan'=>$pesan, 'kode2'=>$kode2, 'pesan2'=>$pesan2]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Tender\Tenderdetail  $tenderdetail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $urle = env('API_BASE_URL')."tenderdetailshow/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        $kode = session('kode')!="" ? session('kode') : "";
        $pesan = session('pesan')!="" ? session('pesan') : "";
	return view('tenderdetail.detail',['model' => $model, 'kode'=>$kode,  'pesan'=>$pesan]);
    }
    
    public function detail($id)
    {
        $urle = env('API_BASE_URL')."tenderdetailshow/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        $kode = session('kode')!="" ? session('kode') : "";
        $pesan = session('pesan')!="" ? session('pesan') : "";
	return view('tenderdetail.detailtender',['model' => $model, 'kode'=>$kode,  'pesan'=>$pesan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Tender\Tenderdetail  $tenderdetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {              
        $urle = env('API_BASE_URL')."tenderdetailedit/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        
        $nowDate = date("Y-m-d H:i:s");
        if($nowDate > $dtAPi['tenderDetail'][0]['tender'][0]['tglTutup']){
            return redirect('/kegiatanpengadaan/'.session('user')->username)->with(['kode2'=>'90', 'pesan2'=>'Kegiatan pengadaan sudah ditutup']);
        }

        return view('tenderdetail.penawaran',['model' => $model, 'pesan'=>$pesan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Tender\Tenderdetail  $tenderdetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $pathFile = "penawaran/file/".$id;
        $pathGambar = "penawaran/gambar/".$id;
        $tenderDetailFile = $request->file('penawaranFile') != NULL ? $request->file('penawaranFile')->store($pathFile) : "";
        $tenderDetailGambar = $request->file('penawaranGambar') != NULL ? $request->file('penawaranGambar')->store($pathGambar) : "";

        $urle = env('API_BASE_URL')."tenderdetaileditpost";
        $response = Http::post($urle,[
            'penawaranCurency' => $request->penawaranCurency,
            'penawaranHarga' => $request->penawaranHarga,
            'penawaranExpired' => $request->penawaranExpired,
            'penawaranSpesifikasi' => $request->penawaranSpesifikasi,
            'penawaranFile' => $tenderDetailFile,
            'penawaranGambar' => $tenderDetailGambar,
            'tenderPenawaranId' => $id,
        ]);

        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
            if($dtAPi['kode']=="90"){
                // Bekerja dengan upload file
                Storage::delete($tenderDetailFile);
                Storage::delete($tenderDetailGambar);   
                // end bekerja dengan upload file
            } 
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
            
            // Bekerja dengan upload file
            Storage::delete($tenderDetailFile);
            Storage::delete($tenderDetailGambar);   
            // end bekerja dengan upload file
        }
        
        return redirect('/kegiatanpengadaan/'.session('user')->username)->with(['kode2'=>$kode, 'pesan2'=>$pesan]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Tender\Tenderdetail  $tenderdetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tenderdetail $tenderdetail)
    {
        //
    }
    
    
    
    public function negosiasi($id)
    {        
        $urle = env('API_BASE_URL')."tenderdetailedit/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        
        $nowDate = date("Y-m-d H:i:s");
        //dd($nowDate);
        if($nowDate >= $dtAPi['tenderDetail'][0]['tender'][0]['tglNegoAwal'] && $nowDate <= $dtAPi['tenderDetail'][0]['tender'][0]['tglNegoAkhir']){
            return view('tenderdetail.negosiasi',['model' => $model, 'pesan'=>$pesan]);
        }elseif($nowDate > $dtAPi['tenderDetail'][0]['tender'][0]['tglNegoAkhir']){
            return redirect('/kegiatanpengadaan/'.session('user')->username)->with(['kode2'=>'90', 'pesan2'=>'Kegiatan Negosiasi sudah ditutup']);
        }else{
            return redirect('/kegiatanpengadaan/'.session('user')->username)->with(['kode2'=>'90', 'pesan2'=>'Kegiatan Negosiasi belum dimulai']);
        }
    }
    
    public function negosiasipost(Request $request, $id)
    {
        $urle = env('API_BASE_URL')."tenderdetailnegosiasipost";
        $response = Http::post($urle,[
            'penawaranNego' => $request->penawaranNego,
            'tglNego' => date("Y-m-d H:i:s"),
            'tenderPenawaranId' => $id,
        ]);

        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
        }
        
        return redirect('/kegiatanpengadaan/'.session('user')->username)->with(['kode2'=>$kode, 'pesan2'=>$pesan]);
    }
}

<?php

namespace App\Http\Controllers\tender;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $urle = env('API_BASE_URL')."tender";
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd();
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        if(session('pesan2')!=""){
            $kode2 = session('kode2');
            $pesan2 = session('pesan2');
        }else{
            $kode2 = "";
            $pesan2 = "";
        }
        
    	return view('tender.index',['model' => $model, 'pesan'=>$pesan, 'kode2'=>$kode2, 'pesan2'=>$pesan2]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $urle = env('API_BASE_URL')."dtKategori";
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        if($responStatus=='200'){
            $dtKat = $dtAPi;
        }else{
            $dtKat = array();
        }
        
        $urlCurr = env('API_BASE_URL')."dtCurrency";
        $responseCurr = Http::post($urlCurr);
        $dtAPiCurr = json_decode($responseCurr->getBody()->getContents(),true);  
        $responStatusCurr = $responseCurr->getStatusCode();
        //dd($dtAPiCurr);
        if($responStatusCurr=='200'){
            $dtCurr = $dtAPiCurr;
        }else{
            $dtCurr = array();
        }
        
        $urlUnit = env('API_BASE_URL')."dtUnit";
        $responsUnit = Http::post($urlUnit);
        $dtAPiUnit = json_decode($responsUnit->getBody()->getContents(),true);  
        $responStatusUnit= $responsUnit->getStatusCode();
        //dd($dtAPiCurr);
        if($responStatusUnit=='200'){
            $dtUnit = $dtAPiUnit;
        }else{
            $dtUnit = array();
        }
        
        return view('tender.create',['dtKat'=>$dtKat, 'dtCurr'=>$dtCurr, 'dtUnit'=>$dtUnit]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //dd($request->all());
        
        $pathFile = "tender/file/".date('Ymd')."/".$request->comp;
        $pathGambar = "tender/gambar/".date('Ymd')."/".$request->comp;
        $tenderFile = $request->file('tenderFile') != NULL ? $request->file('tenderFile')->store($pathFile) : "";
        $tenderGambar = $request->file('tenderGambar') != NULL ? $request->file('tenderGambar')->store($pathGambar) : "";

        $urle = env('API_BASE_URL')."tenderpost";        
        $response = Http::post($urle,[
            'unitId' => $request->unitId,
            'comp' => $request->comp,
            'tenderTahun' => $request->tenderTahun,
            'tenderNama' => $request->tenderNama,
            'tenderJenis' => $request->tenderJenis,
            'tenderSpesifikasi' => $request->tenderSpesifikasi,
            'tenderCurency' => $request->tenderCurency,
            'tenderBudget' => $request->tenderBudget,
            'statusBudget' => $request->statusBudget,
            'tenderType' => $request->tenderType,
            'mkatId' => $request->mkatId,
            'tenderFile' => $tenderFile,
            'tenderGambar' => $tenderGambar,
            'tglTutup' => $request->tglTutup,
            'tglAnwijzing' => $request->tglAnwijzing,
            'lokasiAnwijzing' => $request->lokasiAnwijzing,
            'tenderStatus' => '1',
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            #if($dtAPi['kode']=='99'){
            #    $pathFile = "tenderFile/".$dtAPi['tenderId'];
            #    $pathGambar = "tenderGambar/".$dtAPi['tenderId'];
            #    $tenderFile = $request->file('tenderFile') != NULL ? $request->file('tenderFile')->store($pathFile) : "";
            #    $tenderGambar = $request->file('tenderGambar') != NULL ? $request->file('tenderGambar')->store($pathGambar) : "";
            #}
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
        }
        
        return redirect('/tender')->with(['kode2'=>$kode, 'pesan2'=>$pesan]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $urle = env('API_BASE_URL')."tendershow/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        $kode = session('kode')!="" ? session('kode') : "";
        $pesan = session('pesan')!="" ? session('pesan') : "";
	return view('tender.detail',['model' => $model, 'kode'=>$kode,  'pesan'=>$pesan]);
    }
    
    public function detail($id)
    {
        $urle = env('API_BASE_URL')."tenderdetailshow/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        $kode = session('kode')!="" ? session('kode') : "";
        $pesan = session('pesan')!="" ? session('pesan') : "";
	return view('tender.detailtender',['model' => $model, 'kode'=>$kode,  'pesan'=>$pesan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $urle = env('API_BASE_URL')."tenderedit/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        
        return view('tender.edit',['model' => $model, 'pesan'=>$pesan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pathFile = "tender/file/".date('Ymd')."/".$request->comp;
        $pathGambar = "tender/gambar/".date('Ymd')."/".$request->comp;
        $tenderFile = $request->file('tenderFile') != NULL ? $request->file('tenderFile')->store($pathFile) : "";
        $tenderGambar = $request->file('tenderGambar') != NULL ? $request->file('tenderGambar')->store($pathGambar) : "";

        $urle = env('API_BASE_URL')."tendereditpost";
        $response = Http::post($urle,[
            'comp' => $request->comp,
            'tenderTahun' => $request->tenderTahun,
            'tenderNama' => $request->tenderNama,
            'tenderJenis' => $request->tenderJenis,
            'tenderSpesifikasi' => $request->tenderSpesifikasi,
            'tenderCurency' => $request->tenderCurency,
            'tenderBudget' => $request->tenderBudget,
            'statusBudget' => $request->statusBudget,
            'tenderType' => $request->tenderType,
            'mkatId' => $request->mkatId,
            'tenderReff1' => $request->tenderReff1,
            'tenderReff2' => $request->tenderReff2,
            'tenderFile' => $tenderFile,
            'tenderGambar' => $tenderGambar,
            'tglTutup' => $request->tglTutup,
            'tglAnwijzing' => $request->tglAnwijzing,
            'lokasiAnwijzing' => $request->lokasiAnwijzing,
            'tenderStatus' => $request->tenderStatus,
            'tenderId' => $id,
        ]);

        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
        }
        
        return redirect('/tender')->with(['kode2'=>$kode, 'pesan2'=>$pesan]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $urle = env('API_BASE_URL')."tenderhapus/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $pesan = $dtAPi['pesan'];
        }else{
            $pesan = "";
        }
        return redirect('/tender')->with(['pesan2'=>$pesan]);
    }
    
    public function kirimemail(Request $request, $id)
    {
        $kategori = isset($request->kategori) ? $request->kategori : "";
        $vendor = isset($request->vendor) ? $request->vendor : "";
        
        if($kategori!="" || $vendor!=""){
            $urle = env('API_BASE_URL')."tenderkirimemail";
            $response = Http::post($urle,[
                'kategori' => $kategori,
                'vendor' => $vendor,
                'tenderId' => $id,
            ]);
            $dtAPi = json_decode($response->getBody()->getContents(),true);  
            $responStatus = $response->getStatusCode();
            //dd($dtAPi);
            if($responStatus=='200'){
                $kode   = $dtAPi['kode'];
                $pesan  = $dtAPi['pesan'];
            }else{
                $kode   = "90";
                $pesan  = "Error Sistem";
            }
            return redirect('/tender/detail/'.$id)->with(['kode'=>$kode, 'pesan'=>$pesan]);
        }else{
            return redirect('/tender/detail/'.$id)->with(['kode'=>'90', 'pesan'=> 'Tidak ada email yang dikirim']);
        }
    }
    
    public function waktunegoall(Request $request, $id)
    {
        $urle = env('API_BASE_URL')."waktunego";
        $response = Http::post($urle,[
            'tenderId' => $id,
            'tglNegoAwal' => $request->tglNego1,
            'tglNegoAkhir' => $request->tglNego2,
            'supplier' => $request->supplier,
            'all'=>'1',
        ]);

        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
        }
        
        return redirect('/tender/detail/'.$id)->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }
    
    public function pemenang(Request $request, $id,$id2)
    {
        
        $urle = env('API_BASE_URL')."pemenang";
        $response = Http::post($urle,[
            'tenderId' => $id,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model  = $dtAPi;
            $jml    = count($dtAPi['tenderDetail']);
            $kode   = "99";
            $pesan  = "Data Ditemukan";
        }else{
            $model = "";
            $kode   = "90";
            $jml    = "0";
            $pesan  = "Error Sistem";
        }
        return view('tender.pemenang',['model' => $model, 'pesan'=>$pesan, 'id2'=>$id2, 'jml'=>$jml]);
    }
    
    public function pemenangpost(Request $request, $id)
    {
        
        $urle = env('API_BASE_URL')."pemenangpost";
        $response = Http::post($urle,[
            'tenderId' => $id,
            'tenderRemark1' =>  $request->tenderRemark1,
            'tenderRemark2' =>  $request->tenderRemark2,
            'tenderPenawaranId' => $request->tenderSupplier,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $model = "";
            $kode   = "90";
            $pesan  = "Error Sistem";
        }
        return redirect('/tender/detail/'.$id)->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }
}

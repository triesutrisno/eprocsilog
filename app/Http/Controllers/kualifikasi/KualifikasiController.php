<?php

namespace App\Http\Controllers\Kualifikasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class KualifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $urle = env('API_BASE_URL')."kualifikasi";
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd();
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        if(session('pesan2')!=""){
            $pesan2 = session('pesan2');
        }else{
            $pesan2 = "";
        }
        
	return view('kualifikasi.index',['model' => $model, 'pesan'=>$pesan, 'pesan2'=>$pesan2]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kualifikasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $urle = env('API_BASE_URL')."kualifikasipost";
        $response = Http::post($urle,[
            'mkualNama' => $request->nmKualifikasi,
            'mkualStatus' => $request->status,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        #dd($dtAPi);
        if($responStatus=='200'){
            $pesan = $dtAPi['pesan'];
        }else{
            $pesan = "";
        }
        return redirect('/kualifikasi')->with(['kode'=>'98','pesan2'=>$pesan]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $urle = env('API_BASE_URL')."kualifikasiedit/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $pesan = "";
        }else{
            $model = array();
            $pesan = "";
        }
        
	return view('kualifikasi.edit',['model' => $model, 'pesan'=>$pesan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $urle = env('API_BASE_URL')."kualifikasieditpost";
        $response = Http::post($urle,[
            'mkualNama' => $request->nmKualifikasi,
            'mkualStatus' => $request->status,
            'mkualId' => $id,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $pesan = $dtAPi['pesan'];
        }else{
            $pesan = "";
        }
        return redirect('/kualifikasi')->with(['kode'=>'98','pesan2'=>$pesan]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $urle = env('API_BASE_URL')."kualifikasihapus/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $pesan = $dtAPi['pesan'];
        }else{
            $pesan = "";
        }
        return redirect('/kualifikasi')->with(['pesan2'=>$pesan]);
    }
}

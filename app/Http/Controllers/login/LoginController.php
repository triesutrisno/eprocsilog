<?php

namespace App\Http\Controllers\login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Validator;
use App\User;
use Illuminate\Support\Facades\Http;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
        ]); 
        

//        $curl = curl_init();
//
//        curl_setopt_array($curl, array(
//          CURLOPT_URL => "http://localhost/eprocsilogservice/public/api/login",
//          CURLOPT_RETURNTRANSFER => true,
//          CURLOPT_ENCODING => "",
//          CURLOPT_MAXREDIRS => 10,
//          CURLOPT_TIMEOUT => 0,
//          CURLOPT_FOLLOWLOCATION => true,
//          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//          CURLOPT_CUSTOMREQUEST => "POST",
//          CURLOPT_POSTFIELDS => array('email' => 'triesutrisno@gmail.com','password' => 'sakhiy12'),
//        ));
//
//        $response = curl_exec($curl);
//
//        curl_close($curl);
//        echo $response;
//        
//        die();

        
        $urle = env('API_BASE_URL')."login";
        $response = Http::post($urle, [
                    'email' => $request->email,
                    'password' => $request->password,
        ]);
        
        //try {
//            $client = new Client();
//            $response = $client->request('POST', 'http://localhost/eprocsilogservice/public/api/login', [
//                'form_params' => [
//                    'email' => $request->email,
//                    'password' => $request->password,
//                ]
//            ]);
        //} catch (\GuzzleHttp\Exception\RequestException $e) {
        //    $response = $e->getResponse();
        //}               
//        echo($response->getBody());
//        die();
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();  
        //dd($dtAPi);
        if($responStatus=='200'){
            session(['token' => $dtAPi['data']['token']]);
            $user = new User();
            $user->id = $dtAPi['data']['id'];
            $user->name = $dtAPi['data']['namaUser'];
            $user->username = $dtAPi['data']['email'];
            $user->role = $dtAPi['data']['role'];
            $user->statusTMS = $dtAPi['data']['statusTMS'];
            $user->email = $dtAPi['data']['email'];
            Auth::login($user);
            Auth::setUser($user);
            $request->session()->put('user', $user);
            return redirect("/");
        }else{            
            //dd($dtAPi['pesan']);
            $model = "";     
            $pesan = $dtAPi['pesan'];
            //return view('login.index', ['message'=>$pesan]);
            return redirect('login')->with('pesan', $pesan);
        }        
    }
}

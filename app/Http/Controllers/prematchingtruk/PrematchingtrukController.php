<?php

namespace App\Http\Controllers\Prematchingtruk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Storage;

class PrematchingtrukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $urle = env('API_BASE_URL')."prematchingtruk";
        $response = Http::post($urle,[
            'email' => session('user')->username,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        #dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
        }else{
            $model = array();
        }
        //dd($kode2);
        
	return view('prematchingtruk.index',['model' => $model]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $urle = env('API_BASE_URL')."prematchcreate";
        $response = Http::post($urle,[
            'email' => session('user')->username,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);          
        #dd($dtAPi);
        $responStatus = $response->getStatusCode();
        
        if($responStatus=='200'){            
            $data = $dtAPi;
        }else{
            $data = array();
        }
        
        return view('prematchingtruk.create',['data' => $data]);
    }
    
    public function prematch($email,$id)
    {
        #dd($id);
        $urle = env('API_BASE_URL')."prematchcreate";
        $response = Http::post($urle,[
            'email' => session('user')->username,
            'noDO' => $id,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);
        #dd($dtAPi);
        $responStatus = $response->getStatusCode();
        if($responStatus=='200'){            
            $data = $dtAPi;
        }else{
            $data = array();
        }
        
        return view('prematchingtruk.add',['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #dd($request->all());
        $urle = env('API_BASE_URL')."prematchstore";        
        $response = Http::post($urle,[
            'kdVendor' => $request->kdVendor,
            'nopol' => $request->nopol,
            'driver' => $request->driver,
            'doSilog' => $request->doSilog,
            'tglRencanKirim' => $request->tglRencanKirim,
            'kdShipto' => $request->kdShipto,
            'kdWilayah' => $request->kdWilayah,
            'kdItem1' => $request->kdItem1,
            'kdItem2' => $request->kdItem2,
            'kdItem3' => $request->kdItem3,
            'satuan' => $request->satuan,
            'qty' => $request->qty,
            'asal' => $request->asal,
            'motTransport' => $request->motTransport,
            'branchplant' => $request->branchplant,
            'rem' => $request->rem,
            'kemudi' => $request->kemudi,
            'ban' => $request->ban,
            'elektrik' => $request->elektrik,
            'kabin' => $request->kabin,
            'kebocoran' => $request->kebocoran,
            'kelengkapan' => $request->kelengkapan,
            'khusuDumpTruk' => $request->khusuDumpTruk,
            'khususBulkTruk' => $request->khususBulkTruk,
            'khususBulkTruk2' => $request->khususBulkTruk2,
            'setuju' => $request->setuju
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        #dd($response);
        if($responStatus=='200'){
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
        }
        
        return redirect('/prematchingtruk/'.session('user')->username)->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $urle = env('API_BASE_URL')."permintaanpembayaranshow/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $jml = count($dtAPi['pp'][0]['ppdetail']);
            return view('pp.show',['model' => $model, 'jml'=>$jml]);
        }else{
            return redirect('/permintaanpembayaran/')->with(['kode'=>'90', 'pesan'=>'Mohon maaf ada kesalahan sistem']);
        }
    }
    
    public function detail($email,$id)
    {
        $urle = env('API_BASE_URL')."permintaanpembayarandetail/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            $model = $dtAPi;
            $jml = count($dtAPi['pp'][0]['ppdetail']);
            return view('pp.detail',['model' => $model, 'jml'=>$jml]);
        }else{
            return redirect('/permintaanpembayaran/'.session('user')->username)->with(['kode'=>'90', 'pesan'=>'Mohon maaf ada kesalahan sistem']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($email,$id)
    {        
        $urle = env('API_BASE_URL')."permintaanpembayaranedit/".$id;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi['pp'][0]['status']);
        
        if($responStatus=='200'){
            $model = $dtAPi;
        }else{
            return redirect('/permintaanpembayaran/'.session('user')->username)->with(['kode'=>'90', 'pesan'=>'Mohon maaf ada kesalahan sistem']);
        }
        if($model['pp'][0]['status']=='1'){     
            $urle2 = "http://172.20.145.36/eproc/getOPEdit.php";
            $respons2 = Http::withHeaders([
                            'Content-Type' => 'application/json',
                            'token' => 'tiketing.silog.co.id'
                        ])
                        ->post($urle2,[
                            'doco' => $model['pp'][0]['noDokumen'],
                            'dcto' => $model['pp'][0]['typeDokumen'],
                            'id' => $id
            ]);
            $dtAPi2 = json_decode($respons2->getBody()->getContents(),true);  
            $responStatus2= $respons2->getStatusCode();
            if($responStatus2=='200'){
                $model2 = $dtAPi2['data'];
            }else{
                $model2 = "";
            }
            
            $jml = count($dtAPi['pp'][0]['ppdetail']);
            return view('pp.edit',['model' => $model, 'model2' => $model2, 'jml'=>$jml]);
        }else{
            return redirect('/permintaanpembayaran/'.session('user')->username)->with(['kode'=>'90', 'pesan'=>'Mohon maaf data tidak bisa diupdate']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $email, $id)
    {
        $pathFile = "pp/".date('Ymd')."/".$request->comp;
        $ppFile = $request->file('ppFile') != NULL ? $request->file('ppFile')->store($pathFile) : "";
        //dd($ppFile);
        //dd($request->all());
        $urle = env('API_BASE_URL')."postpermintaanpembayaranedit";    
        $response = Http::post($urle,[
            'ppId' => $id,
            'comp' => $request->comp,
            'keterangan' => $request->keterangan,            
            'noInvoice' => $request->noInvoice,            
            'ppFile' => $ppFile,
            'nilaiRequest' => $request->nilaiRequest,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        //dd($dtAPi);
        if($responStatus=='200'){
            /*if($dtAPi['kode']=="90"){
                if($ppFile!=""){
                    // Bekerja dengan upload file
                    Storage::delete($ppFile);
                    // end bekerja dengan upload file
                }
            } */
            $kode = $dtAPi['kode'];
            $pesan = $dtAPi['pesan'];
        }else{
            $kode = "90";
            $pesan = "Sistem Error";
            // Bekerja dengan upload file
            Storage::delete($ppFile);
            // end bekerja dengan upload file
        }
        
        return redirect('/permintaanpembayaran/'.session('user')->username)->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($email,$id,$doc,$dct,$lnid)
    {
        //dd($id);
        $urle = env('API_BASE_URL')."prematchhapus/".$id."/".$doc."/".$dct."/".$lnid;
        $response = Http::post($urle);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        $responStatus = $response->getStatusCode();
        #dd($response);
        if($responStatus=='200'){
            $kode   = $dtAPi['kode'];
            $pesan  = $dtAPi['pesan'];
        }else{
            $kode   = "";
            $pesan  = "";
        }
        return redirect('/prematchingtruk/'.session('user')->username)->with(['kode'=>$kode, 'pesan'=>$pesan]);
    }    
}

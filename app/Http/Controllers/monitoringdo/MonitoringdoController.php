<?php

namespace App\Http\Controllers\monitoringdo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;


class MonitoringdoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        #$config = new Config();
        #$node = 'tms-monitoring-do';
        #$config->setUsingToken(true);
        #$guzzleClient = GuzzleClient::create($config);
        
        //dd($request->all());
        //add date param
        $param["kdPlant"]  = $request->kdPlant != NULL ? $request->kdPlant : "";
        $param["tglA"]  = $request->tglA != NULL ? $request->tglA : date("Y-m-d");
        $param["tglB"]  = $request->tglB != NULL ? $request->tglB : date("Y-m-d");
        $param["nopin"] = $request->nopin != NULL ? $request->nopin : "";
        $param["noDo"] = $request->noDo != NULL ? $request->noDo : "";
        $param["status"]  = $request->status != NULL ? $request->status : "";
        $param["spjexternal"] = $request->spjexternal != NULL ? $request->spjexternal : "";        
        $param["email"] = session('user')->username;
        #dd($param);
        
        $urle = env('API_BASE_URL')."monitoringdo";
        $response = Http::post($urle,[
            #'email' => session('user')->username,
            'param' => $param,
        ]);
        $dtAPi = json_decode($response->getBody()->getContents(),true);  
        #dd($dtAPi);
        $responStatus = $response->getStatusCode();
        
        #$response = $guzzleClient->getClient()->post($node, ['form_params'=>$param] );
        #$resp = json_decode($response->getBody(), true);
        #dd($resp);
        #$statusCode = $response->getHeader('status_code')[0];  
        
        if($responStatus==200){
            $model = $dtAPi;
            $message = '';
        } else {
            $model = "";
            $message = $resp['message'];
        }
        
        return view('monitoringdo.index',['model'=>$model, 'message'=>$message, 'param'=>$param, 'kode'=>'', 'pesan'=>'']);
    }
}

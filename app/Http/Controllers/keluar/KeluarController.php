<?php

namespace App\Http\Controllers\keluar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Validator;
use App\User;
use Illuminate\Support\Facades\Http;

class KeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset(session('user')->username)){
            $urle = env('API_BASE_URL')."logout";
            $response = Http::post($urle,[
                'email' => session('user')->username,
            ]);
            $dtAPi = json_decode($response->getBody()->getContents(),true);  
            $responStatus = $response->getStatusCode(); 
        }
        //dd($responStatus);
        //if($responStatus=='200'){
            //Auth::logout(session('user'));
            session()->flush();
            return redirect('/login');
        //}        
    }
    
}

@extends('layouts.main')

@push('styles')
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Tambah Data Negosiasi</strong></h4>
                <div class="card-body">
                    <form action="{{ url('/kegiatanpengadaan/negosiasipost') }}/{{ $model['tenderDetail'][0]['tenderPenawaranId'] }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tenderTahun">Tahun : 
                                    {{ $model['tenderDetail'][0]['tender']['0']['tenderTahun'] }}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tenderComp">Compay : 
                                     @if($model['tenderDetail'][0]['tender'][0]['comp']=='10100')
                                        PT SEMEN INDONESIA LOGISTIK
                                    @elseif($model['tenderDetail'][0]['tender'][0]['comp']=='10200')
                                        PT SEMEN INDONESIA DISTRIBUTOR
                                    @elseif($model['tenderDetail'][0]['tender'][0]['comp']=='10300')
                                        PT VARIA USAHA DHARMA SEGARA
                                    @elseif($model['tenderDetail'][0]['tender'][0]['comp']=='10400')
                                        PT VARIA USAHA LINTAS SEGARA
                                    @elseif($model['tenderDetail'][0]['tender'][0]['comp']=='10500')
                                        PT VARIA USAHA BAHARI
                                    @else

                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tenderNama">Nama Kegiatan : 
                                    {{ $model['tenderDetail'][0]['tender']['0']['tenderNama'] }}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tenderSpesifikasi">Spesifikasi : 
                                    {{ $model['tenderDetail'][0]['tender']['0']['tenderSpesifikasi'] }}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="penawaranHarga">Harga Penawaran : 
                                    {{ number_format($model['tenderDetail'][0]['penawaranHarga'],2,',','.') }}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="penawaranCurrency">Currency : 
                                    {{ $model['tenderDetail'][0]['penawaranCurency'] }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="penawaranNego">Harga Nego : <span class="danger">*</span></label>
                                    <input type="text" class="form-control" required id="penawaranNego" name="penawaranNego" value="{{ $model['tenderDetail'][0]['penawaranNego'] }}"> 
                                </div>
                            </div>
                        </div>                        
                        <div class="form-group text-center m-t-20">
                            <div  class="col-lg-2 col-md-2">
                                <button class="btn btn-block btn-success" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('js/validation.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>    
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#penawaranExpired').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                //todayHighlight: true,
                //format: "mm-yyyy",
                //viewMode: "months", 
                //minViewMode: "months"
            });
            $('#input-group-append-id').click(function() {
                //alert("Bismillah !");
                $('#penawaranExpired').datepicker('show');
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
        
    </script>
@endpush
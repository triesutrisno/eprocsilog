@extends('layouts.main')

@push('styles')
    
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/switchery/dist/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />    
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('container')

@php
$nowDate = date("Y-m-d H:i:s");
#echo "Tanggal Hari ini = ".$nowDate;
@endphp

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Data Kegiatan Pengadaan</strong></h4>
                <div class="row button-group">
                    <div class="col-xs-2">
                        <a href="{{ url('/kegiatanpengadaan') }}/{{ session('user')->username }}" class="btn btn-info">Lihat Data</a> 
                    </div>
                </div>   
                <div class="card-body">
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#kegiatanPengadaan" role="tab">Kegiatan Pengadaan</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#mengikuti" role="tab">Mengikuti</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#negosiasi" role="tab">Negosiasi</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pemenang" role="tab">Pemenang</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#kalah" role="tab">Kalah</a> </li>
                    </ul>
                    <br />
                    @if($kode2=="99")
                        <div class="alert alert-success">
                            <i class="fa fa-exclamation-circle"></i>
                            {{ $pesan2 }}
                        </div>
                    @elseif($kode2=="90")
                        <div class="alert alert-danger">
                            <i class="fa fa-exclamation-circle"></i>
                            {{ $pesan2 }}
                        </div>
                    @endif
                    
                    <div class="tab-content"> 
                        <div class="tab-pane active" id="kegiatanPengadaan" role="tabpanel">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="tbKegiatanPengadaanDetail" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th align="center">No</th>
                                                <th>Aksi</th>
                                                <th>Status</th>
                                                <th>Comp</th>
                                                <th>Tahun</th>
                                                <th>Kegitan Pengadaan</th>
                                                <th>Currency</th>
                                                <th>Anggaran</th>
                                                <th>Tutup</th>
                                                <th>Nama Vendor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($model['tenderDetail'] as $dtDet)                                    
                                                <tr>
                                                    <td align="center">{{$loop->iteration}}</td>
                                                    <td align="center">
                                                        <div class="hidden-sm hidden-xs action-buttons">                                                                                                
                                                            <a class="green" href="{{ url('/kegiatanpengadaan/detail') }}/{{ $dtDet['tenderPenawaranId'] }}" title="Detail"><i class="ace-icon fa fa-search bigger-130"></i></a>
                                                            @if($nowDate <= $dtDet['tender']['0']['tglTutup'])
                                                            <a class="green" href="{{ url('/kegiatanpengadaan/penawaran') }}/{{ $dtDet['tenderPenawaranId'] }}" title="Kirim Penawaran"><i class="ace-icon fa fa-paper-plane-o bigger-130"></i></a>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td>
                                                        @if($dtDet['penawaranStatus']=='1')
                                                            Baru
                                                        @elseif($dtDet['penawaranStatus']=='2')
                                                            Mengikuti
                                                        @elseif($dtDet['penawaranStatus']=='3')
                                                            Nego
                                                        @elseif($dtDet['penawaranStatus']=='4')
                                                            Menang
                                                        @elseif($dtDet['penawaranStatus']=='5')
                                                            Kalah
                                                        @elseif($dtDet['penawaranStatus']=='6')
                                                            Tidak Mengikuti 
                                                        @else
                                                            Selesai
                                                        @endif
                                                    </td>
                                                    <td>{{ $dtDet['tender']['0']['comp'] }}</td>
                                                    <td>{{ $dtDet['tender']['0']['tenderTahun'] }}</td>
                                                    <td>{{ $dtDet['tender']['0']['tenderNama'] }}</td>
                                                    <td>{{ $dtDet['tender'][0]['tenderCurency'] }}</td>
                                                    <td>{{ number_format($dtDet['tender'][0]['tenderBudget'],2,',','.') }}</td>
                                                    <td>{{ $dtDet['tender'][0]['tglTutup'] }}</td>
                                                    <td>{{ $dtDet['supplier']['0']['supNama'] }}</td>                                                    
                                                </tr>  
                                            @endforeach                                                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>     
                        <!--second tab -->
                        <div class="tab-pane" id="mengikuti" role="tabpanel">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="tbKegiatanPengadaanMengikuti" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <td rowspan="2" align="center">No</td>
                                                <!--<th>Comp</th>
                                                <th>Tahun</th>
                                                <th>Kegitan Pengadaan</th>-->
                                                <td rowspan="2" align="center">Aksi</td>
                                                <td rowspan="2" align="center">Status</td>
                                                <td rowspan="2">Nama Vendor</td>
                                                <td align="center" colspan="3">Anggaran</td>
                                                <td align="center" colspan="3">Penawaran</td>
                                            </tr>
                                            <tr>
                                                <td align="center">Harga</td>
                                                <td align="center">Curr</td>
                                                <td align="center">Tutup</td>
                                                <td align="center">Harga</td>
                                                <td align="center">Curr</td>
                                                <td align="center">Expired</td>
                                            </tr>
                                        <tbody>
                                            @foreach($model['dtPengikut'] as $dtPengikut)                                    
                                                <tr>
                                                    <td align="center">{{$loop->iteration}}</td>
                                                    <!--<td>{{ $dtPengikut['tender'][0]['comp'] }}</td>
                                                    <td>{{ $dtPengikut['tender'][0]['tenderTahun'] }}</td>
                                                    <td>{{ $dtPengikut['tender'][0]['tenderNama'] }}</td>-->
                                                    <td align="center">
                                                        <div class="hidden-sm hidden-xs action-buttons">                                                                                                
                                                            <a class="green" href="{{ url('/kegiatanpengadaan/detailtender') }}/{{ $dtPengikut['tenderPenawaranId'] }}" title="Detail"><i class="ace-icon fa fa-search bigger-130"></i></a>
                                                            @if(date("Y-m-d H:i:s") <= $dtPengikut['tender'][0]['tglTutup'])
                                                                <a class="green" href="{{ url('/kegiatanpengadaan/penawaran') }}/{{ $dtPengikut['tenderPenawaranId'] }}" title="Kirim Penawaran"><i class="ace-icon fa fa-edit bigger-130"></i></a>
                                                            @else
                                                            
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td>
                                                        @if($dtPengikut['penawaranStatus']=='1')
                                                            Baru
                                                        @elseif($dtPengikut['penawaranStatus']=='2')
                                                            Mengikuti
                                                        @elseif($dtPengikut['penawaranStatus']=='3')
                                                            Nego 
                                                        @elseif($dtPengikut['penawaranStatus']=='4')
                                                            Menang 
                                                        @elseif($dtPengikut['penawaranStatus']=='5')
                                                            Kalah
                                                        @elseif($dtPengikut['penawaranStatus']=='6')
                                                            Tidak Mengikuti
                                                        @else
                                                            Selesai
                                                        @endif
                                                    </td>
                                                    <td>{{ $dtPengikut['supplier'][0]['supNama'] }}</td>
                                                    <td align="right">{{ $dtPengikut['tender'][0]['tenderBudget'] }}</td>
                                                    <td>{{ $dtPengikut['tender'][0]['tenderCurency'] }}</td>
                                                    <td>{{ $dtPengikut['tender'][0]['tglTutup'] }}</td>
                                                    <td align="right">{{ $dtPengikut['penawaranHarga'] }}</td>
                                                    <td>{{ $dtPengikut['penawaranCurency'] }}</td> 
                                                    <td>{{ $dtPengikut['penawaranExpired'] }}</td>
                                                </tr>  
                                            @endforeach                                                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--second tab 2 -->
                        <div class="tab-pane" id="negosiasi" role="tabpanel">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="tbKegiatanPengadaanNego" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <td rowspan="2" align="center">No</td>
                                                <!--<th>Comp</th>
                                                <th>Tahun</th>
                                                <th>Kegitan Pengadaan</th>-->
                                                <td rowspan="2" align="center">Aksi</td>
                                                <td rowspan="2" align="center">Status</td>
                                                <td rowspan="2">Nama Vendor</td>
                                                <td align="center" colspan="3">Anggaran</td>
                                                <td align="center" colspan="3">Penawaran</td>
                                                <td align="center" colspan="4">Negosiasi</td>
                                            </tr>
                                            <tr>
                                                <td align="center">Harga</td>
                                                <td align="center">Curr</td>
                                                <td align="center">Tutup</td>
                                                <td align="center">Harga</td>
                                                <td align="center">Curr</td>                                               
                                                <td align="center">Expired</td>
                                                <td align="center">Awal</td> 
                                                <td align="center">Akhir</td> 
                                                <td align="center">Harga Nego</td>  
                                                <td align="center">Tanggal Nego</td> 
                                            </tr>
                                        <tbody>
                                            @foreach($model['dtNego'] as $dtNego)                                    
                                                <tr>
                                                    <td align="center">{{$loop->iteration}}</td>
                                                    <!--<td>{{ $dtNego['tender'][0]['comp'] }}</td>
                                                    <td>{{ $dtNego['tender'][0]['tenderTahun'] }}</td>
                                                    <td>{{ $dtNego['tender'][0]['tenderNama'] }}</td>-->
                                                    <td align="center">
                                                        <div class="hidden-sm hidden-xs action-buttons">                                                                                                
                                                            <a class="green" href="{{ url('/kegiatanpengadaan/detailtender') }}/{{ $dtNego['tenderPenawaranId'] }}" title="Detail"><i class="ace-icon fa fa-search bigger-130"></i></a>
                                                            @if(date("Y-m-d H:i:s") <= $dtNego['tender'][0]['tglNegoAkhir'])
                                                            <a class="green" href="{{ url('/kegiatanpengadaan/negosiasi') }}/{{ $dtNego['tenderPenawaranId'] }}" title="Kirim Negosiasi"><i class="ace-icon fa  fa-send-o bigger-130"></i></a>
                                                            @else
                                                            
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td>
                                                        @if($dtNego['penawaranStatus']=='1')
                                                            Baru
                                                        @elseif($dtNego['penawaranStatus']=='2')
                                                            Mengikuti
                                                        @elseif($dtNego['penawaranStatus']=='3')
                                                            Nego 
                                                        @elseif($dtNego['penawaranStatus']=='4')
                                                            Menang 
                                                        @elseif($dtNego['penawaranStatus']=='5')
                                                            Kalah
                                                        @elseif($dtNego['penawaranStatus']=='6')
                                                            Tidak Mengikuti
                                                        @else
                                                            Selesai
                                                        @endif
                                                    </td>
                                                    <td>{{ $dtNego['supplier'][0]['supNama'] }}</td>
                                                    <td align="right">{{ $dtNego['tender'][0]['tenderBudget'] }}</td>
                                                    <td>{{ $dtNego['tender'][0]['tenderCurency'] }}</td>
                                                    <td>{{ $dtNego['tender'][0]['tglTutup'] }}</td>
                                                    <td align="right">{{ $dtNego['penawaranHarga'] }}</td>
                                                    <td>{{ $dtNego['penawaranCurency'] }}</td>          
                                                    <td>{{ $dtNego['penawaranExpired'] }}</td>
                                                    <td>{{ $dtNego['tender'][0]['tglNegoAwal'] }}</td>
                                                    <td>{{ $dtNego['tender'][0]['tglNegoAkhir'] }}</td>                                         
                                                    <td align="right">{{ $dtNego['penawaranNego'] }}</td>                                         
                                                    <td align="right">{{ $dtNego['tglNego'] }}</td>
                                                </tr>  
                                            @endforeach                                                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--second tab 3 -->
                        <div class="tab-pane" id="pemenang" role="tabpanel">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="tbKegiatanPengadaanPemenang" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <td rowspan="2" align="center">No</td>
                                                <!--<th>Comp</th>
                                                <th>Tahun</th>
                                                <th>Kegitan Pengadaan</th>-->
                                                <td rowspan="2" align="center">Aksi</td>
                                                <td rowspan="2" align="center">Status</td>
                                                <td rowspan="2">Nama Vendor</td>
                                                <td align="center" colspan="3">Anggaran</td>
                                                <td align="center" colspan="3">Penawaran</td>
                                                <td align="center" colspan="4">Negosiasi</td>
                                            </tr>
                                            <tr>
                                                <td align="center">Harga</td>
                                                <td align="center">Curr</td>
                                                <td align="center">Tutup</td>
                                                <td align="center">Harga</td>
                                                <td align="center">Curr</td>                                               
                                                <td align="center">Expired</td>
                                                <td align="center">Awal</td> 
                                                <td align="center">Akhir</td> 
                                                <td align="center">Harga Nego</td>  
                                                <td align="center">Tanggal Nego</td> 
                                            </tr>
                                        <tbody>
                                            @foreach($model['dtPemenang'] as $dtPemenang)                                    
                                                <tr>
                                                    <td align="center">{{$loop->iteration}}</td>
                                                    <!--<td>{{ $dtPemenang['tender'][0]['comp'] }}</td>
                                                    <td>{{ $dtPemenang['tender'][0]['tenderTahun'] }}</td>
                                                    <td>{{ $dtPemenang['tender'][0]['tenderNama'] }}</td>-->
                                                    <td align="center">
                                                        <div class="hidden-sm hidden-xs action-buttons">                                                                                                
                                                            <a class="green" href="{{ url('/kegiatanpengadaan/detailtender') }}/{{ $dtPemenang['tenderPenawaranId'] }}" title="Detail"><i class="ace-icon fa fa-search bigger-130"></i></a>
                                                            <a class="green" href="{{ url('/kegiatanpengadaan/cetak') }}/{{ $dtPemenang['tenderPenawaranId'] }}" title="Cetak"><i class="ace-icon fa fa-print bigger-130"></i></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        @if($dtPemenang['penawaranStatus']=='1')
                                                            Baru
                                                        @elseif($dtPemenang['penawaranStatus']=='2')
                                                            Mengikuti
                                                        @elseif($dtPemenang['penawaranStatus']=='3')
                                                            Nego 
                                                        @elseif($dtPemenang['penawaranStatus']=='4')
                                                            Menang 
                                                        @elseif($dtPemenang['penawaranStatus']=='5')
                                                            Kalah
                                                        @elseif($dtPemenang['penawaranStatus']=='6')
                                                            Tidak Mengikuti
                                                        @else
                                                            Selesai
                                                        @endif
                                                    </td>
                                                    <td>{{ $dtPemenang['supplier'][0]['supNama'] }}</td>
                                                    <td align="right">{{ $dtPemenang['tender'][0]['tenderBudget'] }}</td>
                                                    <td>{{ $dtPemenang['tender'][0]['tenderCurency'] }}</td>
                                                    <td>{{ $dtPemenang['tender'][0]['tglTutup'] }}</td>
                                                    <td align="right">{{ $dtPemenang['penawaranHarga'] }}</td>
                                                    <td>{{ $dtPemenang['penawaranCurency'] }}</td>
                                                    <td>{{ $dtPemenang['penawaranExpired'] }}</td>
                                                    <td>{{ $dtPemenang['tender'][0]['tglNegoAwal'] }}</td>
                                                    <td>{{ $dtPemenang['tender'][0]['tglNegoAkhir'] }}</td>
                                                    <td align="right">{{ $dtPemenang['penawaranNego'] }}</td>
                                                    <td align="right">{{ $dtPemenang['tglNego'] }}</td>
                                                </tr>  
                                            @endforeach                                                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-tagsinput/src/bootstrap-tagsinput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/js/dataTablesPlugins/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $('#tbKegiatanPengadaanDetail').DataTable();
        $('#tbKegiatanPengadaanMengikuti').DataTable({
            columnDefs: [                
                {
                    "targets": [4],
                    "data": "4",
                    "className": "dt-body-left",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },   
                {
                    "targets": [7],
                    "data": "7",
                    "className": "dt-head-right",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },
                //{
                //    "targets": [6],
                //    "data": "6",
                //    "className": "dt-head-right",
                //    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                //},  
            ]
        });
        $('#tbKegiatanPengadaanNego').DataTable({
            columnDefs: [                
                {
                    "targets": [4],
                    "data": "4",
                    "className": "dt-body-left",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },   
                {
                    "targets": [7],
                    "data": "7",
                    "className": "dt-head-right",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },
                {
                    "targets": [12],
                    "data": "12",
                    "className": "dt-head-right",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },   
            ]
        });
        $('#tbKegiatanPengadaanPemenang').DataTable({
            columnDefs: [                
                {
                    "targets": [4],
                    "data": "4",
                    "className": "dt-body-left",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },   
                {
                    "targets": [7],
                    "data": "7",
                    "className": "dt-head-right",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },
                {
                    "targets": [12],
                    "data": "12",
                    "className": "dt-head-right",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },   
            ]
        });
        
        //$('#tbKegiatanPengadaanMengikuti').DataTable({
            //dom: 'Bfrtip',
            //buttons: [
                //'excel', 'pdf', 'print'
            //    {
            //        extend:    'excelHtml5',
            //        text:      '<i class="fa fa-file-excel-o"></i>',
            //        titleAttr: 'Excel'
            //    },
            //    {
            //        extend:    'pdfHtml5',
            //        text:      '<i class="fa fa-file-pdf-o"></i>',
            //        titleAttr: 'PDF'
            //    },
            //    {
            //        extend:    'print',
            //        text:      '<i class="fa fa-print"></i>',
            //        titleAttr: 'Print'
            //    }
           // ]
        //});
        jQuery(document).ready(function() {        
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

            // For multiselect
            $('#pre-selected-options').multiSelect();
            $('#optgroup').multiSelect({
                selectableOptgroup: true
            });
            $('#public-methods').multiSelect();
        });
    </script>
@endpush
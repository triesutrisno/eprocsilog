@extends('layouts.main')

@push('styles')
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Tambah Data Penawaran</strong></h4>
                <div class="card-body">
                    <form action="{{ url('/kegiatanpengadaan/penawaranpost') }}/{{ $model['tenderDetail'][0]['tenderPenawaranId'] }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                        <div class="col-md-3">
                                <div class="form-group">
                                    <label for="tenderCurency">Mata Uang : <span class="danger">*</span></label>
                                    <select name="penawaranCurency" id="penawaranCurency" class="custom-select form-control select2" required>
                                        <option value="">Silakan Pilih</option>
                                        @foreach($model['curr'] as $val2)
                                            <option value="{{ $val2['currKode'] }}" {{ $model['tenderDetail'][0]['penawaranCurency']==$val2['currKode'] ? 'selected' : '' }}>{{ $val2['currKode'] }}</option>
                                        }
                                        @endforeach
                                    </select>
                                </div>
                            </div> 
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="penawaranHarga">Harga Penawaran : <span class="danger">*</span></label>
                                    <input type="text" class="form-control" required id="penawaranHarga" name="penawaranHarga" value="{{ $model['tenderDetail'][0]['penawaranHarga'] }}"> 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="penawaranExpired">Penawaran Berlaku Sampai : </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control"  id="penawaranExpired" autocomplete="off" name="penawaranExpired" value="{{ $model['tenderDetail'][0]['penawaranExpired'] }}"> 
                                        <div class="input-group-append" id="input-group-append-id">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="penawaranSpesifikasi">Spesifikasi : </label>
                                    <textarea name="penawaranSpesifikasi" id="penawaranSpesifikasi" rows="6" class="form-control">{{ $model['tenderDetail'][0]['penawaranSpesifikasi'] }}</textarea>
                                </div>
                            </div>                            
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="penawaranFile">Lampiran 1 : <span class="danger"><small> <code>Maximal file 1M and only zip file type.</code></small></span></label>
                                    <input type="file" class="dropify" data-max-file-size="1M" data-allowed-file-extensions="zip" id="penawaranFile" name="penawaranFile"> 
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="penawaranGambar">Lampiran 2 : <span class="danger"><small> <code>Maximal file 1M and only zip file type.</code></small></span></label>
                                    <input type="file" class="dropify" data-max-file-size="1M" data-allowed-file-extensions="zip" id="penawaranGambar" name="penawaranGambar"> 
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div  class="col-lg-2 col-md-2">
                                <button class="btn btn-block btn-success" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('js/validation.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>    
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#penawaranExpired').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                //todayHighlight: true,
                //format: "mm-yyyy",
                //viewMode: "months", 
                //minViewMode: "months"
            });
            $('#input-group-append-id').click(function() {
                //alert("Bismillah !");
                $('#penawaranExpired').datepicker('show');
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
        
    </script>
@endpush
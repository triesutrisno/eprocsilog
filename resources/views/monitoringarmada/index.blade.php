@extends('layouts.main')

@push('styles')
<link href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Monitoring Armada</strong></h4>
                <div class="col-lg-12">
                    <div class="card border border-grey-light">
                        <form action="{{ url('/monitoringarmada') }}/{{ session('user')->username }}" method="post">
                        @csrf
                        <div class="card-body">
                            <h4 class="card-title"><span class="lstick"></span>Cari Data</h4>
                            <div class="chat-box">
                                <div class="row p-t-20">
                                    <div class="col-lg-2 col-md-12">
                                        <h6 class="m-t-0">Nopol</h6>
                                        <div class="input-group">
                                            <input type="text" name="nopol" class="form-control" value="{{ $param['nopol'] }}" autocomplete="off">
                                        </div>  
                                    </div> 
                                    <div class="col-lg-2 col-md-12">
                                        <h6 class="m-t-0">Nopin</h6>
                                        <div class="input-group">
                                            <input type="text" name="nopin" class="form-control" value="{{ $param['nopin'] }}" autocomplete="off">
                                        </div>  
                                    </div> 
                                    <div class="col-lg-2 col-md-12">
                                        <h6 class="m-t-0">Pemilik Armada</h6>
                                        <div class="input-group">
                                            <input type="text" name="pemilikArmada" class="form-control" value="{{ $param['pemilikArmada'] }}" autocomplete="off">
                                        </div>  
                                    </div>
                                    <div class="col-lg-2 col-md-12">
                                        <h6 class="m-t-0">Nama Driver</h6>
                                        <div class="input-group">
                                            <input type="text" name="driver" class="form-control" value="{{ $param['driver'] }}" autocomplete="off">
                                        </div>  
                                    </div> 
                                </div>
                                <div class="row p-t-20">
                                    <div class="col-lg-3 col-md-12">
                                        <h6 class="m-t-0">Status Perjalanan</h6>
                                        <div class="input-group">
                                            <select name="status" class="form-control">
                                                <option value="">Silakan Pilih</option>
                                                <option value="10" {{ $param['status']=='10' ? 'selected' : '' }}>Armada Siap</option>
                                                <option value="17" {{ $param['status']=='17' ? 'selected' : '' }}>Pre-Matching Vendor</option>
                                                <option value="17" {{ $param['status']=='18' ? 'selected' : '' }}>Pre-Matching Silog</option>
                                                <option value="20"{{ $param['status']=='20' ? 'selected' : '' }}>Sudah Match DO</option>
                                                <option value="21"{{ $param['status']=='21' ? 'selected' : '' }}>Approve Match</option>
                                                <option value="22"{{ $param['status']=='22' ? 'selected' : '' }}>Konfirmasi Operasional</option>
                                                <option value="30"{{ $param['status']=='30' ? 'selected' : '' }}>Berangkat Ke Pemuatan</option>
                                                <option value="35"{{ $param['status']=='35' ? 'selected' : '' }}>Sampai ke Pemuatan</option>
                                                <option value="36"{{ $param['status']=='36' ? 'selected' : '' }}>Konfirmasi Alamat Kirim</option>
                                                <option value="39"{{ $param['status']=='39' ? 'selected' : '' }}>Selesai Muat</option>
                                                <option value="40"{{ $param['status']=='40' ? 'selected' : '' }}>Berangkat ke Tujuan</option>
                                                <option value="40"{{ $param['status']=='41' ? 'selected' : '' }}>Berangkat ke Tujuan (Tanpa Balen)</option>
                                                <option value="45"{{ $param['status']=='45' ? 'selected' : '' }}>Sampai Tujuan</option>
                                                <option value="48"{{ $param['status']=='48' ? 'selected' : '' }}>Bongkar - Parsial</option>
                                                <option value="49"{{ $param['status']=='49' ? 'selected' : '' }}>Selesai Bongkar</option>
                                                <option value="50"{{ $param['status']=='50' ? 'selected' : '' }}>Berangkat Kembali</option>
                                                <option value="58"{{ $param['status']=='58' ? 'selected' : '' }}>Konfirm Kembali dari DO Lain</option>
                                                <option value="59"{{ $param['status']=='59' ? 'selected' : '' }}>Sampai Kembali</option>
                                                <option value="60"{{ $param['status']=='60' ? 'selected' : '' }}>Dokumen Kembali</option>
                                                <option value="61"{{ $param['status']=='61' ? 'selected' : '' }}>Dokumen Belum Lengkap</option>
                                                <option value="70"{{ $param['status']=='70' ? 'selected' : '' }}>Verifikasi Dokumen Lengkap</option>
                                                <option value="99"{{ $param['status']=='99' ? 'selected' : '' }}>Close</option>
                                                <option value="X1"{{ $param['status']=='X1' ? 'selected' : '' }}>Cancel - Laka</option>
                                                <option value="X2"{{ $param['status']=='X2' ? 'selected' : '' }}>Cancel - Driver Sakit</option>
                                                <option value="X3"{{ $param['status']=='X3' ? 'selected' : '' }}>Cancel - Driver Menolak DO</option>
                                                <option value="X4"{{ $param['status']=='X4' ? 'selected' : '' }}>Cancel - Penelantaran Armada</option>
                                                <option value="X5"{{ $param['status']=='X5' ? 'selected' : '' }}>Cancel - Kerusakan Armada</option>
                                                <option value="X6"{{ $param['status']=='X6' ? 'selected' : '' }}>Cancel - Alasan Operasional</option>
                                                <option value="X7"{{ $param['status']=='X7' ? 'selected' : '' }}>Cancel - Salah Matching</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <h6 class="m-t-0">Status Truk</h6>
                                        <div class="input-group">
                                            <select name="statusTruk" class="form-control">
                                                <option value="">Silakan Pilih</option>
                                                <option value="Parking" {{ $param['statusTruk']=='Parking' ? 'selected' : '' }}>PARKING</option>
                                                <option value="Idle" {{ $param['statusTruk']=='Idle' ? 'selected' : '' }}>IDLE</option>
                                                <option value="Driving" {{ $param['statusTruk']=='Driving' ? 'selected' : '' }}>DRIVING</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <h6 class="m-t-0">Status Muatan</h6>
                                        <div class="input-group">
                                            <select name="statusMuatan" class="form-control">
                                                <option value="">Silakan Pilih</option>
                                                <option value="MUATAN" {{ $param['statusMuatan']=='MUATAN' ? 'selected' : '' }}>MUATAN</option>
                                                <option value="KOSONGAN" {{ $param['statusMuatan']=='KOSONGAN' ? 'selected' : '' }}>KOSONGAN</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body b-t">
                            <div class="row">
                                <div class="col-lg-2 col-md-12">
                                    <a href="{{ url('/monitoringarmada') }}/{{ session('user')->username }}" class="waves-effect waves-light btn btn-success">Lihat Data</a>
                                    <button type="submit" class="btn waves-effect waves-light btn-warning">Cari</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                @if(session('kode')=="99")
                    <div class="alert alert-success">
                        <i class="fa fa-exclamation-circle"></i>
                        {{ session('pesan') }}
                    </div>
                @elseif(session('kode')=="90")
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-circle"></i>
                        {{ session('pesan') }}
                    </div>
                @endif                 
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id='tbMonitoringArmada' class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                @csrf
                                <tr>
                                    <td align="center" width="50">No</td>
                                    <td align="center" width="100">Status <br /> Perjalanan</td>
                                    <td>Status <br /> Muatan</td>
                                    <td width="100">Status <br /> Truk</td>
                                    <td width="100">Durasi</td>
                                    <td align="center" width="100">Nopin</td>
                                    <td align="center" width="100">Nopol</td>
                                    <td align="center" width="150">Pemilik Truk</td>
                                    <td align="center" width="50">Nik Driver</td>
                                    <td align="center" width="100">Nama Driver</td>
                                    <td width="150">Vendor Driver</td>
                                    <td width="100">Kecamatan</td>
                                    <td width="100">Kabupaten</td>
                                    <td width="100">Propinsi</td>
                                    <td>Alamat</td>
                                </tr>
                            </thead>
                            <tbody> 
                            @foreach($model['data'] as $key => $val)
                                @php $no = (int)$key+1; @endphp 
                                <tr>
                                    <td align="center">{{$no}}</td>
                                    <td align="center">
                                        @if($val['sts_perj'] == '10')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-warning">Armada Siap</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '17')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-warning">Pre-Matching Vendor</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '18')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-warning">Pre-Matching Silog</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '20')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-warning">Sudah Match DO</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '21')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-warning">Approve Match</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '22')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-warning">Konfirmasi Operasional</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '30')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-info">Berangkat ke Pemuatan</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '35')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-info">Sampai ke Pemuatan</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '36')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-info">Konfirmasi Alamat Kirim</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '39')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-info">Selesai Muat</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '40')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-info">Berangkat ke Tujuan</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '41')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-info">Berangkat ke Tujuan (Tanpa Balen)</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '45')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-info">Sampai Tujuan</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '48')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-info">Bongkar - Parsial</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '49')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-success">Selesai Bongkar</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '50')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-info">Berangkat Kembali</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '51')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-info">Berangkat Kembali dari DO Lain</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '58')
                                            <a href="#" data-nopin="{{$val['nopin']}}">
                                                <label class="badge badge-info">Konfirm Kembali dari DO Lain</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '59')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-success">Sampai Kembali</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '60')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-success">Dokumen Kembali</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '61')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-success">Dokumen Belum Lengkap</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '70')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-success">Verifikasi Dokumen Lengkap</label>
                                            </a>
                                        @elseif($val['sts_perj'] == '99')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-danger">Close</label>
                                            </a>
                                        @elseif($val['sts_perj'] == 'X1')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-danger">Cancel - Laka</label>
                                            </a>
                                        @elseif($val['sts_perj'] == 'X2')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-danger">Cancel - Driver Sakit</label>
                                            </a>
                                        @elseif($val['sts_perj'] == 'X3')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-danger">Cancel - Driver Menolak DO</label>
                                            </a>
                                        @elseif($val['sts_perj'] == 'X4')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-danger">Cancel - Penelantaran Armada</label>
                                            </a>
                                        @elseif($val['sts_perj'] == 'X5')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-danger">Cancel - Kerusakan Armada</label>
                                            </a>
                                        @elseif($val['sts_perj'] == 'X6')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-danger">Cancel - Alasan Operasional</label>
                                            </a>
                                        @elseif($val['sts_perj'] == 'X7')
                                            <a href="#" data-nopin="{{$val['nopin']}}" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive">
                                                <label class="badge badge-danger">Cancel - Salah Matching</label>
                                            </a>
                                        @endif
                                    </td>
                                    <td align="center">
                                        @if($val['statusmuatan'] == 'MUATAN')
                                            <label class="badge badge-success">MUATAN</label>
                                        @elseif($val['statusmuatan'] == 'KOSONGAN')
                                            <label class="badge badge-warning">KOSONGAN</label>
                                        @elseif($val['statusmuatan'] == '')
                                            <label class="badge badge-danger"></label> 
                                        @endif
                                    </td>
                                    <td align="center">
                                        @if($val['statusarmada'] == 'Parking')
                                            <label class="badge badge-danger">PARKIR</label>
                                        @elseif($val['statusarmada'] == 'Idle')
                                            <label class="badge badge-warning">IDLE</label>
                                        @elseif($val['statusarmada'] == 'Driving')
                                            <label class="badge badge-success">DRIVING</label> 
                                        @endif
                                    </td>
                                    <td>{{$val['durasi']}}</td>
                                    <td>{{$val['nopin']}}</td>
                                    <td>{{$val['nopol']}}</td>
                                    <td>{{$val['namapemiliktruk']}}</td>
                                    <td align="center">{{$val['nikdriver']}}</td>
                                    <td>{{$val['namadriver']}}</td>
                                    <td>{{$val['namavendordrv']}}</td>
                                    <td>{{$val['kecamatan']}}</td>
                                    <td>{{$val['kabupaten']}}</td>
                                    <td>{{$val['provinsi']}}</td>
                                    <td >{{$val['alamat']}}</td>
                                </tr>                                                    
                            @endforeach
                            </tbody>                                                
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script>
    $(document).ready(function() {  
        $('#tbMonitoringArmada').DataTable( {
            dom: '<"row m-0" <"col-sm-3 p-0"l><"col-sm-3 p-t-10"r><"col-sm-6 p-0"<"float-right"B>> >tip',
            "lengthMenu": [[50, 100, 500, 1000, -1], [50, 100, 500, 1000, "All"]],
            "processing": true,
            //"responsive": true,
            "ordering" : false,
            "scrollY": "500px",
            "scrollX": true,
            "scrollCollapse": true,
            "columnDefs": [
                { "width": "20%", "targets": 0 }
            ],
            "fixedColumns": true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},
            "buttons": [                
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fas fa-file-excel"></i>',
                    titleAttr: 'Excel',
                    title: 'monitoring-do',
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fas fa-file-pdf"></i>',
                    titleAttr: 'PDF',
                    title: 'monitoring-do',
                }

            ],  
        } );
        
        $('#datepicker-autoclose').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true,
        });
        $('#input-group-append-id').click(function() {
            //alert("Bismillah !");
            $('#datepicker-autoclose').datepicker('show');
        });
        
        $('#datepicker-autoclose2').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true,
        });
        $('#input-group-append-id2').click(function() {
            //alert("Bismillah !");
            $('#datepicker-autoclose2').datepicker('show');
        });
    });    
</script>
@endpush
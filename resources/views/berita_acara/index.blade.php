@extends('layouts.main')

@push('styles')
    <link href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pages/buttons.dataTables.css') }}" rel="stylesheet">
@endpush

@section('container')
    <div id="module-pages" class="container-fluid">
        <div class="row">
            <!-- column -->
            <div class="col-lg-12">
                <div class="card">
                    <h2 class="card-title col-lg-12"><strong>Berita Acara</strong></h2>
                    <div class="card-body">
                        @if (session()->get('user')->role == 'supplier')
                            <div class="row">
                                <div class="col-lg-3 col-md-3">
                                    <a href="{{ route('berita_acara.create') }}"
                                        class="waves-effect waves-light btn btn-primary">Input Baru</a>
                                    <h6 class="m-t-0">&nbsp;</h6>

                                </div>
                            </div>
                        @endif
                        <div class="col-lg-12 col-md-12 alert alert-warning">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label class="text-small">Nomer BA</label>
                                    <input type="text" name="no_berita_acara" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-lg-3">
                                    <label class="text-small">Nama Supplier/Vendor</label>
                                    <input type="text" name="nama_supplier" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-lg-3">
                                    <label class="text-small">No Kontrak</label>
                                    <input type="text" name="no_kontrak_perjanjian" class="form-control"
                                        autocomplete="off">
                                </div>
                                <div class="col-lg-3">
                                    <label class="text-small">Status Approve BA</label>
                                    <select name="status" class="form-control">
                                        <option value="">Silakan Pilih</option>
                                        <option value="0">Waiting</option>
                                        <option value="1">Approved</option>
                                        <option value="-1">Rejected</option>
                                    </select>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-lg-3">
                                    <label class="text-small">Status Barang</label>
                                    <select name="status_barang" class="form-control">
                                        <option value="">Silakan Pilih</option>
                                        <option value="KLAIM">KLAIM</option>
                                        <option value="NON KLAIM">NON KLAIM</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label class="text-small">Tgl Approve : </label>
                                    <input type="text" name="tgl_approve" class="form-control" autocomplete="off">
                                </div>

                                <div class="col-lg-3">
                                    <label class="text-small">No SPJ</label>
                                    <input type="text" name="no_spj" class="form-control" autocomplete="off">
                                </div>
                            </div>

                            @if (session()->get('user')->role == 'administrator')
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="text-small">No PP Eproc : </label>
                                        <input type="text" name="no_pp" class="form-control">
                                    </div>

                                    <div class="col-lg-3">
                                        <label class="text-small">Sudah Input SAP</label>
                                        <select name="status_input_sap" class="form-control">
                                            <option value="">Silakan Pilih</option>
                                            <option value="0">Belum</option>
                                            <option value="1">Sudah</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3">
                                        <label class="text-small">Sudah Generate Surat Klaim</label>
                                        <select name="status_generate_klaim" class="form-control">
                                            <option value="">Silakan Pilih</option>
                                            <option value="0">Belum</option>
                                            <option value="1">Sudah</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3">
                                        <label class="text-small">Status Approve Klaim</label>
                                        <select name="status_approve_klaim" class="form-control">
                                            <option value="">Silakan Pilih</option>
                                            <option value="0">Belum</option>
                                            <option value="1">Approved</option>
                                        </select>
                                    </div>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-lg-2 col-md-12">
                                    <h4 class="m-t-0">&nbsp;</h4>
                                    <button id="btn_search" type="submit"
                                        class="btn waves-effect waves-light btn-success">Cari</button>
                                </div>
                            </div>

                            <div class="row">
                                <h6 class="m-t-0">&nbsp;</h6>
                                <h6 class="m-t-0">&nbsp;</h6>
                            </div>
                        </div>
                    </div>

                    @include('partials.alert')

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <div id="example23_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                        <table id='berita_acara_table'
                                            class="display table table-hover table-striped table-bordered dataTable"
                                            cellspacing="0" width="100%" role="grid">
                                            <thead>
                                                <tr>
                                                    <td>ID</td>
                                                    <td>No BA</td>
                                                    <td>Perusahaan</td>
                                                    <td>Cust</td>
                                                    <td>No Kontrak</td>
                                                    <td>Periode SPJ</td>
                                                    <td>Jenis Plat</td>
                                                    <td>Plant</td>
                                                    <td>Status Barang</td>
                                                    <td>Status ID</td>
                                                    <td>Status</td>
                                                    <td>Approved By ID</td>
                                                    <td>Approved By</td>
                                                    <td>Tgl Apprv</td>
                                                    <td>Total SPJ</td>
                                                    <td>Total Apprv</td>
                                                    <td>Tgl Entri</td>
                                                    <td>No PP</td>
                                                    <td>Sdh SAP</td>
                                                    <td>No Surat Klaim</td>
                                                    <td>Apprv Surat Klaim</td>
                                                    <td>Aksi</td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div><!-- end row -->
                    </div>
                </div>
            </div>
        </div><!-- end row -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@push('scripts')
    <!-- This is data table -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>


    <script>
        $(document).ready(function() {

            var tgl_approve = $('input[name="tgl_approve"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            tgl_approve.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format(
                    'MM/DD/YYYY'));
            });

            tgl_approve.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            var table = $('#berita_acara_table').DataTable({
                dom: '<"row m-0" <"col-sm-3 p-0"B> <"col-sm-3 p-t-10"r><"col-sm-6 p-0"<"float-right"p>>>t<"row m-0" <"col-sm-3 p-0"i> <"col-sm-3 p-0"l><"col-sm-6 p-0"p> >',
                "lengthChange": true,
                "lengthMenu": [
                    [10, 25, 50, 100, 500],
                    [10, 25, 50, 100, 500]
                ],
                order: [
                    [0, 'desc']
                ],
                "processing": true,
                "serverSide": true,
                "orderCellsTop": true,
                "responsive": true,
                "scrollX": true,
                "language": {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                },
                "ajax": {
                    "url": "{{ route('berita_acara.getList') }}",
                    "data": function(d) {
                            d.no_berita_acara = $('input[name="no_berita_acara"]').val(),
                            d.nama_supplier = $('input[name="nama_supplier"]').val(),
                            d.no_kontrak_perjanjian = $('input[name="no_kontrak_perjanjian"]').val(),
                            d.status = $('select[name="status"]').val(),
                            d.tgl_approve = $('input[name="tgl_approve"]').val()
                            d.status_barang = $('select[name="status_barang"]').val()
                            d.no_spj = $('input[name="no_spj"]').val()
                            d.no_pp = $('input[name="no_pp"]').val()
                            d.status_input_sap = $('select[name="status_input_sap"]').val()
                            d.status_generate_klaim = $('select[name="status_generate_klaim"]').val()
                            d.status_approve_klaim = $('select[name="status_approve_klaim"]').val()
                    }
                },
                "buttons": [

                    {
                        extend: 'colvis',
                        columns: ':not(.noVis)',
                        text: 'Column Visibility',
                        collectionLayout: 'fixed three-column',
                        postfixButtons: [
                            'colvisRestore',
                            {
                                extend: 'colvisGroup',
                                text: 'Show all',
                                show: ':hidden'
                            }
                        ]
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"> Export Excel </i>',
                        titleAttr: 'Excel',
                        title: 'Data Berita Acara',
                    }

                ],

                "columnDefs": [{
                        "targets": [0],
                        "visible": false
                    },
                    {
                        "targets": [1],
                        "data": null,
                        "orderable": false,
                        "render": function(data, type, full, meta) {
                            var id_berita_acara = data[0];

                            var res = '<div class="hidden-sm hidden-xs btn-group btn-group-sm">' +
                                '<a class="green btn" href="{!! url('/berita-acara-show') !!}/' +
                                id_berita_acara +
                                '" title="Detail">' + data[1] + '</a>';

                            res += '</div>';

                            return res;

                        },
                    },
                    {
                        "targets": [4],
                        "visible": false,
                        render: function(data, type, row) {
                            return '<span style="display: block;overflow-wrap: break-word;width: 100px;">' +
                                data + "</span>";
                        }
                    },
                    {
                        "targets": [5],
                        "sortable": false,
                        render: function(data, type, row) {
                            return '<span style="display: block;overflow-wrap: break-word;width: 120px;">' +
                                data + "</span>";
                        }
                    },
                    {
                        "targets": [9],
                        "visible": false
                    },
                    {
                        "targets": [11],
                        "visible": false
                    },
                    {
                        "targets": [12],
                        "visible": false
                    },
                    {
                        "targets": [13],
                        "visible": false
                    },
                    {
                        "targets": [14],
                        "sortable": false
                    },
                    {
                        "targets": [15],
                        "visible": false,
                        "sortable": false
                    },
                    {
                        "targets": [16],
                        "visible": false,
                        "sortable": false
                    },
                    {
                        "targets": [19],
                        "visible": false,
                        "sortable": false
                    },
                    {
                        "targets": [20],
                        "visible": false,
                        "sortable": false
                    },
                    {
                        "targets": [21],
                        "data": null,
                        "orderable": false,
                        "render": function(data, type, full, meta) {
                            var approve_status = data[4];
                            var id_berita_acara = data[0];

                            var res = '<div class="hidden-sm hidden-xs btn-group btn-group-sm">' +
                                '<a class="green btn" href="{!! url('/berita-acara-show') !!}/' +
                                id_berita_acara +
                                '" title="Detail"><i class="ace-icon fa fa-search bigger-130"></i></a>';


                            if (approve_status == '0') {
                                res +=
                                    // '<a class="green btn" href="{{ url('/berita-acara-delete') }}/' +
                                    // id_berita_acara +
                                    // '" title="Ubah "><i class="ace-icon fa fa-edit bigger-130 "></i></a>' +
                                    '<a class="green btn btn_hapus_ba" href="#" data-id="' +
                                    id_berita_acara +
                                    '" title="Hapus"><i class="ace-icon fa fa-trash bigger-130"></i></a>';
                            }

                            res += '</div>';

                            return res;

                        },
                    },
                ]

            });

            $(document).on("click", ".btn_hapus_ba", function(ev) {
                ev.preventDefault();
                var dataId = $(this).attr("data-id");
                swal({
                    title: "Konfirmasi",
                    text: "Hapus Berita Acara ini ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Batal",
                }, function() {
                    window.location.assign(
                        '{{ url('/berita-acara-delete') }}/' + dataId);
                });
            });

            $("#btn_search").click(function() {
                table.ajax.reload();

            });
        });
    </script>
@endpush

@extends('layouts.main')

@push('styles')
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!--
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Beranda</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
            </ol>
        </div>
    </div>
    -->
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Verifikasi email Rekanan / Vendor</strong></h4>
                <br />
                @php if($model['kode']=="90"){ @endphp
                    <h3 class="card-title text-danger"><strong>Maaf !</strong></h3>            
                    <p class="text-justify">
                        {{ $message }}
                    </p>
                    <div class="row button-group">
                        <div class="col-lg-4 col-md-4">
                            <a href="{{ url('/login') }}" class="btn btn-block btn-outline-success">MASUK</a>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <a href="{{ url('/register') }}" class="btn waves-effect waves-light btn-block btn-success">DAFTAR</a>
                        </div>                        
                    </div>
                @php }else{ @endphp
                    <h3 class="card-title text-success"><strong>Terima Kasih !</strong></h3>            
                    <p class="text-justify">
                        {{ $message }}
                    </p>
                    <div class="row button-group">
                        <div class="col-lg-4 col-md-4">
                            <a href="{{ url('/') }}" class="btn btn-block btn-outline-success">BERANDA</a>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <a href="{{ url('/login') }}" class="btn waves-effect waves-light btn-block btn-success">MASUK</a>
                        </div>                        
                    </div>
                @php } @endphp
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
@endpush
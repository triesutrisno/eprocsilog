@extends('layouts.main')

@push('styles')
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />  
    <link href="{{ asset('assets/plugins/wizard/steps.css') }}" rel="stylesheet">
    <!--alerts CSS -->
    <link href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!--
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Beranda</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
            </ol>
        </div>
    </div>
    -->
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body wizard-content">
                    <h4 class="card-title"><strong>Pendaftaran Rekanan / Vendor</strong></h4>
                    <form method="post" action="{{ url('/register') }}" id="formID" class="validation-wizard wizard-circle" enctype="multipart/form-data">
                        @csrf
                        <!-- Step 0 -->
                        <h6>Pakta Integritas</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group" align="justify">                                        
                                        Sehubungan dengan keikutsertaan kami dalam proses pengadaan barang/jasa pada PT Semen Indonesia Logistik Group, dengan ini kami menyatakan hal-hal sebagaimana berikut dibawah ini: <br />
                                        <ol>
                                            <li>Bahwa semua informasi dan dokumen-dokumen yang kami sampaikan dan berikan dalam keikutsertaan Perusahaan dalam proses pengadaan/jasa pada PT Semen Indonesia Logistik Group adalah benar, sehingga apabila dikemudian hari ditemukan adanya ketidaksesuaian atas informasi dan dokumen dimaksud, maka Perusahaan bersedia dituntut di muka pengadilan, bersedia dikenakan sanksi administrative dan dimasukkan dalam Daftar Hitam PT Semen Indonesia Logistik Group</li>
                                            <li>Bahwa Perusahaan sanggup mematuhi semua peraturan perundang-undangan yang terkait anti korupsi, kolusi dan nepotisme (KKN), anti trust, anti pencucian uang dan peraturan perundang-undangan lainnya yang relevan dalam pelaksanaan pengadaan barang/jasa ini</li>
                                            <li>
                                                Jaminan Kewajaran Harga :
                                                <ul>
                                                    <li>Bahwa harga yang kami sampaikan adalah wajar. Bila di kemudian hari diketahui bahwa harga kami sampaikan menunjukkan/mengindikasikan adanya ketidakwajaran, maka Perusahaan sanggup mempertanggungjawabkan dan mengembalikan kelebihan harga tersebut ke PT Semen Indonesia Logistik Group.</li>
                                                    <li>Bahwa harga yang kami tawarkan sudah termasuk keuntungan serta semua unsur biaya yang telah ditetapkan dalam dokumen pengadaan dan [sudah / belum]* termasuk Pajak.</li>
                                                </ul>
                                            </li>
                                            <li>
                                                Bahwa Perusahaan dan Karyawan Perusahaan tidak memiliki benturan kepentingan dengan PT Semen Indonesia Logistik Group (baik pribadi ataupun keluarga) yang membuat Perusahaan menjadi tidak patut untuk bertindak selaku mitra kerja PT Semen Indonesia Logistik, termasuk :
                                                <ul>
                                                    <li>Kepentingan ekonomi secara langsung dan/atau tidak langsung yang bersifat material, hubungan asosiasi atau hubungan lainnya (baik pribadi ataupun keluarga) dengan PT Semen Indonesia Logistik Group atau Karyawan atau Direksi PT Semen Indonesia Logistik Group.</li>
                                                    <li>Selama berlangsungnya proses Pekerjaan tidak akan melakukan tindakan menerima pekerjaan dari pihak manapun secara langsung atau tidak langsung, yang mempunyai atau mengakibatkan timbulnya benturan kepentingan antara Perusahaan dengan PT Semen Indonesia Logistik Group.</li>
                                                    <li>Tidak memberikan komisi, upah atau hadiah dalam bentuk apapun kepada PT Semen Indonesia Logistik Group (baik Karyawan atau Direksi secara pribadi ataupun keluarga)</li>
                                                    <li>Menjamin bahwa tidak ada pembayaran atau pemberian yang telah atau akan langsung atau tidak langsung, dijanjikan kepada Pejabat Pengadaan/Panitia Pengadaan PT Semen Indonesia Logistik Group (baik pribadi ataupun keluarga) guna mempengaruhi tindakan atau menyebabkan Pejabat Pengadaan/Panitia Pengadaan menggunakan pengaruhnya untuk memberikan keuntungan dan/atau menjadikan pemenang yang tidak wajar pada Perusahaan terhadap kegiatan pengadaan barang/jasa ini.</li>
                                                </ul>
                                            </li>
                                        </ol>
                                        Demikain pernyataan ini kami buat untuk dapat dipergunakan sabagaimana mestinya guna memenuhi salah satu syarat dalam proses pengadaan barang/jasa untuk pekerjaan tersebut diatas.
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="c-inputs-stacked">
                                        <label class="inline custom-control custom-checkbox block">
                                            <input type="checkbox" name="paktaIntegritas" class="custom-control-input required"> <span class="custom-control-label ml-0">Saya Setuju *</span>
                                        </label>                                        
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 1 -->
                        <h6>Data Perusahaan</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="namaPerusahaan"> Nama Perusahaan : <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="namaPerusahaan" maxlength="100" name="namaPerusahaan" value="{{old('namaPerusahaan')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="websitePerusahaan"> Website :  </label>
                                        <input type="url" class="form-control" id="websitePerusahaan" maxlength="100" name="websitePerusahaan" value="{{old('websitePerusahaan')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="emailPerusahaan"> Email Perusahaan : <span class="danger">*</span> </label>
                                        <input type="email" class="form-control form-control-danger required" id="emailPerusahaan" maxlength="100" name="emailPerusahaan" value="{{old('emailPerusahaan')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="noTlp">No. Telepon Perusahaan: <span class="danger">*</span> </label>
                                        <input type="tel" class="form-control required" id="noTlp" maxlength="20" name="noTlp" value="{{old('noTlp')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="propinsiPerusahaan"> Propinsi : <span class="danger">*</span> </label>
                                        <select name="propinsiPerusahaan" id="propinsiPerusahaan" class="custom-select form-control required select2 dinamic" data-dependent="kotaperusahaan" stle="width:100%">
                                            <option value="">Silakan Pilih</option>
                                            @foreach($dtProp as $val)
                                                <option value="{{ $val['mpropId'] }}">{{ $val['mpropNama'] }}</option>
                                            }
                                            @endforeach
                                        </select>
                                        <!--<input type="text" class="form-control required" id="propinsiPerusahaan" name="propinsiPerusahaan" value="{{old('propinsiPerusahaan')}}"> -->
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="kotaPerusahaan"> Kota / Kabupaten Perusahaan : <span class="danger">*</span> </label>
                                        <!--<input type="text" class="form-control required" id="kotaPerusahaan" name="kotaPerusahaan" value="{{old('kotaPerusahaan')}}">-->
                                        <select name="kotaPerusahaan" id="kotaperusahaan" class="custom-select form-control required select2 dinamic" data-dependent="kecamatanperusahaan">
                                            <option value="">Silakan Pilih</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="kecamatanPerusahaan"> Kecamatan Perusahaan : <span class="danger">*</span> </label>
                                        <!--<input type="text" class="form-control required" id="kecamatanPerusahaan" name="kecamatanPerusahaan" value="{{old('kecamatanPerusahaan')}}"> -->
                                        <select name="kecamatanPerusahaan" id="kecamatanperusahaan" class="custom-select form-control required select2">
                                            <option value="">Silakan Pilih</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="alamatPerusahaan">Alamat Perusahaan : <span class="danger">*</span></label>
                                        <textarea name="alamatPerusahaan" id="alamatPerusahaan" rows="6" class="form-control required">{{old('alamatPerusahaan')}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="kategoriPerusahaan">Kategori : <span class="danger">*</span> </label>
                                        <!--<input type="text" class="form-control required" id="kategoriPerusahaan" name="kategoriPerusahaan" value="{{old('kategoriPerusahaan')}}"> -->
                                        <select name="kategoriPerusahaan" id="kategoriPerusahaan" class="custom-select form-control required select2">
                                            <option value="">Silakan Pilih</option>
                                            @foreach($dtKat as $val2)
                                                <option value="{{ $val2['mkatId'] }}">{{ $val2['mkatNama'] }}</option>
                                            }
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="kualifikasiPerusahaan">Kualifikasi : <span class="danger">*</span></label>
                                        <select class="custom-select form-control required select2" id="kualifikasiPerusahaan" name="kualifikasiPerusahaan">
                                            <option value="">Silakan Pilih</option>
                                            @foreach($dtKual as $val3)
                                                <option value="{{ $val3['mkualId'] }}">{{ $val3['mkualNama'] }}</option>
                                            }
                                            @endforeach
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="siupPerusahaan"> Nomor Surat Ijin Usaha Perdagangan (SIUP) : <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="siupPerusahaan" maxlength="100" name="siupPerusahaan" value="{{old('siupPerusahaan')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranSiup">Lampiran Siup : <span class="danger">*</span> </label>
                                        <input type="file" class="dropify required" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="lampiranSiup" name="lampiranSiup"> 
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="npwpPerusahaan"> Nomor Pokok Wajib Pajak (NPWP) : <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="npwpPerusahaan" maxlength="100" name="npwpPerusahaan" value="{{old('npwpPerusahaan')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranNPWP">Lampiran NPWP : <span class="danger">*</span> </label>
                                        <input type="file" class="dropify required" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="lampiranNPWP" name="lampiranNPWP"> 
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="nppkpPerusahaan"> Nomor Pengukuhan Pengusaha Kena Pajak (NPPKP) : </label>
                                        <input type="text" class="form-control" id="nppkpPerusahaan" maxlength="100" name="nppkpPerusahaan" value="{{old('nppkpPerusahaan')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranNPPKP">Lampiran NPPKP : </label>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="lampiranNPPKP" name="lampiranNPPKP"> 
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="tdpPerusahaan"> Nomor Tanda Daftar Perusahaan (TDP) : </label>
                                        <input type="text" class="form-control" id="tdpPerusahaan" maxlength="100" name="tdpPerusahaan" value="{{old('tdpPerusahaan')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranTDP">Lampiran TDP : </label>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="lampiranTDP" name="lampiranTDP"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="aktaNotarisPerusahaan">Akta Notaris : </label>
                                        <input type="text" class="form-control" id="aktaNotarisPerusahaan" maxlength="100" name="aktaNotarisPerusahaan" value="{{old('aktaNotarisPerusahaan')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="skdPerusahaan">Surat Keterangan Domisili : <span class="danger">*</span></label>
                                        <select class="custom-select form-control required" id="skdPerusahaan" name="skdPerusahaan">
                                            <option value="">Silakan Pilih</option>
                                            <option value="1">Ada</option>
                                            <option value="2">Tidak Ada</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nmBank1">Nama Bank 1 : <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="nmBank1" maxlength="100" name="nmBank1" value="{{old('nmBank1')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="norekBank1">No Rekening 1 : <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="norekBank1" maxlength="50" name="norekBank1" value="{{old('norekBank1')}}"> 
                                    </div>
                                </div>                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranRek1">Lampiran Rekening 1 : <span class="danger">*</span></label>
                                        <input type="file" class="dropify required" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="supBank1File" name="supBank1File"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nmBank2">Nama Bank 2 :  </label>
                                        <input type="text" class="form-control" id="nmBank2" maxlength="100"  name="nmBank2" value="{{old('nmBank2')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="norekBank2">No Rekening 2 : </label>
                                        <input type="text" class="form-control" id="norekBank2" maxlength="50" name="norekBank2" value="{{old('norekBank2')}}"> 
                                    </div>
                                </div>                              
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranRek2">Lampiran Rekening 2 : </label>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="supBank2File" name="supBank2File"> 
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 2 -->
                        <h6>Data Penanggung Jawab Perusahaan</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="pemipinPerusahaan">Nama Pemimpin : <span class="danger">*</span></label>
                                        <input type="text" class="form-control required" id="pemipinPerusahaan" maxlength="100" name="pemipinPerusahaan" value="{{old('pemipinPerusahaan')}}">
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="jabatanPemimpin">Jabatan : <span class="danger">*</span></label>
                                        <input type="text" class="form-control required" id="jabatanPemimpin" maxlength="100" name="jabatanPemimpin" value="{{old('jabatanPemimpin')}}"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="emailPemimpin"> Email : <span class="danger">*</span> </label>
                                        <input type="email" class="form-control required" id="emailPemimpin" maxlength="100" name="emailPemimpin" value="{{old('emailPemimpin')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="noTlpPemimpin">No. Telepon : </label>
                                        <input type="tel" class="form-control" id="noTlpPemimpin" maxlength="20" name="noTlpPemimpin" value="{{old('noTlpPemimpin')}}"> 
                                    </div>
                                </div>                                
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="noHpPemimpin">No. Handphone : <span class="danger">*</span> </label>
                                        <input type="tel" class="form-control required" id="noHpPemimpin" maxlength="20" name="noHpPemimpin" value="{{old('noHpPemimpin')}}"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="alamatPemimpin">Alamat Rumah : <span class="danger">*</span></label>
                                        <textarea name="alamatPemimpin" id="alamatPemimpin" rows="6" class="form-control required">{{old('alamatPemimpin')}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="ktpPemimpin"> Nomor KTP Pemimpin: <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="ktpPemimpin" maxlength="50" name="ktpPemimpin" value="{{old('ktpPemimpin')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pemimpinKTPFile">Lampiran KTP Pemimpin : <span class="danger">*</span> </label>
                                        <input type="file" class="dropify required" data-height="25" data-max-file-size="1M" id="pemimpinKTPFile" name="pemimpinKTPFile"> 
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 3 -->
                        <h6>Data Pemilik Perusahaan</h6>                        
                        <section>
                            <div class="row">
                                <div class="col-md-10">
                                    <p>Apabila pemilik perusahaan lebih dari satu, sebutkan salah satu identitas yang mempunyai modal dominan, sedangkan yang lainnya diisi dalam lebar tersendiri</p>
                                    <br />
                                </div>                                
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="pemilikPerusahaan">Nama Pemilik : <span class="danger">*</span></label>
                                        <input type="text" class="form-control required" id="pemilikPerusahaan" maxlength="100" name="pemilikPerusahaan" value="{{old('pemilikPerusahaan')}}">
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="jabatanPemilik">Jabatan : <span class="danger">*</span></label>
                                        <input type="text" class="form-control required" id="jabatanPemilik" maxlength="100" name="jabatanPemilik" value="{{old('jabatanPemilik')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="emailPemilik"> Email : <span class="danger">*</span> </label>
                                        <input type="email" class="form-control required" id="emailPemilik" maxlength="100" name="emailPemilik" value="{{old('emailPemilik')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="noTlpPemilik">No. Telepon : </label>
                                        <input type="tel" class="form-control" id="noTlpPemilik" maxlength="20" name="noTlpPemilik" value="{{old('noTlpPemilik')}}"> 
                                    </div>
                                </div>                                
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="noHpPemilik">No. Handphone : <span class="danger">*</span> </label>
                                        <input type="tel" class="form-control required" id="noHpPemilik" maxlength="20" name="noHpPemilik" value="{{old('noHpPemilik')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="alamatPemilik">Alamat Rumah : <span class="danger">*</span></label>
                                        <textarea name="alamatPemilik" id="alamatPemilik" rows="6" class="form-control required">{{old('alamatPemilik')}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="ktpPemilik"> Nomor KTP Pemilik: <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="ktpPemilik" maxlength="50" name="ktpPemilik" value="{{old('ktpPemilik')}}"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="PemilikKTPFile">Lampiran KTP Pemilik : <span class="danger">*</span> </label>
                                        <input type="file" class="dropify required" data-height="25" data-max-file-size="1M" id="PemilikKTPFile" name="PemilikKTPFile"> 
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 4 -->
                        <h6>Lain - Lain</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-10">
                                <br />
                                    <ul class="search-listing">
                                        <li>
                                            <h3>KETENTUAN DAN PERSYARATAN YANG BERLAKU</h3>
                                            <p>
                                               Calon supplier/pemasok dengan ini menyatakan tanpa paksaan maupun tekanan dari pihak manapun untuk tunduk,
                                               patuh dan terikat terhadap segala ketentuan-ketentuan maupun kebijakan-kebijakan dan/atau sanksi-sanksi yang
                                               berlaku pada PT Semen Indonesia Logistik yang telah ada maupun akan dan/atau mungkin timbul dikemudian hari.
                                            </p>
                                        </li>
                                        <li>
                                            <h3>TEMPAT KEDUDUKAN HUKUM</h3>
                                            <p>
                                                Apabila terjadi perselisihan dengan kedua belah pihak, maka kedua belah pihak akan menyelesaikan dan menentukan tempat
                                                kedudukan hukum yang tepat di Kantor Panitera Pengadilan Tinggi Gresik
                                            </p>
                                        </li>
                                        <li>
                                            <h3>PERNYATAAN</h3>
                                            <p>
                                                Dengan ini saya calon supplier/pemasok menyatakan dengan sebenar-benarnya, bahwa informasi diatas adalah sesuai
                                                dengan kondisi yang ada. Dan apabila saya memberikan keterangan yang tidak sesuai dengan kenyataan yang ada, maka
                                                saya bersedia dituntut sesuai hukum yang berlaku.
                                                <br />
                                                Saya dengan ini memberikan kuasa pada PT Semen Indonesia Logistik untuk menghubungi pada pihak manapun guna memperoleh
                                                dan memverifikasi keterangan yang diperlukan.
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password"> Password: <span class="danger">*</span> </label>
                                        <input type="password" class="form-control required" id="password" name="password"> 
                                    </div>
                                </div>
                                <!--
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="repassword"> Repassword: <span class="danger">*</span> </label>
                                        <input type="password" class="form-control required" id="repassword" name="repassword">   
                                    </div>
                                </div>
                                -->
                                <div class="col-md-10">                                    
                                    <div class="form-group">
                                        <label>Unit :</label>
                                            <div class="c-inputs-stacked">
                                                @foreach($dtUnit as $vUnit)
                                                <label class="inline custom-control custom-checkbox block">
                                                    <input type="checkbox" name="unit[{{ $vUnit['unitId'] }}]" class="custom-control-input"><span class="custom-control-label ml-0"><a href="" data-toggle="modal" data-target="#modal-{{$vUnit['unitId']}}">{{$vUnit['unit']}}</a></span>
                                                </label>
                                                <div id="modal-{{$vUnit['unitId']}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Modal Heading</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h4>{{ $vUnit['unit']}}</h4>
                                                                <p>{{ $vUnit['keterangan'] }}</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                @endforeach
                                            </div>
                                    </div>                                    
                                </div>
                                <div class="col-md-10">
                                    <div class="c-inputs-stacked">
                                        <label class="inline custom-control custom-checkbox block">
                                            <input type="checkbox" name="setuju" class="custom-control-input required"> <span class="custom-control-label ml-0">Setuju *</span>
                                        </label>                                        
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/wizard/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/wizard/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/wizard/steps.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <!--<script src="{{ asset('js/jquery-latest.js') }}"></script>
    <script src="{{ asset('js/jquery.chained.js') }}"></script>-->

    <script>
    
     $(document).ready(function() {
         $(".dinamic").change(function(){
             //alert('Alhamdulillah');
             if($(this).val() != ''){
                 var select = $(this).attr('id');
                 var value = $(this).val();
                 var dependent = $(this).data('dependent');
                 var _token = $('input[name="_token"]').val();
                 $.ajax({
                     url : "{{ url('/') }}/"+dependent,
                     method : "POST",
                     data : {select:select, value:value, _token:_token, dependent:dependent},
                     success : function(result){
                         //alert(result);
                         $('#'+dependent).html(result);
                     }
                 })
             }
         });
         $(".select2").select2();
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
@endpush
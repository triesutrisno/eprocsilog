<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-small-cap">PERSONAL</li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="{{ url('/') }}" aria-expanded="false">
                        <i class="mdi mdi-home-outline"></i>
                        <span class="hide-menu">Beranda</span>
                    </a>
                </li>
                @if (!isset(session('user')->name))
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="{{ url('/kebijakan') }}"
                            aria-expanded="false">
                            <i class="mdi mdi-bullseye"></i>
                            <span class="hide-menu">Kebijakan</span>
                        </a>

                    </li>
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="{{ url('/info') }}" aria-expanded="false">
                            <i class="mdi mdi-information-outline"></i>
                            <span class="hide-menu">Informasi</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                            <i class="mdi mdi-chart-bubble"></i>
                            <span class="hide-menu">Pengumuman </span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="#">Pengumuman Lelang </a></li>
                            <li><a href="#">Pengumuman Pemenang</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="{{ url('/register') }}"
                            aria-expanded="false">
                            <i class="mdi mdi-google-pages"></i>
                            <span class="hide-menu">Pendaftaran Rekanan </span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="{{ url('/faq') }}" aria-expanded="false">
                            <i class="mdi mdi-find-replace"></i>
                            <span class="hide-menu">F.A.Q </span>
                        </a>
                    </li>
                @else
                    @if (session('user')->role == 'supplier')
                        <li>
                            <a class="has-arrow waves-effect waves-dark"
                                href="{{ url('/datapribadi') }}/{{ session('user')->username }}" aria-expanded="false">
                                <i class="mdi mdi-account"></i>
                                <span class="hide-menu">Data Pribadi</span>
                            </a>
                        </li>
                        <!-- <li>
                            <a class="has-arrow waves-effect waves-dark"
                                href="{{ url('/kegiatanpengadaan') }}/{{ session('user')->username }}"
                                aria-expanded="false">
                                <i class="mdi mdi-cart-outline"></i>
                                <span class="hide-menu">Kegiatan Pengadaan</span>
                            </a>
                        </li>
                        <li>
                            <a class="has-arrow waves-effect waves-dark"
                                href="{{ url('/permintaanpembayaran') }}/{{ session('user')->username }}"
                                aria-expanded="false">
                                <i class="mdi mdi-wallet-giftcard"></i>
                                <span class="hide-menu">Permintaan Pembayaran</span>
                            </a>
                        </li> -->
                        <!-- @if (session('user')->statusTMS == '1')
                            <li>
                                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                                    <i class="mdi mdi-truck-delivery"></i>
                                    <span class="hide-menu">Mitra Carter</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li>
                                        <a href="{{ url('/prematchingtruk') }}/{{ session('user')->username }}"
                                            aria-expanded="false">
                                            <i class="ti ti-angle-double-right"></i>
                                            <span class="hide-menu">Pre-matching Truk</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/monitoringdo') }}/{{ session('user')->username }}"
                                            aria-expanded="false">
                                            <i class="ti ti-angle-double-right"></i>
                                            <span class="hide-menu">Monitoring DO</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/monitoringarmada') }}/{{ session('user')->username }}"
                                            aria-expanded="false">
                                            <i class="ti ti-angle-double-right"></i>
                                            <span class="hide-menu">Monitoring Armada</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endif -->
                    @elseif(session('user')->role == 'admin')
                        <!-- <li>
                            <a class="has-arrow waves-effect waves-dark" href="{{ url('/tender') }}"
                                aria-expanded="false">
                                <i class="mdi mdi-cart-outline"></i>
                                <span class="hide-menu">Kegiatan Pengadaan</span>
                            </a>
                        </li>
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="{{ url('/permintaanpembayaran') }}"
                                aria-expanded="false">
                                <i class="mdi mdi-wallet-giftcard"></i>
                                <span class="hide-menu">Permintaan Pembayaran</span>
                            </a>
                        </li> -->
                    @elseif(session('user')->role == 'administrator')
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                                <i class="mdi mdi-widgets"></i>
                                <span class="hide-menu">Master</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{ url('/kategori') }}" aria-expanded="false">
                                        <i class="ti ti-angle-double-right"></i>
                                        <span class="hide-menu">Master Kategori</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/kualifikasi') }}" aria-expanded="false">
                                        <i class="ti ti-angle-double-right"></i>
                                        <span class="hide-menu">Master Kualifikasi</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/daftarvendor') }}" aria-expanded="false">
                                        <i class="ti ti-angle-double-right"></i>
                                        <span class="hide-menu">Daftar Vendor</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/nextnumber') }}" aria-expanded="false">
                                        <i class="ti ti-angle-double-right"></i>
                                        <span class="hide-menu">Master Nextnumber</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- <li>
                            <a class="has-arrow waves-effect waves-dark" href="{{ url('/tender') }}"
                                aria-expanded="false">
                                <i class="mdi mdi-cart-outline"></i>
                                <span class="hide-menu">Kegiatan Pengadaan</span>
                            </a>
                        </li>
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="{{ url('/permintaanpembayaran') }}"
                                aria-expanded="false">
                                <i class="mdi mdi-wallet-giftcard"></i>
                                <span class="hide-menu">Permintaan Pembayaran</span>
                            </a>
                        </li> -->
                    @else
                        <li></li>
                    @endif
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="{{ url('/berita-acara') }}"
                            aria-expanded="false">
                            <i class="mdi mdi-newspaper"></i>
                            <span class="hide-menu">Berita Acara SI</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="{{ url('/berita-acara-sbi') }}"
                            aria-expanded="false">
                            <i class="mdi mdi-newspaper"></i>
                            <span class="hide-menu">Berita Acara SBI</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="{{ url('/berita-acara-sp-zak') }}"
                            aria-expanded="false">
                            <i class="mdi mdi-newspaper"></i>
                            <span class="hide-menu">BA Semen Padang Zak</span>
                        </a>
                    </li>
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="{{ url('/berita-acara-sp-curah') }}"
                            aria-expanded="false">
                            <i class="mdi mdi-newspaper"></i>
                            <span class="hide-menu">BA Semen Padang Curah</span>
                        </a>
                    </li>
                    @if (session('user')->role == 'administrator')
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                            <i class="mdi mdi-file-document"></i>
                            <span class="hide-menu">Invoice Monitor</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li>
                                <a href="{{ url('/invoice-monitor') }}" aria-expanded="false">
                                    <i class="ti ti-angle-double-right"></i>
                                    <span class="hide-menu">Invoice Monitor</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/invoice-monitor-upload') }}" aria-expanded="false">
                                    <i class="ti ti-angle-double-right"></i>
                                    <span class="hide-menu">Upload Invoice Monitor</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/invoice-monitor-all') }}" aria-expanded="false">
                                    <i class="ti ti-angle-double-right"></i>
                                    <span class="hide-menu">Invoice Monitor History</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @else
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="{{ url('/invoice-monitor') }}"
                            aria-expanded="false">
                            <i class="mdi mdi-file-document"></i>
                            <span class="hide-menu">Invoice Monitor</span>
                        </a>
                    </li>
                    @endif
                @endif
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->

@extends('layouts.main')

@push('styles')
    
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!--
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Beranda</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
            </ol>
        </div>
    </div>
    -->
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title text-danger"><strong>Selamat Datang</strong></h3>
                    <h3 class="card-title text-success"><strong>E-PROCUREMENT SILOG GROUP</strong></h3>
                    <p class="text-justify">
                        Merupakan aplikasi yang bertujuan untuk memberikan informasi resmi kepada rekanan Semen Indonesia Logistik Group mengenai kegiatan pengadaan barang dan jasa, 
                        serta membantu rekanan dalam pengajuan & monitoring permintaan pembayaran yang telah diajukan kepada Semen Indonesia Logistik Group
                    </p>
                    @if(!isset(session('user')->name))
                    <div class="row button-group">
                        <div class="col-lg-4 col-md-4">
                            <a href="{{ url('/login') }}" class="btn btn-block btn-outline-success">MASUK</a>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <a href="{{ url('/register') }}" class="btn waves-effect waves-light btn-block btn-success">DAFTAR</a>
                        </div>                        
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <!-- style="background-image:url(public/assets/images/background/login-register.jpg);" -->
            <div>
                <img src="{{ url('assets/images/background/gambarSilogGroup.png') }}">
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    
@endpush
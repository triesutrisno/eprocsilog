<!-- ============================================================== -->
<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/') }}">
                <!-- Logo icon --><b>
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <!--<img src="{{ asset('assets/images/silogLogo.png') }}" alt="homepage" class="dark-logo" />-->
                    <!-- Light Logo icon -->
                    <img src="{{ asset('assets/images/silogLogoPutih.png') }}" alt="homepage" class="light-logo" />
                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span>
                    <!-- dark Logo text -->
                    <!--<img src="{{ asset('assets/images/logo-text.png') }}" alt="homepage" class="dark-logo" />-->
                    <!-- Light Logo text -->    
                    &nbsp;&nbsp;<img src="{{ asset('assets/images/silogtext.png') }}" class="light-logo" alt="homepage" />                    
                </span> 
            </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item hidden-sm-down"><span></span></li>
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">                
                <!-- ============================================================== -->
                <!-- Profile -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    @if(isset(session('user')->name))
                        <a href="{{ url('/keluar') }}" class="btn waves-effect waves-light btn-sm btn-warning">Keluar</a>
                    @else
                        <a href="{{ url('/login') }}" class="btn waves-effect waves-light btn-sm btn-warning">Masuk</a>
                    @endif                   
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
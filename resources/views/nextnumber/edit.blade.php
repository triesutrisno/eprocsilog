@extends('layouts.main')

@push('styles')
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Ubah Data Master Nextnumber</strong></h4>
                <div class="card-body">
                    <form action="{{ url('/nextnumber/editpost') }}/{{ $model[0]['id'] }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tahun">Tahun : <span class="danger">*</span></label>
                                    <select class="custom-select form-control" required id="tahun" name="tahun">
                                        <option value="">Silakan Pilih</option>
                                        @php 
                                        $now = "2020";
                                        $upto = date("Y") + 1;
                                        for($i=$now;$i<=$upto;$i++){ 
                                        @endphp                                 
                                        <option value="{{ $i }}" {{ $model[0]['tahun']== $i ? 'selected' : '' }}>{{ $i }}</option>
                                        @php } @endphp
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="comp">Company : <span class="danger">*</span></label>
                                    <select class="custom-select form-control" required id="comp" name="comp">
                                        <option value="">Silakan Pilih</option>
                                        <option value="10100" {{ $model[0]['comp']== '10100' ? 'selected' : '' }}>SILOG</option>
                                        <option value="10200" {{ $model[0]['comp']== '10200' ? 'selected' : '' }}>SID</option>
                                        <option value="10300" {{ $model[0]['comp']== '10300' ? 'selected' : '' }}>VUDS</option>
                                        <option value="10400" {{ $model[0]['comp']== '10400' ? 'selected' : '' }}>VULS</option>
                                        <option value="10500" {{ $model[0]['comp']== '10500' ? 'selected' : '' }}>VUBA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="kode">Unit : <span class="danger">*</span></label>
                                    <select class="custom-select form-control" required id="kode" name="kode">
                                        <option value="">Silakan Pilih</option>
                                        @foreach($dtUnit as $vUnit)
                                            <option value="{{ $vUnit['kode'] }}" {{ $model[0]['kode']== $vUnit['kode'] ? 'selected' : '' }}>{{ $vUnit['unit'] }}</option>
                                        }
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis">Jenis : <span class="danger">*</span></label>
                                    <select class="custom-select form-control" required id="jenis" name="jenis">
                                        <option value="">Silakan Pilih</option>
                                        <option value="PB" {{ $model[0]['jenis']== 'PB' ? 'selected' : '' }}>Pengadaan Barang</option>
                                        <option value="PJ" {{ $model[0]['jenis']== 'PJ' ? 'selected' : '' }}>Pengadaan Jasa</option>
                                    </select>
                                </div>
                            </div> <div class="col-md-3">
                                <div class="form-group">
                                    <label for="jenis">Urutan : <span class="danger">*</span></label>
                                    <input type="text" name="urutan" id="urutan" class="form-control" required value="{{ $model[0]['urutan'] }}">
                                </div>
                            </div>                                               
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="status">Status :</label>
                                    <select class="custom-select form-control required" id="status" name="status">
                                        <option value="">Silakan Pilih</option>
                                        <option value="1" {{ $model[0]['status']== '1' ? 'selected' : '' }}>Aktif</option>
                                        <option value="2" {{ $model[0]['status']== '2' ? 'selected' : '' }}>Tidak Aktif</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keterangan">Keterangan : <span class="danger">*</span></label>
                                    <textarea name="keterangan" id="keterangan" rows="6" class="form-control" required>{{ $model[0]['keterangan'] }}</textarea>
                                </div>
                            </div>    
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div  class="col-lg-2 col-md-2">
                                <button class="btn btn-block btn-success" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('js/custom.min.js') }}"></script>
<script src="{{ asset('js/validation.js') }}"></script>
@endpush
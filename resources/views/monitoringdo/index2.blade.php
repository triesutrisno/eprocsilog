@extends('app')

@push('styles')
<link href="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/media/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<link href="{{ asset('css/dataTablesCustom.css') }}" rel="stylesheet">
<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>

@endpush

@section('content')
<div id="module-pages" class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Monitoring DO</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                <li class="breadcrumb-item active">Monitoring DO</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <!-- column -->
        <div class="col-lg-12">
            <div class="card">
                {!! Form::open(['method' => 'post', 'route' => 'tms-monitoring-do.index', 'class'=>'form']) !!}
                <div class="card-body">
                    <h4 class="card-title"><span class="lstick"></span>Cari Data</h4>
                    <div class="chat-box">
                        <div class="row p-t-20">
                            <div class="col-lg-3 col-md-12">
                                <h6 class="m-t-0">Plant</h6>
                                <div class="input-group">
                                    <select name="kdPlant" id="kdPlant" class="form-control">
                                        <option value="">Silakan Pilih</option>
                                        @foreach($model['data2'] as $val2)
                                            <option value="{{ trim($val2['abalky']) }}" {{ $param['kdPlant']==trim($val2['abalky']) ? 'selected' : '' }}>{{ trim($val2['abalky']) }} | {{ $val2['abalph'] }}</option>                                        
                                        @endforeach
                                    </select>
                                </div>  
                            </div> 
                            <div class="col-lg-2 col-md-12">
                                <h6 class="m-t-0">Tanggal Awal</h6>
                                <div class="input-group">
                                    <input type="text" name="tglA" class="form-control" id="datepicker-autoclose" autocomplete="off" value="{{ $param["tglA"] }}" >
                                    <div class="input-group-append" id="input-group-append-id">
                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                    </div>
                                </div>  
                            </div>                                
                            <div class="col-lg-2 col-md-12">
                                <h6 class="m-t-0">Tanggal Akhir</h6>
                                <div class="input-group">
                                    <input type="text" name="tglB" class="form-control" id="datepicker-autoclose2" autocomplete="off" value="{{ $param["tglB"] }}" >
                                    <div class="input-group-append" id="input-group-append-id2">
                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                    </div>
                                </div>  
                            </div>
                            <div class="col-lg-2 col-md-12">
                                <h6 class="m-t-0">Nopin</h6>
                                <div class="input-group">
                                    <input type="text" name="nopin" class="form-control" value="{{ $param['nopin'] }}" autocomplete="off">
                                </div>  
                            </div> 
                            <div class="col-lg-2 col-md-12">
                                <h6 class="m-t-0">No Do/SPJ</h6>
                                <div class="input-group">
                                    <input type="text" name="noDo" class="form-control" value="{{ $param['noDo'] }}" autocomplete="off">
                                </div>  
                            </div>
                        </div>
                        <div class="row p-t-20">
                            <div class="col-lg-3 col-md-12">
                                <h6 class="m-t-0">Status</h6>
                                <div class="input-group">
                                    <select name="status" class="form-control">
                                        <option value="">Silakan Pilih</option>
                                        <option value="10" {{ $param['status']=='10' ? 'selected' : '' }}>Armada Siap</option>
                                        <option value="17" {{ $param['status']=='17' ? 'selected' : '' }}>Pre-Matching Vendor</option>
                                        <option value="17" {{ $param['status']=='18' ? 'selected' : '' }}>Pre-Matching Silog</option>
                                        <option value="20"{{ $param['status']=='20' ? 'selected' : '' }}>Sudah Match DO</option>
                                        <option value="21"{{ $param['status']=='21' ? 'selected' : '' }}>Approve Match</option>
                                        <option value="22"{{ $param['status']=='22' ? 'selected' : '' }}>Konfirmasi Operasional</option>
                                        <option value="30"{{ $param['status']=='30' ? 'selected' : '' }}>Berangkat Ke Pemuatan</option>
                                        <option value="35"{{ $param['status']=='35' ? 'selected' : '' }}>Sampai ke Pemuatan</option>
                                        <option value="36"{{ $param['status']=='36' ? 'selected' : '' }}>Konfirmasi Alamat Kirim</option>
                                        <option value="39"{{ $param['status']=='39' ? 'selected' : '' }}>Selesai Muat</option>
                                        <option value="40"{{ $param['status']=='40' ? 'selected' : '' }}>Berangkat ke Tujuan</option>
                                        <option value="40"{{ $param['status']=='41' ? 'selected' : '' }}>Berangkat ke Tujuan (Tanpa Balen)</option>
                                        <option value="45"{{ $param['status']=='45' ? 'selected' : '' }}>Sampai Tujuan</option>
                                        <option value="48"{{ $param['status']=='48' ? 'selected' : '' }}>Bongkar - Parsial</option>
                                        <option value="49"{{ $param['status']=='49' ? 'selected' : '' }}>Selesai Bongkar</option>
                                        <option value="50"{{ $param['status']=='50' ? 'selected' : '' }}>Berangkat Kembali</option>
                                        <option value="58"{{ $param['status']=='58' ? 'selected' : '' }}>Konfirm Kembali dari DO Lain</option>
                                        <option value="59"{{ $param['status']=='59' ? 'selected' : '' }}>Sampai Kembali</option>
                                        <option value="60"{{ $param['status']=='60' ? 'selected' : '' }}>Dokumen Kembali</option>
                                        <option value="61"{{ $param['status']=='61' ? 'selected' : '' }}>Dokumen Belum Lengkap</option>
                                        <option value="70"{{ $param['status']=='70' ? 'selected' : '' }}>Verifikasi Dokumen Lengkap</option>
                                        <option value="99"{{ $param['status']=='99' ? 'selected' : '' }}>Close</option>
                                        <option value="X1"{{ $param['status']=='X1' ? 'selected' : '' }}>Cancel - Laka</option>
                                        <option value="X2"{{ $param['status']=='X2' ? 'selected' : '' }}>Cancel - Driver Sakit</option>
                                        <option value="X3"{{ $param['status']=='X3' ? 'selected' : '' }}>Cancel - Driver Menolak DO</option>
                                        <option value="X4"{{ $param['status']=='X4' ? 'selected' : '' }}>Cancel - Penelantaran Armada</option>
                                        <option value="X5"{{ $param['status']=='X5' ? 'selected' : '' }}>Cancel - Kerusakan Armada</option>
                                        <option value="X6"{{ $param['status']=='X6' ? 'selected' : '' }}>Cancel - Alasan Operasional</option>
                                        <option value="X7"{{ $param['status']=='X7' ? 'selected' : '' }}>Cancel - Salah Matching</option>
                                    </select>
                                </div>
                            </div>                             
                            <div class="col-lg-2 col-md-12">
                                <h6 class="m-t-0">SPJ External</h6>
                                <div class="input-group">
                                    <input type="text" name="spjexternal" class="form-control" value="{{ $param['spjexternal'] }}" autocomplete="off">
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body b-t">
                    <div class="row">
                        <div class="col-lg-2 col-md-12">
                            <a href="{{route('tms-monitoring-do.index')}}" class="waves-effect waves-light btn btn-success">Lihat Data</a>
                            <button type="submit" class="btn waves-effect waves-light btn-warning">Cari</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-lg-12">
                @include('partials.alert')
                @if(session('kode')=="99")
                <div class="card-body">  
                    <div class="alert alert-success">
                        <h6 class="card-title"> 
                            <i>{{session('pesan')}}</i> &nbsp;&nbsp;
                            <i class="fas fa-exclamation"></i>
                        </h6>
                    </div>
                </div>
                @elseif(session('kode')=="90")
                <div class="card-body">  
                    <div class="alert alert-danger">
                        <h6 class="card-title"> 
                            <i>{{session('pesan')}}</i> &nbsp;&nbsp;
                            <i class="fas fa-exclamation"></i>
                        </h6>
                    </div>
                </div>
                @endif
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <div id="example23_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                                <table id='tms-logtelegram' class="display table table-hover table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid">
                                                    <thead>
                                                        <tr>
                                                            <td align="center" width="100">Status</td>
                                                            <td width="50">Kode Branch</td>
                                                            <td align="center" width="100">No DO/SPJ</td>
                                                            <td align="center" width="50">Type</td>
                                                            <td width="50">Line</td>
                                                            <td align="center" width="100">Nopin</td>
                                                            <td align="center" width="100">Nopol</td>
                                                            <td width="150">Tgl Matching</td>
                                                            <td width="150">Tgl Berangakat Muat</td>
                                                            <td width="150">Tgl Masuk Plant</td>
                                                            <td width="150">Tgl Keluar Plant</td>
                                                            <td width="150">Tgl Masuk Bongkaran</td>
                                                            <td width="150">Tgl Keluar Bongkaran</td>
                                                            <td width="70">Lead Time Berangkat</td>
                                                            <td width="150">Tgl Epod</td>
                                                            <td width="150">Status Epod</td>
                                                            <td width="150">Tgl Transfer</td>
                                                            <td width="50">SPJ External</td>
                                                            <td width="50">Kode Plant</td>
                                                            <td width="150">Nama Plant</td>
                                                            <td width="50">Kode Shipto</td>
                                                            <td width="150">Nama Shipto</td>
                                                            <td width="50">Kode Driver</td>
                                                            <td width="150">Nama Driver</td>
                                                            <td>Nama Pelangan</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody> 
                                                    @foreach($model['data'] as $key => $val)
                                                        @php $no = (int)$key+1; @endphp 
                                                        <tr>
                                                            <td align="center">
                                                                @if($val['sts_perj'] == '10')
                                                                    <label class="badge badge-warning">Armada Siap</label>
                                                                @elseif($val['sts_perj'] == '17')
                                                                    <label class="badge badge-warning">Pre-Matching Vendor</label>
                                                                @elseif($val['sts_perj'] == '18')
                                                                    <label class="badge badge-warning">Pre-Matching Silog</label>
                                                                @elseif($val['sts_perj'] == '20')
                                                                    <label class="badge badge-warning">Sudah Match DO</label>
                                                                @elseif($val['sts_perj'] == '21')
                                                                    <label class="badge badge-warning">Approve Match</label>
                                                                @elseif($val['sts_perj'] == '22')
                                                                    <label class="badge badge-warning">Konfirmasi Operasional</label>
                                                                @elseif($val['sts_perj'] == '30')
                                                                    <label class="badge badge-info">Berangkat ke Pemuatan</label>
                                                                @elseif($val['sts_perj'] == '35')
                                                                    <label class="badge badge-info">Sampai ke Pemuatan</label>
                                                                @elseif($val['sts_perj'] == '36')
                                                                    <label class="badge badge-info">Konfirmasi Alamat Kirim</label>
                                                                @elseif($val['sts_perj'] == '39')
                                                                    <label class="badge badge-info">Selesai Muat</label>
                                                                @elseif($val['sts_perj'] == '40')
                                                                    <label class="badge badge-info">Berangkat ke Tujuan</label>
                                                                @elseif($val['sts_perj'] == '41')
                                                                    <label class="badge badge-info">Berangkat ke Tujuan (Tanpa Balen)</label>
                                                                @elseif($val['sts_perj'] == '45')
                                                                    <label class="badge badge-info">Sampai Tujuan</label>
                                                                @elseif($val['sts_perj'] == '48')
                                                                    <label class="badge badge-info">Bongkar - Parsial</label>
                                                                @elseif($val['sts_perj'] == '49')
                                                                    <label class="badge badge-success">Selesai Bongkar</label>
                                                                @elseif($val['sts_perj'] == '50')
                                                                    <label class="badge badge-info">Berangkat Kembali</label>
                                                                @elseif($val['sts_perj'] == '51')
                                                                    <label class="badge badge-info">Berangkat Kembali dari DO Lain</label>
                                                                @elseif($val['sts_perj'] == '58')
                                                                    <label class="badge badge-info">Konfirm Kembali dari DO Lain</label>
                                                                @elseif($val['sts_perj'] == '59')
                                                                    <label class="badge badge-success">Sampai Kembali</label>
                                                                @elseif($val['sts_perj'] == '60')
                                                                    <label class="badge badge-success">Dokumen Kembali</label>
                                                                @elseif($val['sts_perj'] == '61')
                                                                    <label class="badge badge-success">Dokumen Belum Lengkap</label>
                                                                @elseif($val['sts_perj'] == '70')
                                                                    <label class="badge badge-success">Verifikasi Dokumen Lengkap</label>
                                                                @elseif($val['sts_perj'] == '99')
                                                                    <label class="badge badge-danger">Close</label>
                                                                @elseif($val['sts_perj'] == 'X1')
                                                                    <label class="badge badge-danger">Cancel - Laka</label>
                                                                @elseif($val['sts_perj'] == 'X2')
                                                                    <label class="badge badge-danger">Cancel - Driver Sakit</label>
                                                                @elseif($val['sts_perj'] == 'X3')
                                                                    <label class="badge badge-danger">Cancel - Driver Menolak DO</label>
                                                                @elseif($val['sts_perj'] == 'X4')
                                                                    <label class="badge badge-danger">Cancel - Penelantaran Armada</label>
                                                                @elseif($val['sts_perj'] == 'X5')
                                                                    <label class="badge badge-danger">Cancel - Kerusakan Armada</label>
                                                                @elseif($val['sts_perj'] == 'X6')
                                                                    <label class="badge badge-danger">Cancel - Alasan Operasional</label>
                                                                @elseif($val['sts_perj'] == 'X7')
                                                                    <label class="badge badge-danger">Cancel - Salah Matching</label>
                                                                @endif
                                                            </td>
                                                            <td align="center">{{$val['kodeplant']}}</td>
                                                            <td align="center">{{$val['no_do']}}</td>
                                                            <td align="center">{{$val['do_ty']}}</td>
                                                            <td align="center">{{$val['do_line']}}</td>
                                                            <td align="center">{{trim($val['nopin'])}}</td>
                                                            <td align="center">{{trim($val['nopol'])}}</td>
                                                            <td>{{$val['tglmatch']}}</td>
                                                            <td>
                                                                @if(date('Y', strtotime($val['tglbrgkatmuat']))!='1900')
                                                                    {{date('Y-m-d', strtotime($val['tglbrgkatmuat']))}} {{date('H:i:s', strtotime($val['jambrgkatmuat']))}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(date('Y', strtotime($val['tglmasukmuat']))!='1900')
                                                                    {{date('Y-m-d', strtotime($val['tglmasukmuat']))}} {{date('H:i:s', strtotime($val['jammasukmuat']))}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(date('Y', strtotime($val['tglkeluarmuat']))!='1900')
                                                                    {{date('Y-m-d', strtotime($val['tglkeluarmuat']))}} {{date('H:i:s', strtotime($val['jamkeluarmuat']))}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(date('Y', strtotime($val['tglmasukbongkar']))!='1900')
                                                                    {{date('Y-m-d', strtotime($val['tglmasukbongkar']))}} {{date('H:i:s', strtotime($val['jammasukbongkar']))}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(date('Y', strtotime($val['tglkeluarbongkar']))!='1900')
                                                                    {{date('Y-m-d', strtotime($val['tglkeluarbongkar']))}} {{date('H:i:s', strtotime($val['jamkeluarbongkar']))}}
                                                                @endif
                                                            </td>
                                                             <td>
                                                                @php
                                                                    $tglMasukPlant = date_create(date('Y-m-d', strtotime($val['tglkeluarmuat']))." ".date('H:i:s', strtotime($val['jamkeluarmuat'])));
                                                                    $tglKeluarPlant = date_create(date('Y-m-d', strtotime($val['tglmasukbongkar']))." ".date('H:i:s', strtotime($val['jammasukbongkar'])));
                                                                    $diff  = date_diff( $tglMasukPlant, $tglKeluarPlant );
                                                                @endphp
                                                                @if(date('Y', strtotime($val['tglmasukbongkar'])) != '1900' && date('Y', strtotime($val['tglkeluarmuat'])) != '1900')
                                                                    {{ $diff->d." Hari, ".$diff->h." Jam, ".$diff->i." Menit, ".$diff->s." Detik" }}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(date('Y', strtotime($val['tglepod']))!='1900')
                                                                    {{date('Y-m-d', strtotime($val['tglepod']))}} {{date('H:i:s', strtotime($val['jamepod']))}}
                                                                @endif
                                                            </td>
                                                            <td>
                                                                 @if(date('Y', strtotime($val['tglepod']))!='1900')
                                                                    @if($val['logprogram']=='EPOD')
                                                                        EPOD
                                                                    @else
                                                                        Manual EPOD
                                                                    @endif
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(date('Y', strtotime($val['tgltransfer']))!='1900' && date('Y', strtotime($val['tgltransfer']))!='1970')
                                                                    {{date('Y-m-d', strtotime($val['tgltransfer']))}} {{date('H:i:s', strtotime($val['jamtransfer']))}}
                                                                @endif
                                                            </td>
                                                            <td>{{trim($val['ref1'])}}</td>
                                                            <td>{{trim($val['kode_asalmuat'])}}</td>
                                                            <td>{{trim($val['nama_asalmuat'])}}</td>
                                                            <td>{{trim($val['kode_shipto'])}}</td>
                                                            <td>{{trim($val['nama_shipto'])}}</td>
                                                            <td>{{trim($val['kode_vdriver'])}}</td>
                                                            <td>{{trim($val['nama_vdriver'])}}</td>
                                                            <td>{{trim($val['nama_plg'])}}</td>
                                                        </tr>                                                    
                                                    @endforeach
                                                    </tbody>                                                
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div><!-- end row -->
                </div>
            </div>
        </div>
    </div><!-- end row -->
</div>



@endsection

@push('scripts') 
<script src="{{ asset('plugins/datatables/datatables.js') }}"></script>
<script src="{{ asset('js/dataTablesPlugins/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/dataTablesPlugins/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/dataTablesPlugins/buttons.flash.min.js') }}"></script>
<script src="{{ asset('js/dataTablesPlugins/jszip.min.js') }}"></script>
<script src="{{ asset('js/dataTablesPlugins/pdfmake.min.js') }}"></script>
<script src="{{ asset('js/dataTablesPlugins/vfs_fonts.js') }}"></script>
<script src="{{ asset('js/dataTablesPlugins/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/dataTablesPlugins/buttons.print.min.js') }}"></script>
<script src="{{ asset('js/dataTablesPlugins/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('js/dataTablesPlugins/dataTables.select.min.js') }}"></script>
<script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

<script>
    $(document).ready(function() {
        $('#tms-logtelegram').DataTable( {
            dom: '<"row m-0" <"col-sm-3 p-0"l><"col-sm-3 p-t-10"r><"col-sm-6 p-0"<"float-right"B>> >tip',
            "lengthMenu": [[50, 100, 500, 1000, -1], [50, 100, 500, 1000, "All"]],
            "processing": true,
            //"responsive": true,
            "ordering" : true,
            "scrollY": "500px",
            "scrollX":        true,
            "scrollCollapse": true,
            "columnDefs": [
                { "width": "20%", "targets": 0 }
            ],
            "fixedColumns": true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},
            "buttons": [                
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fas fa-file-excel"></i>',
                    titleAttr: 'Excel',
                    title: 'monitoring-do',
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fas fa-file-pdf"></i>',
                    titleAttr: 'PDF',
                    title: 'monitoring-do',
                }

            ],
            "columnDefs": [        
                //{
                //    "targets": [8],
                //    "data": "8",
                //    "className": "pull-right dt-body-right",
                //    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                //},                
                //{
                //    "targets": [9],
                //    "data": "9",
                //    "className": "pull-right dt-body-right",
                //    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                //},
                //{
                //    "targets": [10],
                //    "data": "10",
                //    "className": "pull-right dt-body-right",
                //    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                //},  
                // {
               //     "targets": [11],
               //     "data": "11",
               //     "className": "pull-right dt-body-right",
               //     "render": $.fn.dataTable.render.number( '.', ',', 2 )
               // },  
               // {
               //     "targets": [12],
               //     "data": "12",
               //     "className": "pull-right dt-body-right",
               //     "render": $.fn.dataTable.render.number( '.', ',', 2 )
               // },  
               // {
               //     "targets": [13],
               //     "data": "13",
               //     "className": "pull-right dt-body-right",
               //     "render": $.fn.dataTable.render.number( '.', ',', 2 )
               // },                            
               // {
               //     "targets": [14],
               //     "data": "14",
               //     "className": "pull-right dt-body-right",
               //     "render": $.fn.dataTable.render.number( '.', ',', 2 )
               // },                            
               // {
               //     "targets": [15],
               //     "data": "15",
               //     "className": "pull-right dt-body-right",
               //     "render": $.fn.dataTable.render.number( '.', ',', 2 )
               // },                                
               // {
               //     "targets": [16],
               //     "data": "16",
               //     "className": "pull-right dt-body-right",
               //     "render": $.fn.dataTable.render.number( '.', ',', 2 )
               // },      
            ],
        } );
        
        $('#datepicker-autoclose').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true,
        });
        $('#input-group-append-id').click(function() {
            //alert("Bismillah !");
            $('#datepicker-autoclose').datepicker('show');
        });
        
        $('#datepicker-autoclose2').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayHighlight: true,
        });
        $('#input-group-append-id2').click(function() {
            //alert("Bismillah !");
            $('#datepicker-autoclose2').datepicker('show');
        });
    } );
</script>

@endpush
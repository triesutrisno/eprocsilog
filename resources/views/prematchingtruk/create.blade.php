@extends('layouts.main')

@push('styles')    
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" /> 
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Tambah Data Prematching Truk</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/prematchingtruk') }}/{{ session('user')->username }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('/prematchingtruk') }}/{{ session('user')->username }}/add" method="post">
                        @csrf
                        <div class="row">                            
                            <div class="table-responsive m-t-40">
                                <table id="tbMasterNextnumber" class="display wrap table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th align="center" width="10">No</th>
                                            <th align="center" width="10">Action</th>
                                            <th>Nama Vendor</th>
                                            <th width="150">DO</th>
                                            <th width="150">Rencana Kirim</th>
                                            <th width="150">Shipto</th>
                                            <th width="150">Muat</th>
                                            <th width="150">Item</th>
                                            <th width="50">Qty</th>
                                            <th width="30">Satuan</th>
                                            <th width="30">MOT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data['dataDO'] as $key => $dt)                                    
                                            <tr valign="middle">                                                
                                                <td align="center">{{$loop->iteration}}</td>
                                                <td align="center">
                                                    <div class="hidden-sm hidden-xs action-buttons">
                                                        <a class="btn btn-sm btn-warning" href="{{ url('/prematchingtruk') }}/{{ session('user')->username }}/prematch/{{ $dt['no_do'] }}-{{ $dt['line_do'] }}-{{ $dt['do_ty'] }}" title="Prematch"><i class="ace-icon fa fa-gears bigger-130"></i></a>
                                                    </div>
                                                </td>
                                                <td>{{ $dt['abalph'] }}</td>
                                                <td>{{ $dt['no_do'] }}-{{ $dt['line_do'] }}-{{ $dt['do_ty'] }}</td>
                                                <td>{{ $dt['tgl_req_kirim'] }}</td>
                                                <td>{{ $dt['nama_shipto'] }}</td>
                                                <td>{{ $dt['nama_muatdi'] }}</td>
                                                <td>{{ $dt['nama_brg'] }}</td>
                                                <td>{{ $dt['qty'] }}</td>
                                                <td>{{ $dt['sat'] }}</td>
                                                <td>{{ $dt['ket_mot'] }}</td>
                                            </tr>                                            
                                        @endforeach                                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('js/custom.min.js') }}"></script>
<script src="{{ asset('assets/plugins/wizard/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>  
<script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    //$('#tbMasterNextnumber').DataTable();
    $(document).ready(function() {
        $(".select2").select2();
        //$('#typeDokumen').change(function(){
        //    //alert('bismillah');
        //    var _doco = $('#noDokumen').val();
        //    var _dcto = $('#typeDokumen').val();
        //    $.ajax({
        //        url : "{{ url('getOP') }}/",
        //        method : "GET",
        //        data : {doco:_doco, dcto:_dcto},
        //        success : function(result){
        //            //alert(result);
        //            $('#dataOP').html(result);
        //            //location.reload();
        //        }
        //    })            
        //});
    });  
        
    function cekNominal(val,id) {
        var openAmount = $('#'+id).attr('data-id');
        if(parseInt(val) > parseInt(openAmount)){
            //alert(openAmount+ ' ' +val);
            alert('Mohon maaf, nilai pengajuan tidak boleh melebihi nilai Open !');
            $('#simpan').prop('disabled', true);
        }else{
            $('#simpan').prop('disabled', false);
        }
    }
</script>
@endpush
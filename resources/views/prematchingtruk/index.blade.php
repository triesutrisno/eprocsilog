@extends('layouts.main')

@push('styles')
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Prematching Truk</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/prematchingtruk') }}/{{ session('user')->username }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/prematchingtruk') }}/{{ session('user')->username }}/add" class="btn btn-block btn-success">Tambah Data</a>
                    </div>
                </div>                
                @if(session('kode')=="99")
                    <div class="alert alert-success">
                        <i class="fa fa-exclamation-circle"></i>
                        {{ session('pesan') }}
                    </div>
                @elseif(session('kode')=="90")
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-circle"></i>
                        {{ session('pesan') }}
                    </div>
                @endif                 
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="tbMasterNextnumber" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>                                                            
                                    <td align="center" width="40">Aksi</td>
                                    <td align="center" width="80">Status</td>
                                    <td align="center" width="100">Nomer</td>
                                    <td align="center" width="100">Nopin</td>
                                    <td width="100">Nopol</td>
                                    <td width="150">Nama Sopir</td>
                                    <td align="center" width="100">Tgl Rencana <br />Kirim</td>
                                    <td align="center" width="100">Tanggal <br />Pre-Match</td>
                                    <td width="100">Kode Item</td>
                                    <td>Item</td>
                                    <td width="100">Qty</td>
                                    <td width="100">Satuan</td>
                                    <td width="100">Status2</td>
                                </tr>
                            </thead>
                            <tbody> 
                            @foreach($model as $key => $val)
                                @php $no = (int)$key+1; @endphp 
                                <tr>
                                    @csrf
                                    <td align="center" width="40">
                                        <div class="hidden-sm hidden-xs action-buttons">
                                            @if($val['slssts'] == '17')
                                                <form action="{{ url('/prematchingtruk-delete')}}/{{ session('user')->username }}/{{ $val['spdoco'] }}/{{ $val['spdoc'] }}/{{ $val['spdct'] }}/{{ $val['spxlin'] }}" method="post" class="d-inline">
                                                    @method('patch')
                                                    @csrf
                                                    <button class="btn btn-sm btn-info hapusMatchingtruk"> 
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        @if($val['slssts'] == '10')
                                            <label class="badge badge-warning">Armada Siap</label>
                                        @elseif($val['slssts'] == '17')
                                            <label class="badge badge-warning">Pre-Matching Vendor</label>
                                        @elseif($val['slssts'] == '18')
                                            <label class="badge badge-warning">Pre-Matching Silog</label>
                                        @elseif($val['slssts'] == '20')
                                            <label class="badge badge-warning">Sudah Match DO</label>
                                        @elseif($val['slssts'] == '21')
                                            <label class="badge badge-warning">Approve Match</label>
                                        @elseif($val['slssts'] == '22')
                                            <label class="badge badge-warning">Konfirmasi Operasional</label>
                                        @elseif($val['slssts'] == '30')
                                            <label class="badge badge-info">Berangkat ke Pemuatan</label>
                                        @elseif($val['slssts'] == '35')
                                            <label class="badge badge-info">Sampai ke Pemuatan</label>
                                        @elseif($val['slssts'] == '36')
                                            <label class="badge badge-info">Konfirmasi Alamat Kirim</label>
                                        @elseif($val['slssts'] == '39')
                                            <label class="badge badge-info">Selesai Muat</label>
                                        @elseif($val['slssts'] == '40')
                                            <label class="badge badge-info">Berangkat ke Tujuan</label>
                                        @elseif($val['slssts'] == '41')
                                            <label class="badge badge-info">Berangkat ke Tujuan (Tanpa Balen)</label>
                                        @elseif($val['slssts'] == '45')
                                            <label class="badge badge-info">Sampai Tujuan</label>
                                        @elseif($val['slssts'] == '48')
                                            <label class="badge badge-info">Bongkar - Parsial</label>
                                        @elseif($val['slssts'] == '49')
                                            <label class="badge badge-success">Selesai Bongkar</label>
                                        @elseif($val['slssts'] == '50')
                                            <label class="badge badge-info">Berangkat Kembali</label>
                                        @elseif($val['slssts'] == '51')
                                            <label class="badge badge-info">Berangkat Kembali dari DO Lain</label>
                                        @elseif($val['slssts'] == '58')
                                            <label class="badge badge-info">Konfirm Kembali dari DO Lain</label>
                                        @elseif($val['slssts'] == '59')
                                            <label class="badge badge-success">Sampai Kembali</label>
                                        @elseif($val['slssts'] == '60')
                                            <label class="badge badge-success">Dokumen Kembali</label>
                                        @elseif($val['slssts'] == '61')
                                            <label class="badge badge-success">Dokumen Belum Lengkap</label>
                                        @elseif($val['slssts'] == '70')
                                            <label class="badge badge-success">Verifikasi Dokumen Lengkap</label>
                                        @elseif($val['slssts'] == '99')
                                            <label class="badge badge-danger">Close</label>
                                        @elseif($val['slssts'] == 'X1')
                                            <label class="badge badge-danger">Cancel - Laka</label>
                                        @elseif($val['slssts'] == 'X2')
                                            <label class="badge badge-danger">Cancel - Driver Sakit</label>
                                        @elseif($val['slssts'] == 'X3')
                                            <label class="badge badge-danger">Cancel - Driver Menolak DO</label>
                                        @elseif($val['slssts'] == 'X4')
                                            <label class="badge badge-danger">Cancel - Penelantaran Armada</label>
                                        @elseif($val['slssts'] == 'X5')
                                            <label class="badge badge-danger">Cancel - Kerusakan Armada</label>
                                        @elseif($val['slssts'] == 'X6')
                                            <label class="badge badge-danger">Cancel - Alasan Operasional</label>
                                        @elseif($val['slssts'] == 'X7')
                                            <label class="badge badge-danger">Cancel - Salah Matching</label>
                                        @endif
                                    </td>
                                    <td align="center">{{$val['spdoc']}}-{{$val['spdct']}}-{{$val['spxlin']}}</td>
                                    <td >{{$val['spvehi']}}</td>
                                    <td>{{$val['spdocso']}}</td>
                                    <td>{{ $val['spalph'] }}</td>
                                    <td>{{ date('d-m-Y', strtotime($val['tglrencanakirim'])) }}</td>
                                    <td>{{ date('d-m-Y', strtotime($val['tglprematch'])) }}</td>
                                    <td>{{ $val['splitm'] }}</td>
                                    <td>{{ $val['imdsc1'] }}</td>
                                    <td>{{ $val['spuorg'] }}</td>
                                    <td>{{ $val['spuom'] }}</td>
                                    <td>{{ $val['slssts'] }}</td>
                                </tr>                                                    
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script>
    $(document).ready(function() {        
        $('#tbMasterNextnumber').DataTable({
            order: [[12, 'asc']],
            columnDefs: [
                {
                    "targets": [12],
                    "visible": false,
                    "searchable": false,
                },
            ],
        });
        $('.hapusMatchingtruk').click(function(){
            var jawab = confirm("Anda yakin akan menghapus data ini ?");
            if (jawab === true) {
                return true;
            } else {
                return false;
            }
            
        });
    });    
</script>
@endpush
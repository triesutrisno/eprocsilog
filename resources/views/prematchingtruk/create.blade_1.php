@extends('layouts.main')

@push('styles')    
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" /> 
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Tambah Data Prematching Truk</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/prematchingtruk') }}/{{ session('user')->username }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('/prematchingtruk') }}/{{ session('user')->username }}/add" method="post">
                        @csrf
                        <div class="row">                            
                            <div class="table-responsive m-t-40">
                                <table id="tbMasterNextnumber" class="display wrap table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th align="center">No</th>
                                            <th>Nama Vendor</th>
                                            <th>DO</th>
                                            <th>Rencana Kirim</th>
                                            <th>Shipto</th>
                                            <th>Muat</th>
                                            <th>Item</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data['dataDO'] as $key => $dt)                                    
                                            <tr>                                                
                                                <td align="center">{{$loop->iteration}}</td>
                                                <td>{{ $dt['abalph'] }}</td>
                                                <td>{{ $dt['no_do'] }}-{{ $dt['line_do'] }}-{{ $dt['do_ty'] }}</td>
                                                <td>{{ $dt['tgl_req_kirim'] }}</td>
                                                <td>{{ $dt['nama_shipto'] }}</td>
                                                <td>{{ $dt['nama_muatdi'] }}</td>
                                                <td>{{ $dt['nama_brg'] }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="7">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-lg-6">
                                                            <div class="form-group">
                                                                <label for="qty">Qty : </label>
                                                                {{$dt['qty']}}
                                                            </div>
                                                        </div>                                                        
                                                        <div class="col-sm-12 col-lg-6">
                                                            <div class="col-md-7">
                                                                <div class="form-group">
                                                                    <label for="nopol">Nopol : </label>
                                                                    <select name="nopol[]" id="nopol" class="custom-select form-control select2 dinamic">
                                                                        <option value="">Silakan Pilih</option>
                                                                        @foreach($data['dataTruk'] as $val)
                                                                            <option value="{{ trim($val['nopin']) }}#{{ trim($val['nopol']) }}#{{ trim($val['kode_kuat']) }}#{{ trim($val['vmcvum']) }}#{{ trim($val['vmmcu']) }}#{{ trim($val['vmvown']) }}#{{ trim($val['vmvtyp']) }}">{{ $val['nopol'] }}</option>
                                                                        }
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-12">
                                                            <div class="form-group">
                                                                <label for="sat">Satuan : </label>
                                                                {{$dt['sat']}}
                                                            </div>
                                                        </div> 
                                                        <div class="col-sm-12 col-lg-6">
                                                            <div class="col-sm-12 col-lg-7">
                                                                <div class="form-group">
                                                                    <label for="driver">Driver : </label>
                                                                    <select name="driver[]" id="driver" class="custom-select form-control select2 dinamic">
                                                                        <option value="">Silakan Pilih</option>
                                                                        @foreach($data['dataSopir'] as $dtSopir)
                                                                            <option value="{{ trim($dtSopir['kode_drv']) }}#{{ trim($dtSopir['nama_drv']) }}#{{ trim($dtSopir['abalky']) }}">{{ trim($dtSopir['nama_drv']) }}</option>
                                                                        }
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>   
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <div class="form-group">
                                                                <label for="sat">MOT : </label>
                                                                {{$dt['ket_mot']}}
                                                            </div>
                                                        </div>
                                                        <!--
                                                        <div class="row">
                                                            <div class="col-sm-12 col-lg-12">
                                                                <div class="card card-inverse">
                                                                    <div class="card-body">
                                                                        <label>1. REM : Rem tangan, rem kaki, rem angin, drain tangki udara, indikator tekanan udara dan level oli rem </label>
                                                                        <div class="demo-radio-button">
                                                                            <input type="radio" name="rem[]" id="remYa{{$key}}" value="1" class="filled-in chk-col-teal" />
                                                                            <label for="remYa{{$key}}">Ya</label>
                                                                            <input type="radio" name="rem[]" id="remPerbaikan{{$key}}" value="2" class="radio-col-red" />
                                                                            <label for="remPerbaikan{{$key}}">Perlu Perbaikan</label>
                                                                            <input type="radio" name="rem[]" id="remTidak{{$key}}" value="3" class="radio-col-red" />
                                                                            <label for="remTidak{{$key}}">Tidak Tersedia</label>
                                                                        </div>
                                                                        <label>2. KEMUDI : Level oli power steering, spelling steer </label>
                                                                        <div class="demo-radio-button">
                                                                            <input type="radio" name="kemudi[]" id="kemudiYa{{$key}}" value="1" class="filled-in chk-col-teal" />
                                                                            <label for="kemudiYa{{$key}}">Ya</label>
                                                                            <input type="radio" name="kemudi[]" id="kemudiPerbaikan{{$key}}" value="2" class="radio-col-red" />
                                                                            <label for="kemudiPerbaikan{{$key}}">Perlu Perbaikan</label>
                                                                            <input type="radio" name="kemudi[]" id="kemudiTidak{{$key}}" value="3" class="radio-col-red" />
                                                                            <label for="kemudiTidak{{$key}}">Tidak Tersedia</label>
                                                                        </div>
                                                                        <label>3. BAN : Ban (terpasang dengan serep) memenuhi kedalaman minimum 1,5 mm, ban tidak sobek, baut lengkap, tekanan angian sesuai standart,
                                                                            velg tidak retak, kancing tidak hilang dan ban depan tidak vulkanisir
                                                                        </label>
                                                                        <div class="demo-radio-button">
                                                                            <input type="radio" name="ban[]" id="banYa{{$key}}" value="1" class="filled-in chk-col-teal" />
                                                                            <label for="banYa{{$key}}">Ya</label>
                                                                            <input type="radio" name="ban[]" id="banPerbaikan{{$key}}" value="2" class="radio-col-red" />
                                                                            <label for="banPerbaikan{{$key}}">Perlu Perbaikan</label>
                                                                            <input type="radio" name="ban[]" id="banTidak{{$key}}" value="3" class="radio-col-red" />
                                                                            <label for="banTidak{{$key}}">Tidak Tersedia</label>
                                                                        </div>
                                                                        <label>4. ELEKTRIK : Lampu mundur, lampu rem, lampu dekat, lampu jauh, lampu hazzard,lampu sein, klakson, alarm mundur, wiper dan lampu rotary (Khusus trailer, bulk dan dump truk) </label>
                                                                        <div class="demo-radio-button">
                                                                            <input type="radio" name="elektrik[]" id="elektrikYa{{$key}}" value="1" class="filled-in chk-col-teal" />
                                                                            <label for="elektrikYa{{$key}}">Ya</label>
                                                                            <input type="radio" name="elektrik[]" id="elektrikPerbaikan{{$key}}" value="2" class="radio-col-red" />
                                                                            <label for="elektrikPerbaikan{{$key}}">Perlu Perbaikan</label>
                                                                            <input type="radio" name="elektrik[]" id="elektrikTidak{{$key}}" value="3" class="radio-col-red" />
                                                                            <label for="elektrikTidak{{$key}}">Tidak Tersedia</label>
                                                                        </div>
                                                                        <label>5. KABIN : Sepion lengkap, seat belt 3 titik, dan tidak ada benda yang dapat menganggu keamanan pengemudi </label>
                                                                        <div class="demo-radio-button">
                                                                            <input type="radio" name="kabin[]" id="kabinYa{{$key}}" value="1" class="filled-in chk-col-teal" />
                                                                            <label for="kabinYa{{$key}}">Ya</label>
                                                                            <input type="radio" name="kabin[]" id="kabinPerbaikan{{$key}}" value="2" class="radio-col-red" />
                                                                            <label for="kabinPerbaikan{{$key}}">Perlu Perbaikan</label>
                                                                            <input type="radio" name="kabin[]" id="kabinTidak{{$key}}" value="3" class="radio-col-red" />
                                                                            <label for="kabinTidak{{$key}}">Tidak Tersedia</label>
                                                                        </div>
                                                                        <label>6. KEBOCORAN : Udara, oli hidrolik, oli mesin, air radiator, oli, power streering </label>
                                                                        <div class="demo-radio-button">
                                                                            <input type="radio" name="kebocoran[]" id="kebocoranYa{{$key}}" value="1" class="filled-in chk-col-teal" />
                                                                            <label for="kebocoranYa{{$key}}">Ya</label>
                                                                            <input type="radio" name="kebocoran[]" id="kebocoranPerbaikan{{$key}}" value="2" class="radio-col-red" />
                                                                            <label for="kebocoranPerbaikan{{$key}}">Perlu Perbaikan</label>
                                                                            <input type="radio" name="kebocoran[]" id="kebocoranTidak{{$key}}" value="3" class="radio-col-red" />
                                                                            <label for="kebocoranTidak{{$key}}">Tidak Tersedia</label>
                                                                        </div>
                                                                        <label>7. KELENGKAPAN : APAR (3 KG),P3K, ganjal ban 2 buah, APD (helm,sepatu safety,rompi,masker,kacamata), segitiga pengaman/cone 2 buah, dongkrak, kunci roda, dokumen (STNK,SIM,KIR,ID), pelindung (samping dan belakang), reflektor lengkap, IVMS, Terpal (kecuali bulk) </label>
                                                                        <div class="demo-radio-button">
                                                                            <input type="radio" name="kelengkapan[]" id="kelengkapanYa{{$key}}" value="1" class="filled-in chk-col-teal" />
                                                                            <label for="kelengkapanYa{{$key}}">Ya</label>
                                                                            <input type="radio" name="kelengkapan[]" id="kelengkapanPerbaikan{{$key}}" value="2" class="radio-col-red" />
                                                                            <label for="kelengkapanPerbaikan{{$key}}">Perlu Perbaikan</label>
                                                                            <input type="radio" name="kelengkapan[]" id="kelengkapanTidak{{$key}}" value="3" class="radio-col-red" />
                                                                            <label for="kelengkapanTidak{{$key}}">Tidak Tersedia</label>
                                                                        </div>
                                                                        <label>8. Khusus Dump Truk : alarm dump,kunci bak truk,pengaman tuas PTO,rusuk (khusus muat clinker) </label>
                                                                        <div class="demo-radio-button">
                                                                            <input type="radio" name="khusuDumpTruk[]" id="khusuDumpTrukYa{{$key}}" value="1" class="filled-in chk-col-teal" />
                                                                            <label for="khusuDumpTrukYa{{$key}}">Ya</label>
                                                                            <input type="radio" name="khusuDumpTruk[]" id="khusuDumpTrukPerbaikan{{$key}}" value="2" class="radio-col-red" />
                                                                            <label for="khusuDumpTrukPerbaikan{{$key}}">Perlu Perbaikan</label>
                                                                            <input type="radio" name="khusuDumpTruk[]" id="khusuDumpTrukTidak{{$key}}" value="3" class="radio-col-red" />
                                                                            <label for="khusuDumpTrukTidak{{$key}}">Tidak Tersedia</label>
                                                                        </div>
                                                                        <label>9. Khusus Bulk Truk : Pressure gauge,whipcheck,hose connector,flexibel hose,full body harness </label>
                                                                        <div class="demo-radio-button">
                                                                            <input type="radio" name="khususBulkTruk[]" id="khususBulkTrukYa{{$key}}" value="1" class="filled-in chk-col-teal" />
                                                                            <label for="khususBulkTrukYa{{$key}}">Ya</label>
                                                                            <input type="radio" name="khususBulkTruk[]" id="khususBulkTrukPerbaikan{{$key}}" value="2" class="radio-col-red" />
                                                                            <label for="khususBulkTrukPerbaikan{{$key}}">Perlu Perbaikan</label>
                                                                            <input type="radio" name="khususBulkTruk[]" id="khususBulkTrukTidak{{$key}}" value="3" class="radio-col-red" />
                                                                            <label for="khususBulkTrukTidak{{$key}}">Tidak Tersedia</label>
                                                                        </div>                                                                        
                                                                        <label>10. Khusus Bulk Truk : Pemeriksaan bagian dalam tangki sebelum pemuatan,pastikan tengki dalam keadaan bersih dan tidak ada material lain selain sisa semen. </label>
                                                                        <div class="demo-radio-button">
                                                                            <input type="radio" name="khususBulkTruk2[]" id="khususBulkTruk2Ya{{$key}}" value="1" class="filled-in chk-col-teal" />
                                                                            <label for="khususBulkTruk2Ya{{$key}}">Ya</label>
                                                                            <input type="radio" name="khususBulkTruk2[]" id="hususBulkTruk2Perbaikan{{$key}}" value="2" class="radio-col-red" />
                                                                            <label for="hususBulkTruk2Perbaikan{{$key}}">Perlu Perbaikan</label>
                                                                            <input type="radio" name="khususBulkTruk2[]" id="hususBulkTruk2Tidak{{$key}}" value="3" class="radio-col-red" />
                                                                            <label for="hususBulkTruk2Tidak{{$key}}">Tidak Tersedia</label>
                                                                        </div>
                                                                        <label>Dengan ini saya menyatakan bahwa seluruh data dan informasi pada form isian di atas adalah BENAR dan apabila terdapat ketidak sesuaian informasi pada form isian tersebut maka segala akibat yang timbul sepenuhnya menjadi TANGGUNG JAWAB saya. </label>
                                                                        <div class="demo-checkbox">
                                                                            <input type="checkbox" name="setuju[]" id="setuju{{$key}}" value="1" class="filled-in chk-col-teal required" checked  />
                                                                            <label for="setuju{{$key}}">Setuju</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="kdVendor" name="kdVendor" readonly="readonly" value="{{ $dt['kode_vendor'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="doSilog" name="doSilog[]" readonly="readonly" value="{{ $dt['no_do'] }}-{{ $dt['line_do'] }}-{{ $dt['do_ty'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="tglRencanKirim" name="tglRencanKirim[]" readonly="readonly" value="{{ $dt['sddrqj'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="kdShipto" name="kdShipto[]" readonly="readonly" value="{{ $dt['kode_shipto'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="kdWilayah" name="kdWilayah[]" readonly="readonly" value="{{ $dt['kode_wilayah'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="kdItem1" name="kdItem1[]" readonly="readonly" value="{{ $dt['sditm'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="kdItem2" name="kdItem2[]" readonly="readonly" value="{{ $dt['kode_brg'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="kdItem3" name="kdItem3[]" readonly="readonly" value="{{ $dt['sdaitm'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="satuan" name="satuan[]" readonly="readonly" value="{{ $dt['sat'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="qty" name="qty[]" readonly="readonly" value="{{ $dt['qty'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="asal" name="asal[]" readonly="readonly" value="{{ $dt['kode_muatdi'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="motTransport" name="motTransport[]" readonly="readonly" value="{{ $dt['mot'] }}">
                                                        </div>
                                                        <div class="col-sm-12 col-lg-6">
                                                            <input type="hidden" class="form-control" id="branchplant" name="branchplant[]" readonly="readonly" value="{{ trim($dt['sdmcu']) }}">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach                                                   
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12"></div>
                            <div class="col-md-12">
                                <a href="{{ url('/prematchingtruk') }}/{{ session('user')->username }}" class="btn btn-info waves-effect waves-light">Kembali</a>
                                <button type="submit" id="simpan" class="btn btn-success waves-effect waves-light m-r-10">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('js/custom.min.js') }}"></script>
<script src="{{ asset('assets/plugins/wizard/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>  
<script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    //$('#tbMasterNextnumber').DataTable();
    $(document).ready(function() {
        $(".select2").select2();
        //$('#typeDokumen').change(function(){
        //    //alert('bismillah');
        //    var _doco = $('#noDokumen').val();
        //    var _dcto = $('#typeDokumen').val();
        //    $.ajax({
        //        url : "{{ url('getOP') }}/",
        //        method : "GET",
        //        data : {doco:_doco, dcto:_dcto},
        //        success : function(result){
        //            //alert(result);
        //            $('#dataOP').html(result);
        //            //location.reload();
        //        }
        //    })            
        //});
    });  
        
    function cekNominal(val,id) {
        var openAmount = $('#'+id).attr('data-id');
        if(parseInt(val) > parseInt(openAmount)){
            //alert(openAmount+ ' ' +val);
            alert('Mohon maaf, nilai pengajuan tidak boleh melebihi nilai Open !');
            $('#simpan').prop('disabled', true);
        }else{
            $('#simpan').prop('disabled', false);
        }
    }
</script>
@endpush
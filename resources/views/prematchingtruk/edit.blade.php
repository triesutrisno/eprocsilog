@extends('layouts.main')

@push('styles')    
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Ubah Data Permintaan Pembayaran</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/permintaanpembayaran') }}/{{ session('user')->username }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('/editpermintaanpembayaran') }}/{{ session('user')->username }}/{{ $model['pp'][0]['ppId'] }} " method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="comp">Company : <span class="danger">*</span></label>
                                    <select class="custom-select form-control" required id="comp" name="comp">
                                        <option value="">Silakan Pilih</option>
                                        <option value="10100" {{ $model['pp'][0]['comp']=='10100' ? 'selected' : '' }}>SILOG</option>
                                        <option value="10200" {{ $model['pp'][0]['comp']=='10200' ? 'selected' : '' }}>SID</option>
                                        <option value="10300" {{ $model['pp'][0]['comp']=='10300' ? 'selected' : '' }}>VUDS</option>
                                        <option value="10400" {{ $model['pp'][0]['comp']=='10400' ? 'selected' : '' }}>VULS</option>
                                        <option value="10500" {{ $model['pp'][0]['comp']=='10500' ? 'selected' : '' }}>VUBA</option>
                                    </select>
                                </div>
                            </div>                          
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="noInvoice">No Invoice : <span class="danger">*</span></label>
                                    <input type="text" name="noInvoice" id="noInvoice" class="form-control" required value="{{$model['pp'][0]['noInvoice']}}">
                                </div>
                            </div>                           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis">No Dokumen : <span class="danger">*</span></label>
                                    <input type="text" name="noDokumen" id="noDokumen" class="form-control" disabled="disabled" value="{{$model['pp'][0]['noDokumen']}}">
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis">Type Dokumen : <span class="danger">*</span></label>
                                    <!--<input type="text" name="typeDokumen" id="typeDokumen" class="form-control" required value="{{old('typeDokumen')}}">-->
                                    <select class="custom-select form-control" id="typeDokumen" name="typeDokumen" disabled="disabled">
                                        <option value="">Silakan Pilih</option>
                                        <option value="OP"  {{ $model['pp'][0]['typeDokumen']=='OP' ? 'selected' : '' }}>PEMBELIAN BARANG</option>
                                        <option value="OJ"  {{ $model['pp'][0]['typeDokumen']=='OJ' ? 'selected' : '' }}>PEMBELIAN JASA</option>
                                        <option value="O5"  {{ $model['pp'][0]['typeDokumen']=='O5' ? 'selected' : '' }}>PEMBELIAN IMPORT</option>
                                        <option value="OK"  {{ $model['pp'][0]['typeDokumen']=='OK' ? 'selected' : '' }}>PEMBELIAN KONSINYASI</option>
                                        <option value="O7"  {{ $model['pp'][0]['typeDokumen']=='O7' ? 'selected' : '' }}>PEMBELIAN MINISTORE</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!--<label for="ppFile">Lampiran  : <span class="danger"><small> * <code>Maximal file 2M and only zip file type.</code></small></span></label>
                                    <input type="file" class="dropify required" data-max-file-size="2M" data-allowed-file-extensions="zip" id="ppFile" name="ppFile"> -->
                                    <label for="ppFile">Lampiran : <span class="danger"></span> </label>
                                    <input type="file" class="dropify" data-max-file-size="2M" data-allowed-file-extensions="zip" id="ppFile" name="ppFile"> 
                                    {{ $model['pp'][0]['ppFile'] }}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keterangan">Keterangan : <span class="danger">*</span></label>
                                    <textarea name="keterangan" id="keterangan" rows="6" class="form-control" required>{{$model['pp'][0]['keterangan']}}</textarea>
                                </div>
                            </div>    
                        </div>                        
                        <div class="col-md-12">
                            <table class='display nowrap table table-hover table-striped table-bordered' cellspacing='0' width='100%'>
                                <thead>
                                    <tr>
                                        <th align='center'>No</th>
                                        <th>No Dokument</th>
                                        <th>Type</th>
                                        <th>Line</th>
                                        <th>Kode Item</th>
                                        <th>Nama Item</th>
                                        <th>Qty</th>
                                        <th>Curr</th>
                                        <th>Satuan</th>
                                        <th>Nilai Total</th>
                                        <th>Nilai Open</th>
                                        <th>Nilai Pengajuan</th>
                                    </tr>
                                </thead> 
                                @if($jml>0)<!-- jika ppDetail ada datanya -->
                                    @foreach($model['pp'][0]['ppdetail'] as $key => $val)
                                        @php
                                            $openAmount = isset($model2[$val['lineDokumen']]) ? $model2[$val['lineDokumen']] : 0;
                                        @endphp
                                        <tr>
                                            <td align='center'>{{$loop->iteration}}</td>
                                            <td>{{$val['noDokumen'] }}</td>
                                            <td>{{ $val['typeDokumen'] }}</td>
                                            <td>{{ $val['lineDokumen'] }}</td>
                                            <td>{{ $val['itemNumber'] }}</td>
                                            <td>{{ $val['itemName'] }}</td>
                                            <td>{{ $val['qty'] }}</td>
                                            <td>{{ $val['curency'] }}</td>
                                            <td>{{ $val['satuan'] }}</td>
                                            <td>{{ number_format($val['totalNilai'],2,',','.') }}</td>
                                            <td id="{{$key}}" data-id="{{ $openAmount }}">{{ number_format($openAmount,2,',','.') }}</td>
                                            <td><input type='text' id='{{$val['ppDetailId']}}' name='nilaiRequest[{{$val['ppDetailId']}}]' value="{{ $val['requestNilai'] }}" class='form-control' onchange='cekNominal(this.value,{{$key}})'></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan='12' align='center'>Data tidak ada !</td>
                                    </tr>
                                @endif
                            </table>
                        </div>                        
                        <!--<a href="{{ url('/permintaanpembayaran') }}/{{ session('user')->username }}" class="btn btn-info waves-effect waves-light">Kembali</a>-->
                        <button type="submit" id="simpan" class="btn btn-success waves-effect waves-light m-r-10">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('js/custom.min.js') }}"></script>
<script src="{{ asset('assets/plugins/wizard/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>  
<script>
    function cekNominal(val,id) {
        var openAmount = $('#'+id).attr('data-id');
        if(parseInt(val) > parseInt(openAmount)){
            alert('Mohon maaf, nilai pengajuan tidak boleh melebihi nilai Open !');
            $('#simpan').prop('disabled', true);
        }else{
            $('#simpan').prop('disabled', false);
        }
    }
</script>
@endpush
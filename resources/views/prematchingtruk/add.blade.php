@extends('layouts.main')

@push('styles')    
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" /> 
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Tambah Data Prematching Truk</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/prematchingtruk') }}/{{ session('user')->username }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('/prematchingtruk') }}/{{ session('user')->username }}/add" method="post">
                        @csrf
                        <div class="row">  
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="nopol">Nopol : </label>
                                    <select name="nopol" id="nopol" class="custom-select form-control select2 dinamic">
                                        <option value="">Silakan Pilih</option>
                                        @foreach($data['dataTruk'] as $val)
                                            <option value="{{ trim($val['nopin']) }}#{{ trim($val['nopol']) }}#{{ trim($val['kode_kuat']) }}#{{ trim($val['vmcvum']) }}#{{ trim($val['vmmcu']) }}#{{ trim($val['vmvown']) }}#{{ trim($val['vmvtyp']) }}">{{ $val['nopol'] }}</option>
                                        }
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="driver">Driver : </label>
                                    <select name="driver" id="driver" class="custom-select form-control select2 dinamic">
                                        <option value="">Silakan Pilih</option>
                                        @foreach($data['dataSopir'] as $dtSopir)
                                            <option value="{{ trim($dtSopir['kode_drv']) }}#{{ trim($dtSopir['nama_drv']) }}#{{ trim($dtSopir['abalky']) }}">{{ trim($dtSopir['abalky']) }} | {{ trim($dtSopir['nama_drv']) }}</option>
                                        }
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-lg-12">
                                    <div class="card card-inverse">
                                        <div class="card-body">
                                            <label>1. REM : Rem tangan, rem kaki, rem angin, drain tangki udara, indikator tekanan udara dan level oli rem </label>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="rem" id="remYa" value="1" class="filled-in chk-col-teal" />
                                                <label for="remYa">Ya</label>
                                                <input type="radio" name="rem" id="remPerbaikan" value="2" class="radio-col-red" />
                                                <label for="remPerbaikan">Perlu Perbaikan</label>
                                                <input type="radio" name="rem" id="remTidak" value="3" class="radio-col-red" />
                                                <label for="remTidak">Tidak Tersedia</label>
                                            </div>
                                            <label>2. KEMUDI : Level oli power steering, spelling steer </label>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="kemudi" id="kemudiYa" value="1" class="filled-in chk-col-teal" />
                                                <label for="kemudiYa">Ya</label>
                                                <input type="radio" name="kemudi" id="kemudiPerbaikan" value="2" class="radio-col-red" />
                                                <label for="kemudiPerbaikan">Perlu Perbaikan</label>
                                                <input type="radio" name="kemudi" id="kemudiTidak" value="3" class="radio-col-red" />
                                                <label for="kemudiTidak">Tidak Tersedia</label>
                                            </div>
                                            <label>3. BAN : Ban (terpasang dengan serep) memenuhi kedalaman minimum 1,5 mm, ban tidak sobek, baut lengkap, tekanan angian sesuai standart,
                                                velg tidak retak, kancing tidak hilang dan ban depan tidak vulkanisir
                                            </label>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="ban" id="banYa" value="1" class="filled-in chk-col-teal" />
                                                <label for="banYa">Ya</label>
                                                <input type="radio" name="ban" id="banPerbaikan" value="2" class="radio-col-red" />
                                                <label for="banPerbaikan">Perlu Perbaikan</label>
                                                <input type="radio" name="ban" id="banTidak" value="3" class="radio-col-red" />
                                                <label for="banTidak">Tidak Tersedia</label>
                                            </div>
                                            <label>4. ELEKTRIK : Lampu mundur, lampu rem, lampu dekat, lampu jauh, lampu hazzard,lampu sein, klakson, alarm mundur, wiper dan lampu rotary (Khusus trailer, bulk dan dump truk) </label>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="elektrik" id="elektrikYa" value="1" class="filled-in chk-col-teal" />
                                                <label for="elektrikYa">Ya</label>
                                                <input type="radio" name="elektrik" id="elektrikPerbaikan" value="2" class="radio-col-red" />
                                                <label for="elektrikPerbaikan">Perlu Perbaikan</label>
                                                <input type="radio" name="elektrik" id="elektrikTidak" value="3" class="radio-col-red" />
                                                <label for="elektrikTidak">Tidak Tersedia</label>
                                            </div>
                                            <label>5. KABIN : Sepion lengkap, seat belt 3 titik, dan tidak ada benda yang dapat menganggu keamanan pengemudi </label>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="kabin" id="kabinYa" value="1" class="filled-in chk-col-teal" />
                                                <label for="kabinYa">Ya</label>
                                                <input type="radio" name="kabin" id="kabinPerbaikan" value="2" class="radio-col-red" />
                                                <label for="kabinPerbaikan">Perlu Perbaikan</label>
                                                <input type="radio" name="kabin" id="kabinTidak" value="3" class="radio-col-red" />
                                                <label for="kabinTidak">Tidak Tersedia</label>
                                            </div>
                                            <label>6. KEBOCORAN : Udara, oli hidrolik, oli mesin, air radiator, oli, power streering </label>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="kebocoran" id="kebocoranYa" value="1" class="filled-in chk-col-teal" />
                                                <label for="kebocoranYa">Ya</label>
                                                <input type="radio" name="kebocoran" id="kebocoranPerbaikan" value="2" class="radio-col-red" />
                                                <label for="kebocoranPerbaikan">Perlu Perbaikan</label>
                                                <input type="radio" name="kebocoran" id="kebocoranTidak" value="3" class="radio-col-red" />
                                                <label for="kebocoranTidak">Tidak Tersedia</label>
                                            </div>
                                            <label>7. KELENGKAPAN : APAR (3 KG),P3K, ganjal ban 2 buah, APD (helm,sepatu safety,rompi,masker,kacamata), segitiga pengaman/cone 2 buah, dongkrak, kunci roda, dokumen (STNK,SIM,KIR,ID), pelindung (samping dan belakang), reflektor lengkap, IVMS, Terpal (kecuali bulk) </label>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="kelengkapan" id="kelengkapanYa" value="1" class="filled-in chk-col-teal" />
                                                <label for="kelengkapanYa">Ya</label>
                                                <input type="radio" name="kelengkapan" id="kelengkapanPerbaikan" value="2" class="radio-col-red" />
                                                <label for="kelengkapanPerbaikan">Perlu Perbaikan</label>
                                                <input type="radio" name="kelengkapan" id="kelengkapanTidak" value="3" class="radio-col-red" />
                                                <label for="kelengkapanTidak">Tidak Tersedia</label>
                                            </div>
                                            <label>8. Khusus Dump Truk : alarm dump,kunci bak truk,pengaman tuas PTO,rusuk (khusus muat clinker) </label>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="khusuDumpTruk" id="khusuDumpTrukYa" value="1" class="filled-in chk-col-teal" />
                                                <label for="khusuDumpTrukYa">Ya</label>
                                                <input type="radio" name="khusuDumpTruk" id="khusuDumpTrukPerbaikan" value="2" class="radio-col-red" />
                                                <label for="khusuDumpTrukPerbaikan">Perlu Perbaikan</label>
                                                <input type="radio" name="khusuDumpTruk" id="khusuDumpTrukTidak" value="3" class="radio-col-red" />
                                                <label for="khusuDumpTrukTidak">Tidak Tersedia</label>
                                            </div>
                                            <label>9. Khusus Bulk Truk : Pressure gauge,whipcheck,hose connector,flexibel hose,full body harness </label>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="khususBulkTruk" id="khususBulkTrukYa" value="1" class="filled-in chk-col-teal" />
                                                <label for="khususBulkTrukYa">Ya</label>
                                                <input type="radio" name="khususBulkTruk" id="khususBulkTrukPerbaikan" value="2" class="radio-col-red" />
                                                <label for="khususBulkTrukPerbaikan">Perlu Perbaikan</label>
                                                <input type="radio" name="khususBulkTruk" id="khususBulkTrukTidak" value="3" class="radio-col-red" />
                                                <label for="khususBulkTrukTidak">Tidak Tersedia</label>
                                            </div>                                                                        
                                            <label>10. Khusus Bulk Truk : Pemeriksaan bagian dalam tangki sebelum pemuatan,pastikan tengki dalam keadaan bersih dan tidak ada material lain selain sisa semen. </label>
                                            <div class="demo-radio-button">
                                                <input type="radio" name="khususBulkTruk2" id="khususBulkTruk2Ya" value="1" class="filled-in chk-col-teal" />
                                                <label for="khususBulkTruk2Ya">Ya</label>
                                                <input type="radio" name="khususBulkTruk2" id="hususBulkTruk2Perbaikan" value="2" class="radio-col-red" />
                                                <label for="hususBulkTruk2Perbaikan">Perlu Perbaikan</label>
                                                <input type="radio" name="khususBulkTruk2" id="hususBulkTruk2Tidak" value="3" class="radio-col-red" />
                                                <label for="hususBulkTruk2Tidak">Tidak Tersedia</label>
                                            </div>
                                            <label>Dengan ini saya menyatakan bahwa seluruh data dan informasi pada form isian di atas adalah BENAR dan apabila terdapat ketidak sesuaian informasi pada form isian tersebut maka segala akibat yang timbul sepenuhnya menjadi TANGGUNG JAWAB saya. </label>
                                            <div class="Check this custom checkbox">
                                                <input type="checkbox" name="setuju" id="setuju" value="1" class="filled-in chk-col-teal" required />
                                                <label for="setuju">Setuju *</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="kdVendor" name="kdVendor" readonly="readonly" value="{{ $data['dataDO'][0]['kode_vendor'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="doSilog" name="doSilog" readonly="readonly" value="{{ $data['dataDO'][0]['no_do'] }}-{{ $data['dataDO'][0]['line_do'] }}-{{ $data['dataDO'][0]['do_ty'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="tglRencanKirim" name="tglRencanKirim" readonly="readonly" value="{{ $data['dataDO'][0]['sddrqj'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="kdShipto" name="kdShipto" readonly="readonly" value="{{ $data['dataDO'][0]['kode_shipto'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="kdWilayah" name="kdWilayah" readonly="readonly" value="{{ $data['dataDO'][0]['kode_wilayah'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="kdItem1" name="kdItem1" readonly="readonly" value="{{ $data['dataDO'][0]['sditm'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="kdItem2" name="kdItem2" readonly="readonly" value="{{ $data['dataDO'][0]['kode_brg'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="kdItem3" name="kdItem3" readonly="readonly" value="{{ $data['dataDO'][0]['sdaitm'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="satuan" name="satuan" readonly="readonly" value="{{ $data['dataDO'][0]['sat'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="qty" name="qty" readonly="readonly" value="{{ $data['dataDO'][0]['qty'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="asal" name="asal" readonly="readonly" value="{{ $data['dataDO'][0]['kode_muatdi'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="motTransport" name="motTransport" readonly="readonly" value="{{ $data['dataDO'][0]['mot'] }}">
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <input type="hidden" class="form-control" id="branchplant" name="branchplant" readonly="readonly" value="{{ trim($data['dataDO'][0]['sdmcu']) }}">
                            </div>                            
                            <div class="col-md-12"></div>
                            <div class="col-md-12">
                                <a href="{{ url('/prematchingtruk') }}/{{ session('user')->username }}" class="btn btn-info waves-effect waves-light">Kembali</a>
                                <button type="submit" id="simpan" class="btn btn-success waves-effect waves-light m-r-10">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('js/custom.min.js') }}"></script>
<script src="{{ asset('assets/plugins/wizard/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>  
<script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    //$('#tbMasterNextnumber').DataTable();
    $(document).ready(function() {
        $(".select2").select2();
        //$('#typeDokumen').change(function(){
        //    //alert('bismillah');
        //    var _doco = $('#noDokumen').val();
        //    var _dcto = $('#typeDokumen').val();
        //    $.ajax({
        //        url : "{{ url('getOP') }}/",
        //        method : "GET",
        //        data : {doco:_doco, dcto:_dcto},
        //        success : function(result){
        //            //alert(result);
        //            $('#dataOP').html(result);
        //            //location.reload();
        //        }
        //    })            
        //});
    });  
        
    function cekNominal(val,id) {
        var openAmount = $('#'+id).attr('data-id');
        if(parseInt(val) > parseInt(openAmount)){
            //alert(openAmount+ ' ' +val);
            alert('Mohon maaf, nilai pengajuan tidak boleh melebihi nilai Open !');
            $('#simpan').prop('disabled', true);
        }else{
            $('#simpan').prop('disabled', false);
        }
    }
</script>
@endpush
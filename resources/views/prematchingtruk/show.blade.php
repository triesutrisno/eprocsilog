@extends('layouts.main')

@push('styles')
    
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/switchery/dist/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />    
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('container')

@php
$nowDate = date("Y-m-d H:i:s");
#echo "Tanggal Hari ini = ".$nowDate;
@endphp

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Data Detail Permintaan Pembayaran</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/permintaanpembayaran') }}" class="btn btn-block btn-info">Lihat Data</a>                        
                    </div>
                </div>   
                <div class="card-body">
                    @if(session('kode')=="90")
                        <div class="alert alert-danger">
                            <i class="fa fa-exclamation-circle"></i>
                            {{ session('pesan') }}
                        </div>
                    @elseif(session('kode')=="99")
                        <div class="alert alert-success">
                            <i class="fa fa-exclamation-circle"></i>
                            {{ session('pesan') }}
                        </div>
                    @endif
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="kegiatanPengadaan" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <table class="table color-bordered-table default-bordered-table">
                                        <tr>
                                            <td width="20%">Company</td>
                                            <td width="5%" align="center">:</td>
                                            <td>
                                                @if($model['pp'][0]['comp']=='10100')
                                                    PT SEMEN INDONESIA LOGISTIK
                                                @elseif($model['tender'][0]['comp']=='10200')
                                                    PT SEMEN INDONESIA DISTRIBUTOR
                                                @elseif($model['tender'][0]['comp']=='10300')
                                                    PT VARIA USAHA DHARMA SEGARA
                                                @elseif($model['tender'][0]['comp']=='10400')
                                                    PT VARIA USAHA LINTAS SEGARA
                                                @elseif($model['tender'][0]['comp']=='10500')
                                                    PT VARIA USAHA BAHARI
                                                @else
                                                    
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>No Dokumen</td>
                                            <td align="center">:</td>
                                            <td>{{ $model['pp'][0]['noDokumen'] }} @if($model['pp'][0]['typeDokumen']!="") ( {{ $model['pp'][0]['typeDokumen'] }} )@endif</td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Invoice</td>
                                            <td align="center">:</td>
                                            <td>{{ $model['pp'][0]['noInvoice'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Keterangan</td>
                                            <td align="center">:</td>
                                            <td>{{ $model['pp'][0]['keterangan'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td align="center">:</td>
                                            <td>                                                
                                                @if($model['pp'][0]['status']=='1')
                                                    Baru
                                                @elseif($model['pp'][0]['status']=='2')
                                                    Lolos Verifikasi
                                                @elseif($model['pp'][0]['status']=='3')
                                                    Tidak Lolos Verifikasi
                                                @elseif($model['pp'][0]['status']=='4')
                                                    No PP Sudah dibuat
                                                @elseif($model['pp'][0]['status']=='5')
                                                    No Ekspedisi Sudah dibuat
                                                @elseif($model['pp'][0]['status']=='6')
                                                    Proses Akuntansi
                                                @elseif($model['pp'][0]['status']=='7')
                                                    Proses Bendahara
                                                @elseif($model['pp'][0]['status']=='8')
                                                    Sudah Terbayar                                                
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Lampiran</td>
                                            <td align="center">:</td>
                                            <td>
                                                @if($model['pp'][0]['ppFile']!="")
                                                    <a href="{{ asset('../storage/'.$model['pp'][0]['ppFile']) }}">Lampiran</a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Remark</td>
                                            <td align="center">:</td>
                                            <td>{{ $model['pp'][0]['remark'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Verifikasi</td>
                                            <td align="center">:</td>
                                            <td>{{ date('d-m-Y H:i:s', strtotime($model['pp'][0]['tglVerifikasi'])) }}</td>
                                        </tr>
                                        <tr>
                                            <td>No PP</td>
                                            <td align="center">:</td>
                                            <td>{{ $model['pp'][0]['noPP'] }} @if($model['pp'][0]['typeDokPP']!="") ( {{ $model['pp'][0]['typeDokPP'] }} ) @endif</td>
                                        </tr>
                                        <tr>
                                            <td>No EPP</td>
                                            <td align="center">:</td>
                                            <td>{{ $model['pp'][0]['noEPP'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>No Bukti Pembayaran</td>
                                            <td align="center">:</td>
                                            <td>{{ $model['pp'][0]['noBuktiPembayaran'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Created at</td>
                                            <td align="center">:</td>
                                            <td>{{ date('d-m-Y H:i:s', strtotime($model['pp'][0]['created_at'])) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Update at</td>
                                            <td align="center">:</td>
                                            <td>{{ date('d-m-Y H:i:s', strtotime($model['pp'][0]['updated_at'])) }}</td>
                                        </tr>                                         
                                        @if($model['pp'][0]['status']=="1")
                                        <tr>
                                            <td  colspan='3'>
                                                @csrf
                                                <div class="row button-group">
                                                    <div class="col-lg-3 col-md-4">
                                                        <a href="javascript:void(0);" data-id="{{ $model['pp'][0]['ppId'] }}" class="btn btn-block btn-success komplit">Dokumen Lengkap</a>                        
                                                    </div>
                                                    <div class="col-lg-3 col-md-4">
                                                        <a href="{{ url('/permintaanpembayaran/nokomplit') }}/{{ $model['pp'][0]['ppId'] }}" class="btn btn-block btn-danger notKomplit">Dokumen Kurang Lengkap</a>                        
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>                                        
                                        @endif
                                        <tr>
                                            <td  colspan='3'></td>
                                        </tr>
                                    </table>
                                    <table class='display nowrap table table-hover table-striped table-bordered' cellspacing='0' width='100%'>
                                        <thead>
                                            <tr>
                                                <th align='center'>No</th>
                                                <th>No Dokument</th>
                                                <th>Type</th>
                                                <th>Line</th>
                                                <th>Kode Item</th>
                                                <th>Nama Item</th>
                                                <th>Qty</th>
                                                <th>Curr</th>
                                                <th>Satuan</th>
                                                <th>Nilai Total</th>
                                                <th>Nilai Pengajuan</th>
                                            </tr>
                                        </thead>  
                                        @if($jml>0)<!-- jika ppDetail ada datanya -->
                                            @php $totNilaiRequest = 0; @endphp
                                            @foreach($model['pp'][0]['ppdetail'] as $key => $val)
                                                @php $totNilaiRequest += $val['requestNilai']; @endphp
                                                <tr>
                                                    <td align='center'>{{$loop->iteration}}</td>
                                                    <td>{{$val['noDokumen'] }}</td>
                                                    <td>{{ $val['typeDokumen'] }}</td>
                                                    <td>{{ $val['lineDokumen'] }}</td>
                                                    <td>{{ $val['itemNumber'] }}</td>
                                                    <td>{{ $val['itemName'] }}</td>
                                                    <td>{{ $val['qty'] }}</td>
                                                    <td>{{ $val['curency'] }}</td>
                                                    <td>{{ $val['satuan'] }}</td>
                                                    <td>{{ number_format($val['totalNilai'],2,',','.') }}</td>
                                                    <td>{{ number_format($val['requestNilai'],2,',','.') }}</td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan='10' align='center'>Total Request : </td>
                                                <td>{{ number_format($totNilaiRequest,2,',','.') }}</td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td colspan='11' align='center'>Data tidak ada !</td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-tagsinput/src/bootstrap-tagsinput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/js/dataTablesPlugins/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $('#tbKegiatanPengadaanDetail').DataTable();
        //$('#tbKegiatanPengadaanMengikuti').DataTable();
        $('#tbKegiatanPengadaanMengikuti').DataTable({
        dom: 'Bfrtip',
        buttons: [
            //'excel', 'pdf', 'print'
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel'
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF'
            },
            {
                extend:    'print',
                text:      '<i class="fa fa-print"></i>',
                titleAttr: 'Print'
            }
        ]
    });
        jQuery(document).ready(function() {        
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

            // For multiselect
            $('#pre-selected-options').multiSelect();
            $('#optgroup').multiSelect({
                selectableOptgroup: true
            });
            $('#public-methods').multiSelect();
            
            $('.komplit').click(function(){
                var jawab = confirm("Anda yakin akan memproses data ini ?");
                if (jawab === true) {
    //            kita set hapus false untuk mencegah duplicate request
                    var hapus = false;
                    if (!hapus) {
                        hapus = true;
                        //$.post('hapus.php', {id: $(this).attr('data-id')},
                        var idne = $(this).attr('data-id');
                        var _token = $('input[name="_token"]').val();
                        //alert(idne);
                        $.ajax({
                            url : "{{ url('/permintaanpembayaran/komplit') }}/"+idne,
                            method : "POST",
                            data : {_token:_token},
                            success : function(result){
                                //alert(result['kode']);
                                //$('#'+dependent).html(result);
                                //$.session.set("kode", result['kode']);
                                //location.reload();
                                //window.location.href = window.location.href + "/kode="+result['kode'];
                                var kode = result['kode'];
                                var pesan = result['pesan'];
                                //var param = "'id'=>"+idne+",'kode'=>'99','pesan'=>'Bismillah'";
                                //var url = "('flash', ['id'=>"+idne+",'kode'=>"+kode+",'pesan'=>"+pesan+"])";
                                //alert(url);
                                window.location.href = "{{ URL::to('permintaanpembayaran/flash') }}/"+idne+"/"+kode+"/"+pesan;
                            }
                        })
                        hapus = false;
                    }
                } else {
                    return false;
                }

            });
        });        
    </script>
@endpush
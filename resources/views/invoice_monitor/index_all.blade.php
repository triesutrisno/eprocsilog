@extends('layouts.main')

@push('styles')
    <link href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
@endpush

@section('container')
    <div id="module-pages" class="container-fluid">
        <div class="row">
            <!-- column -->
            <div class="col-lg-12">
                <div class="card">
                    <h2 class="card-title col-lg-12"><strong>Invoice Monitor</strong></h2>
                    <div class="card-body">
                        @if (session()->get('user')->role == 'admin')
                            <div class="row">
                                <div class="col-lg-3 col-md-3">
                                    <a href="{{ route('invoice-monitor.upload_form') }}"
                                        class="waves-effect waves-light btn btn-primary">Upload Data</a>
                                    <h6 class="m-t-0">&nbsp;</h6>

                                </div>
                            </div>
                        @endif
                        <div class="col-lg-12 col-md-12 alert alert-warning">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label class="text-small">No Kwitansi</label>
                                    <input type="text" name="no_kwitansi" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-lg-3">
                                    <label class="text-small">Nama Perusahaan</label>
                                    <input type="text" name="nama_perusahaan" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-md-3">
                                    <label class="text-small">Tgl Kwitansi : </label>
                                    <input type="text" name="tgl_kwitansi" class="form-control" autocomplete="off">
                                </div>
                                <div class="col-lg-2 col-md-2">
                                    <h4 class="m-t-0">&nbsp;</h4>
                                    <button id="btn_search" type="submit"
                                        class="btn waves-effect waves-light btn-success">Cari</button>
                                </div>
                            </div>


                            <div class="row">
                                <h6 class="m-t-0">&nbsp;</h6>
                                <h6 class="m-t-0">&nbsp;</h6>
                            </div>
                        </div>
                    </div>

                    @include('partials.alert')

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <div id="example23_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                        <table id='berita_acara_table'
                                            class="display table table-hover table-striped table-bordered dataTable"
                                            cellspacing="0" width="100%" role="grid">
                                            <thead>
                                                <tr>
                                                    <td>ID</td>
                                                    <td>Kode SAP Vendor</td>
                                                    <td>Nama Perusahaan</td>
                                                    <td>Nama Pelanggan</td>
                                                    <td>No Kwitansi</td>
                                                    <td>Nominal</td>
                                                    <td>Tgl Kwitansi</td>
                                                    <td>Posisi Tagihan</td>
                                                    <td>Tgl Rencana Pembayaran</td>
                                                    <td>Tgl Data Input</td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div><!-- end row -->
                    </div>
                </div>
            </div>
        </div><!-- end row -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@push('scripts')
    <!-- This is data table -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>


    <script>
        $(document).ready(function() {

            var tgl_kwitansi = $('input[name="tgl_kwitansi"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            tgl_kwitansi.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format(
                    'MM/DD/YYYY'));
            });

            tgl_kwitansi.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            var table = $('#berita_acara_table').DataTable({
                dom: '<"row m-0" <"col-sm-3 p-0"l><"col-sm-3 p-t-10"r><"col-sm-6 p-0"<"float-right"p>>>t<"row m-0" <"col-sm-3 p-0"i> <"col-sm-3 p-0"B><"col-sm-6 p-0"p> >',
                "lengthChange": true,
                "lengthMenu": [
                    [10, 25, 50, 100, 500],
                    [10, 25, 50, 100, 500]
                ],
                order: [
                    [0, 'desc']
                ],
                "processing": true,
                "serverSide": true,
                "orderCellsTop": true,
                "responsive": true,
                "scrollX": true,
                "language": {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                },
                "ajax": {
                    "url": "{{ route('invoice_monitor.getListAll') }}",
                    "data": function(d) {
                        d.no_kwitansi = $('input[name="no_kwitansi"]').val(),
                            d.nama_perusahaan = $('input[name="nama_perusahaan"]').val(),
                            d.tgl_kwitansi = $('input[name="tgl_kwitansi"]').val()
                    }
                },
                "buttons": [{
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel"> Export Excel </i>',
                        titleAttr: 'Excel',
                        title: 'Data Invoice Monitor',
                    }

                ],

                "columnDefs": [{
                        "targets": [0],
                        "visible": false
                    },
                    {
                        "targets": [1],
                        "visible": false
                    },
                ]

            });

            $("#btn_search").click(function() {
                table.ajax.reload();

            });
        });
    </script>
@endpush

@extends('layouts.main')

@push('styles')
    {{-- <link href="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet"> --}}
@endpush

@section('container')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        @include('partials.alert')

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="col-lg-12">
                        <h4 class="card-title">
                            <strong>Upload Invoice Monitor</strong>
                        </h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('/invoice-monitor-upload') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-5" <div class="form-group">
                                    <div class="form-group">
                                        <label>Upload Data</label>
                                        <input type="file" class="form-control" id="uploaded_file" name="uploaded_file"
                                            aria-describedby="fileHelp">
                                    </div>
                                </div>
                                <div class="col-md-7 my-auto">
                                    <div class="form-group my-auto">
                                        <a href="{{ url('assets/xls/UPLOAD INVOICE MONITORING.xlsx') }}">Link Template Upload
                                            Excel</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group text-center m-t-20">
                                        <button class="btn btn-block btn-success" type="submit">Simpan</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@push('scripts')
    <script src="{{ asset('js/custom.min.js') }}"></script>
    {{-- <script src="{{ asset('js/validation.js') }}"></script>
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script> --}}

    <script>
        // var periode_spj = $('input[name="periode_spj"]').daterangepicker({
        //     autoUpdateInput: false,
        //     locale: {
        //         cancelLabel: 'Clear'
        //     }
        // });

        // periode_spj.on('apply.daterangepicker', function(ev, picker) {
        //     $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format(
        //         'MM/DD/YYYY'));
        // });

        // periode_spj.on('cancel.daterangepicker', function(ev, picker) {
        //     $(this).val('');
        // });
    </script>
@endpush

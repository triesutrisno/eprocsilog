@extends('layouts.main')

@push('styles')
    
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/switchery/dist/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />    
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('container')

@php
$nowDate = date("Y-m-d H:i:s");
#echo "Tanggal Hari ini = ".$nowDate;
@endphp

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Data Detail Kegiatan Pengadaan</strong></h4>
                <div class="row button-group">
                    <div class="col-xs-2">
                        <a href="{{ url('/tender/detail') }}/{{ $model['tender'][0]['tenderId'] }}" class="btn btn-info">Lihat Data</a> 
                    </div>
                </div>   
                <div class="card-body">
                    @if($pesan!="")
                        <br />
                        @if($kode=="99")
                            <div class="alert alert-success">
                                <i class="fa fa-exclamation-circle"></i>
                                {{ $pesan }}
                            </div>
                        @elseif($kode=="90")
                            <div class="alert alert-danger">
                                <i class="fa fa-exclamation-circle"></i>
                                {{ $pesan }}
                            </div>
                        @endif
                    @endif
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="kegiatanPengadaan" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <table class="table color-bordered-table default-bordered-table">
                                        <tr>
                                            <td width="20%">Company</td>
                                            <td width="5%" align="center">:</td>
                                            <td colspan='2'>
                                                @if($model['tender'][0]['comp']=='10100')
                                                    PT SEMEN INDONESIA LOGISTIK
                                                @elseif($model['tender'][0]['comp']=='10200')
                                                    PT SEMEN INDONESIA DISTRIBUTOR
                                                @elseif($model['tender'][0]['comp']=='10300')
                                                    PT VARIA USAHA DHARMA SEGARA
                                                @elseif($model['tender'][0]['comp']=='10400')
                                                    PT VARIA USAHA LINTAS SEGARA
                                                @elseif($model['tender'][0]['comp']=='10500')
                                                    PT VARIA USAHA BAHARI
                                                @else
                                                    
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tahun</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['tenderTahun'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nomor</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['tenderNomor'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Kegiatan</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['tenderNama'] }}</td>
                                        </tr>  
                                        <tr>
                                            <td>Jenis</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>
                                                @if($model['tender'][0]['tenderJenis']=='1')
                                                    Pengadaan Barang Stok
                                                @elseif($model['tender'][0]['tenderJenis']=='2')
                                                    Pengadaan Barang Non Stok
                                                @elseif($model['tender'][0]['tenderJenis']=='3')
                                                    Pengadaan Jasa
                                                @else
                                                    ''
                                                @endif
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>Type</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>
                                                @if($model['tender'][0]['tenderType']=='1')
                                                    Tender
                                                @elseif($model['tender'][0]['tenderType']=='2')
                                                    Non Tender
                                                @elseif($model['tender'][0]['tenderType']=='3')
                                                    Repead Order
                                                @else
                                                    
                                                @endif
                                            </td>
                                        </tr>  
                                        <tr>
                                            <td>Kategori</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['kategori']['0']['mkatNama'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Close</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tenderDetail'][0]['tender'][0]['tglTutup'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Spesifikasi Penawaran</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tenderDetail'][0]['penawaranSpesifikasi'] }}</td>
                                        </tr> 
                                        <tr>
                                            <td>Currency Penawaran</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tenderDetail'][0]['penawaranCurency'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Harga Penawaran</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ number_format($model['tenderDetail'][0]['penawaranHarga'],2,',','.') }}</td>
                                        </tr>                                        
                                        <tr>
                                            <td>Tanggal Expired Penawaran</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tenderDetail'][0]['penawaranExpired'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Harga Nego</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>
                                                @if(date("Y-m-d H:i:s")>$model['tender'][0]['tglNegoAkhir'])
                                                    {{ number_format($model['tenderDetail'][0]['penawaranNego'],2,',','.') }}
                                                @else
                                                
                                                @endif
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td>Tanggal Nego</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tenderDetail'][0]['tglNego'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lampiran 1</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>
                                                @if($model['tenderDetail'][0]['penawaranFile']!="")
                                                    <a href="{{ asset('../storage/'.$model['tenderDetail'][0]['penawaranFile']) }}" target="_blank()">Lampiran</a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Lampiran 2</td>
                                            <td align="center">:</td>
                                            <td  colspan='2'>
                                                @if($model['tenderDetail'][0]['penawaranGambar']!="")
                                                    <a href="{{ asset('../storage/'.$model['tenderDetail'][0]['penawaranGambar']) }}" target="_blank()">Lampiran</a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td align="center">:</td>
                                            <td>
                                                @if($model['tenderDetail'][0]['penawaranStatus']=='1')
                                                    Baru
                                                @elseif($model['tenderDetail'][0]['penawaranStatus']=='2')
                                                    Mengikuti
                                                @elseif($model['tenderDetail'][0]['penawaranStatus']=='3')
                                                    Nego 
                                                @elseif($model['tenderDetail'][0]['penawaranStatus']=='4')
                                                    Menang 
                                                @elseif($model['tenderDetail'][0]['penawaranStatus']=='5')
                                                    Kalah 
                                                @elseif($model['tenderDetail'][0]['penawaranStatus']=='6')
                                                    Tidak Mengikuti 
                                                @else
                                                    Selesai
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Reference 1</td>
                                            <td align="center">:</td>
                                            <td  colspan='2'>{{ $model['tender'][0]['tenderReff1'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Reference 2</td>
                                            <td align="center">:</td>
                                            <td  colspan='2'>{{ $model['tender'][0]['tenderReff2'] }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-tagsinput/src/bootstrap-tagsinput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/js/dataTablesPlugins/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $('#tbKegiatanPengadaanDetail').DataTable();
        //$('#tbKegiatanPengadaanMengikuti').DataTable();
        $('#tbKegiatanPengadaanMengikuti').DataTable({
        dom: 'Bfrtip',
        buttons: [
            //'excel', 'pdf', 'print'
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel'
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF'
            },
            {
                extend:    'print',
                text:      '<i class="fa fa-print"></i>',
                titleAttr: 'Print'
            }
        ]
    });
        jQuery(document).ready(function() {        
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

            // For multiselect
            $('#pre-selected-options').multiSelect();
            $('#optgroup').multiSelect({
                selectableOptgroup: true
            });
            $('#public-methods').multiSelect();
        });
    </script>
@endpush
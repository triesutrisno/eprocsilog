@extends('layouts.main')

@push('styles')
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Input Data Pemenang</strong></h4>
                <div class="card-body">
                    @if($jml<='0')
                        <div class="col-lg-6 col-md-12">
                            <div class="alert alert-danger ">                                            
                                <h3 class="text-danger">
                                    <i class="fa fa-exclamation-triangle"></i> 
                                    Maaf !
                                </h3> 
                                Untuk saat ini belum ada supplier yang mengajukan negosiasi harga.
                            </div>
                        </div>
                    @else
                        <form action="{{ url('/tender/pemenangpost') }}/{{ $model['tenderDetail'][0]['tender'][0]['tenderId'] }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-5">  
                                    <div class="col-md-12">                                
                                        <div class="form-group">
                                            <label for="tenderSupplier">Supplier : <span class="danger">*</span></label>
                                            <select name="tenderSupplier" id="tenderSupplier" class="custom-select form-control select2" required>
                                                <option value="">Silakan Pilih</option>
                                                @foreach($model['tenderDetail'] as $val2)
                                                    <option value="{{ $val2['tenderPenawaranId'] }}" {{ $val2['supplier'][0]['supId']==$id2 ? 'selected' : '' }}>{{ $val2['supplier'][0]['supNama'] }}</option>
                                                }
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="tenderRemark1">Remark 1 : <span class="danger">*</span></label>
                                            <input type="text" class="form-control" required id="tenderRemark1" name="tenderRemark1"> 
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="tenderRemark2">Remark 2 : </label>
                                            <div class="input-group">
                                                <input type="text" class="form-control"  id="tenderRemark2" autocomplete="off" name="tenderRemark2"> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center m-t-20">
                                <div  class="col-lg-2 col-md-2">
                                    <button class="btn btn-block btn-success" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('js/validation.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>    
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#penawaranExpired').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                //todayHighlight: true,
                //format: "mm-yyyy",
                //viewMode: "months", 
                //minViewMode: "months"
            });
            $('#input-group-append-id').click(function() {
                //alert("Bismillah !");
                $('#penawaranExpired').datepicker('show');
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
        
    </script>
@endpush
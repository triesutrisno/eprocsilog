@extends('layouts.main')

@push('styles')
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Data Kegiatan Pengadaan</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/tender') }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/tender/add') }}" class="btn btn-block btn-success">Tambah Data</a>
                    </div>
                </div>
                
                @if($kode2=="99")
                    <div class="alert alert-success">
                        <i class="fa fa-exclamation-circle"></i>
                        {{ $pesan2 }}
                    </div>
                @elseif($kode2=="90")
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-circle"></i>
                        {{ $pesan2 }}
                    </div>
                @endif
                 
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="tbKegiatanPengadaan" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th align="center">No</th>
                                    <th>Aksi</th>
                                    <th>Status</th>
                                    <th>Comp</th>
                                    <th>Tahun</th>
                                    <th>Kegitan Pengadaan</th>
                                    <th>Jenis</th>
                                    <th>Currency</th>                                    
                                    <th>Budget</th>
                                    <th>Kategori</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th align="center">No</th>
                                    <th>Aksi</th>
                                    <th>Status</th>
                                    <th>Comp</th>
                                    <th>Tahun</th>
                                    <th>Kegitan Pengadaan</th>
                                    <th>Jenis</th>
                                    <th>Currency</th>                                    
                                    <th>Budget</th>
                                    <th>Kategori</th>
                                    <th>Type</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($model as $dt)                                    
                                    <tr>
                                        <td align="center">{{$loop->iteration}}</td>
                                        @csrf
                                        <td align="center">
                                            <div class="hidden-sm hidden-xs action-buttons">                                                                                                
                                                <a class="green" href="{{ url('/tender/detail')}}/{{ $dt['tenderId'] }}" title="Detail"><i class="ace-icon fa fa-search bigger-130"></i></a>
                                                @if($dt['tenderStatus']=='1')
                                                <a class="green" href="{{ url('/tender/edit')}}/{{ $dt['tenderId'] }}" title="Ubah"><i class="ace-icon fa fa-edit bigger-130"></i></a>
                                                <a class="green hapusTender" href="#" data-id="{{ $dt['tenderId'] }}" title="Hapus"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            @if($dt['tenderStatus']=='1')
                                                Baru
                                            @elseif($dt['tenderStatus']=='2')
                                                Undangan Terkirim
                                            @elseif($dt['tenderStatus']=='3')
                                                Nego Harga
                                            @else
                                                Selesai
                                            @endif
                                        </td>
                                        <td>{{ $dt['comp'] }}</td>
                                        <td>{{ $dt['tenderTahun'] }}</td>
                                        <td>{{ $dt['tenderNama'] }}</td>
                                        <td>
                                            @if($dt['tenderJenis']=='1')
                                                Pengadaan Barang Stok
                                            @elseif($dt['tenderJenis']=='2')
                                                pengadaan Barang Non Stok
                                            @else
                                                Pengadaan Jasa
                                            @endif
                                        </td>
                                        <td>{{ $dt['tenderCurency'] }}</td>
                                        <td>{{ number_format($dt['tenderBudget'],'2',',','.') }}</td>
                                        <td>{{ $model[0]['kategori']['0']['mkatNama'] }}</td>
                                        <td>
                                            @if($dt['tenderType']=='1')
                                                Tender
                                            @elseif($dt['tenderType']=='2')
                                                No Tender
                                            @else
                                                Repead Order
                                            @endif
                                        </td>
                                    </tr>  
                                @endforeach                                                   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<!-- This is data table -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script>
    $('#tbKegiatanPengadaan').DataTable();
    $(document).ready(function() {
        $('.hapusTender').click(function(){
            var jawab = confirm("Anda yakin akan menghapus data ini ?");
            if (jawab === true) {
//            kita set hapus false untuk mencegah duplicate request
                var hapus = false;
                if (!hapus) {
                    hapus = true;
                    //$.post('hapus.php', {id: $(this).attr('data-id')},
                    var idne = $(this).attr('data-id');
                    var _token = $('input[name="_token"]').val();
                    //alert(idne);
                    $.ajax({
                        url : "{{ url('/tender/delete') }}/"+idne,
                        method : "POST",
                        data : {_token:_token},
                        success : function(result){
                            //alert(result);
                            //$('#'+dependent).html(result);
                            location.reload();
                        }
                    })
                    hapus = false;
                }
            } else {
                return false;
            }
            
        });
    });    
</script>
@endpush
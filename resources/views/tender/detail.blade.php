@extends('layouts.main')

@push('styles')
    
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/switchery/dist/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />    
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" >
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Data Detail Kegiatan Pengadaan</strong></h4>
                <div class="row button-group">
                    <div class="col-xs-2">
                        <a href="{{ url('/tender') }}" class="btn btn-info">Lihat Data</a> 
                    </div>
                    <div class="col-xs-2">
                        <a href="{{ url('/tender/add') }}" class="btn waves-effect waves-light btn-success">Tambah Data</a>
                    </div>
                    @if($model['tender'][0]['tenderStatus']=="1")
                        <div class="col-xs-2">
                            <a href="#" data-toggle="modal" data-target="#responsive-modal" class="btn waves-effect waves-light btn-warning">Kirim Email</a>
                        </div>
                    @endif
                    
                    @if(date("Y-m-d H:i:s") > $model['tender'][0]['tglNegoAkhir'] && $model['tender'][0]['tglNegoAkhir']!="")
                        @if($model['tender'][0]['tenderStatus']<"4")
                        <div class="col-xs-2">
                            <a href="{{ url('/tender/pemenang') }}/{{ $model['tender'][0]['tenderId'] }}/0" class="btn waves-effect waves-light btn-warning">Input Pemenang</a>
                        </div>
                        @endif
                    @endif
                </div>   
                <div class="card-body">
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#kegiatanPengadaan" role="tab">Kegiatan Pengadaan</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#undangan" role="tab">Undangan</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#mengikuti" role="tab">Mengikuti</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#negoHarga" role="tab">Undangan Nego Harga</a> </li>
                    </ul>                    
                    @if($pesan!="")
                        <br />
                        @if($kode=="99")
                            <div class="alert alert-success">
                                <i class="fa fa-exclamation-circle"></i>
                                {{ $pesan }}
                            </div>
                        @elseif($kode=="90")
                            <div class="alert alert-danger">
                                <i class="fa fa-exclamation-circle"></i>
                                {{ $pesan }}
                            </div>
                        @endif
                    @endif
                    
                    <!-- sample modal content -->
                    <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Kirim Email</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <form action="{{ url('/tender/kirimemail') }}/{{ $model['tender'][0]['tenderId']}}" method="post">
                                    <div class="modal-body">  
                                            @csrf                                           
                                            <div class="form-group">
                                                <input type="checkbox" name="kategori" id="md_checkbox_1" value="{{ $model['tender'][0]['mkatId'] }}" class="chk-col-red" />
                                                <label for="md_checkbox_1">Kategori : {{ $model['tender'][0]['kategori'][0]['mkatNama'] }}</label>
                                            </div>
                                            <div class="form-group">
                                                <h5 class="m-t-20">Supplier</h5>
                                                <select name="vendor[]" class="select2 m-b-10 select2-multiple" style="width: 100%" multiple="multiple">
                                                <!--<select name="vendor" class="select2 form-control custom-select" multiple="multiple">-->                                                    
                                                    @foreach($model['supp'] as $val)
                                                        <option value="{{ $val['supId'] }}">{{ $val['supNama'] }}</option>
                                                    }
                                                    @endforeach                                             
                                                </select>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-danger waves-effect waves-light">Save changes</button>
                                    </div>                                
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.modal -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="kegiatanPengadaan" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <table class="table color-bordered-table default-bordered-table">
                                        <tr>
                                            <td width="20%">Company</td>
                                            <td width="5%" align="center">:</td>
                                            <td colspan='2'>
                                                @if($model['tender'][0]['comp']=='10100')
                                                    PT SEMEN INDONESIA LOGISTIK
                                                @elseif($model['tender'][0]['comp']=='10200')
                                                    PT SEMEN INDONESIA DISTRIBUTOR
                                                @elseif($model['tender'][0]['comp']=='10300')
                                                    PT VARIA USAHA DHARMA SEGARA
                                                @elseif($model['tender'][0]['comp']=='10400')
                                                    PT VARIA USAHA LINTAS SEGARA
                                                @elseif($model['tender'][0]['comp']=='10500')
                                                    PT VARIA USAHA BAHARI
                                                @else
                                                    
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tahun</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['tenderTahun'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nomor</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['tenderNomor'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Kegiatan</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['tenderNama'] }}</td>
                                        </tr>  
                                        <tr>
                                            <td>Jenis</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>
                                                @if($model['tender'][0]['tenderJenis']=='1')
                                                    Pengadaan Barang Stok
                                                @elseif($model['tender'][0]['tenderJenis']=='2')
                                                    Pengadaan Barang Non Stok
                                                @elseif($model['tender'][0]['tenderJenis']=='3')
                                                    Pengadaan Jasa
                                                @else
                                                    ''
                                                @endif
                                            </td>
                                        </tr>  
                                        <tr>
                                            <td>Spesifikasi</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['tenderSpesifikasi'] }}</td>
                                        </tr> 
                                        <tr>
                                            <td>Currency</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['tenderCurency'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Harga</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ number_format($model['tender'][0]['tenderBudget'],2,',','.') }}</td>
                                        </tr>
                                        <tr>
                                            <td>Type</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>
                                                @if($model['tender'][0]['tenderType']=='1')
                                                    Tender
                                                @elseif($model['tender'][0]['tenderType']=='2')
                                                    Non Tender
                                                @elseif($model['tender'][0]['tenderType']=='3')
                                                    Repead Order
                                                @else
                                                    
                                                @endif
                                            </td>
                                        </tr>  
                                        <tr>
                                            <td>Kategori</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['kategori']['0']['mkatNama'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Close</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>{{ $model['tender'][0]['tglTutup'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Lampiran 1</td>
                                            <td align="center">:</td>
                                            <td colspan='2'>
                                                @if($model['tender'][0]['tenderFile']!="")
                                                    <a href="{{ asset('../storage/'.$model['tender'][0]['tenderFile']) }}" target="_blank()">Lampiran</a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Lampiran 2</td>
                                            <td align="center">:</td>
                                            <td  colspan='2'>
                                                @if($model['tender'][0]['tenderGambar']!="")
                                                    <a href="{{ asset('../storage/'.$model['tender'][0]['tenderGambar']) }}" target="_blank()">Lampiran</a>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td align="center">:</td>
                                            <td>
                                                @if($model['tender'][0]['tenderStatus']=='1')
                                                    Baru
                                                @elseif($model['tender'][0]['tenderStatus']=='2')
                                                    Undangan Terkirim
                                                @elseif($model['tender'][0]['tenderStatus']=='3')
                                                    Nego harga
                                                @elseif($model['tender'][0]['tenderStatus']=='4')
                                                    Selesai
                                                @else
                                                    
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Reference 1</td>
                                            <td align="center">:</td>
                                            <td  colspan='2'>{{ $model['tender'][0]['tenderReff1'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Reference 2</td>
                                            <td align="center">:</td>
                                            <td  colspan='2'>{{ $model['tender'][0]['tenderReff2'] }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--second tab-->
                        <div class="tab-pane" id="undangan" role="tabpanel">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="tbKegiatanPengadaanDetail" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th align="center">No</th>
                                                <th>Comp</th>
                                                <th>Tahun</th>
                                                <th>Kegitan Pengadaan</th>
                                                <th>Nama Vendor</th>
                                                <th>Status Email</th>
                                                <th>Status</th>
                                                <!--<th>Aksi</th>-->
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th align="center">No</th>
                                                <th>Comp</th>
                                                <th>Tahun</th>
                                                <th>Kegitan Pengadaan</th>
                                                <th>Nama Vendor</th>
                                                <th>Status Email</th>
                                                <th>Status</th>
                                                <!--<th>Aksi</th>-->
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach($model['tenderDetail'] as $dtDet)                                    
                                                <tr>
                                                    <td align="center">{{$loop->iteration}}</td>
                                                    <td>{{ $dtDet['tender']['0']['comp'] }}</td>
                                                    <td>{{ $dtDet['tender']['0']['tenderTahun'] }}</td>
                                                    <td>{{ $dtDet['tender']['0']['tenderNama'] }}</td>
                                                    <td>{{ $dtDet['supplier']['0']['supNama'] }}</td>
                                                    <td>{{ $dtDet['email']['0']['Stat'] }}</td>
                                                    <td>
                                                        @if($dtDet['penawaranStatus']=='1')
                                                            Baru
                                                        @elseif($dtDet['penawaranStatus']=='2')
                                                            Mengikuti
                                                        @elseif($dtDet['penawaranStatus']=='3')
                                                            Nego
                                                        @elseif($dtDet['penawaranStatus']=='4')
                                                            Menang 
                                                        @elseif($dtDet['penawaranStatus']=='5')
                                                            Kalah 
                                                        @elseif($dtDet['penawaranStatus']=='6')
                                                            Tidak Mengikuti
                                                        @else
                                                            Selesai
                                                        @endif
                                                    </td>
                                                    <!--<td align="center">
                                                        <div class="hidden-sm hidden-xs action-buttons">                                                                                                
                                                            <a class="green" href="" title="Detail"><i class="ace-icon fa fa-search bigger-130"></i></a>
                                                            <a class="green" href="" title="Ubah"><i class="ace-icon fa fa-edit bigger-130"></i></a>
                                                        </div>
                                                    </td>-->
                                                </tr>  
                                            @endforeach                                                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> 
                        <!--second tab 2-->
                        <div class="tab-pane" id="mengikuti" role="tabpanel">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="tbKegiatanPengadaanMengikuti" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <td rowspan="2" align="center">No</td>
                                                <!--<th>Comp</th>
                                                <th>Tahun</th>
                                                <th>Kegitan Pengadaan</th>-->
                                                <td rowspan="2">Nama Vendor</td>
                                                <td align="center" colspan="2">Anggaran</td>
                                                <td align="center" colspan="3">Penawaran</td>
                                                <td align="center" colspan="4">Negosiasi</td>
                                                <td rowspan="2" align="center">Status</td>
                                                <td rowspan="2" align="center">Aksi</td>
                                            </tr>
                                            <tr>
                                                <td align="center">Harga</td>
                                                <td align="center">Curr</td>
                                                <td align="center">Harga</td>
                                                <td align="center">Curr</td>
                                                <td align="center">Expired</td>
                                                <td align="center">Awal</td>
                                                <td align="center">Akhir</td>
                                                <td align="center">Harga</td>
                                                <td align="center">Tanggal</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($model['dtPengikut'] as $dtPengikut)                                    
                                                <tr>
                                                    <td align="center">{{$loop->iteration}}</td>
                                                    <!--<td>{{ $dtPengikut['comp'] }}</td>
                                                    <td>{{ $dtPengikut['tenderTahun'] }}</td>
                                                    <td>{{ $dtPengikut['tenderNama'] }}</td>-->
                                                    <td>{{ $dtPengikut['supNama'] }}</td>
                                                    <td align="right">{{ $dtPengikut['harga'] }}</td>
                                                    <td>{{ $dtPengikut['currency'] }}</td>
                                                    <td align="right">{{ $dtPengikut['hargaPenawaran'] }}</td>
                                                    <td>{{ $dtPengikut['currencyPenawaran'] }}</td>
                                                    <td>{{ $dtPengikut['expired'] }}</td>
                                                    <td align="right">{{ $dtPengikut['tglNegoAwal'] }}</td>
                                                    <td align="right">{{ $dtPengikut['tglNegoAkhir'] }}</td>
                                                    <td align="right">
                                                        @if(date("Y-m-d H:i:s")>$dtPengikut['tglNegoAkhir'])
                                                            {{ $dtPengikut['hargaNegosiasi'] }}
                                                        @else
                                                            
                                                        @endif
                                                    </td>
                                                    <td align="right">{{ $dtPengikut['tglNegosiasi'] }}</td>
                                                    <td>                                                        
                                                        @if($dtPengikut['penawaranStatus']=='1')
                                                            Baru
                                                        @elseif($dtPengikut['penawaranStatus']=='2')
                                                            Mengikuti
                                                        @elseif($dtPengikut['penawaranStatus']=='3')
                                                            Nego
                                                        @elseif($dtPengikut['penawaranStatus']=='4')
                                                            Menang 
                                                        @elseif($dtPengikut['penawaranStatus']=='5')
                                                            Kalah 
                                                        @elseif($dtPengikut['penawaranStatus']=='6')
                                                            Tidak Mengikuti
                                                        @else
                                                            Selesai
                                                        @endif
                                                    </td>
                                                    <td align="center">
                                                        <div class="hidden-sm hidden-xs action-buttons">                                                                                                
                                                            <a class="green" href="{{ url('/tender/detailtender') }}/{{ $dtPengikut['penawaranId'] }}" title="Detail"><i class="ace-icon fa fa-search bigger-130"></i></a>
                                                            <a class="green" href="{{ url('/tender/pemenang') }}/{{ $dtDet['tender']['0']['tenderId'] }}/{{ $dtDet['supplier']['0']['supId'] }}" title="Pemenang"><i class="ace-icon fa fa-paper-plane-o bigger-130"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>  
                                            @endforeach                                                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--second tab 3-->
                        <div class="tab-pane" id="negoHarga" role="tabpanel">
                            <div class="card-body">
                                <!--{{date("Y-m-d H:i:s")}} < {{$model['tender'][0]['tglTutup'] }}-->
                                @if(date("Y-m-d H:i:s") < $model['tender'][0]['tglTutup'])
                                    <div class="col-lg-6 col-md-12">
                                        <div class="alert alert-danger ">                                            
                                            <h3 class="text-danger">
                                                <i class="fa fa-exclamation-triangle"></i> 
                                                Maaf !
                                            </h3> 
                                            Untuk saat ini undangan negosiasi tidak bisa dilakukan, karena proses penawaran belum ditutup.
                                        </div>
                                    </div>
                                @elseif(date("Y-m-d H:i:s") > $model['tender'][0]['tglNegoAkhir'] && $model['tender'][0]['tglNegoAwal']<>"")
                                    <div class="col-lg-6 col-md-12">
                                        <div class="alert alert-danger ">                                            
                                            <h3 class="text-danger">
                                                <i class="fa fa-exclamation-triangle"></i> 
                                                Maaf !
                                            </h3> 
                                            Untuk saat ini undangan negosiasi tidak bisa dilakukan, karena proses negosiasi sudah ditutup.
                                        </div>
                                    </div>
                                @else
                                    <form action="{{ url('/tender/waktunegoall') }}/{{ $model['tender'][0]['tenderId'] }}" method="post">
                                        @csrf                                           
                                        <div class="form-group">                                                
                                            <h5 class="m-t-20">Tanggal Awal</h5>
                                            <div class="input-group">
                                                <input type="text" class="form-control" required id="tglNego1" autocomplete="off" name="tglNego1" value="{{old('tglNego1')}}"> 
                                                <div class="input-group-append" id="input-group-append-id">
                                                    <span class="input-group-text"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">                                                
                                            <h5 class="m-t-20">Tanggal Akhir</h5>
                                            <div class="input-group">
                                                <input type="text" class="form-control" required id="tglNego2" autocomplete="off" name="tglNego2" value="{{old('tglNego2')}}"> 
                                                <div class="input-group-append" id="input-group-append-id2">
                                                    <span class="input-group-text"><i class="icon-calender"></i></span>
                                                </div>
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <h5 class="m-t-20">Supplier</h5>
                                            <select name="supplier[]" class="select2 m-b-10 select2-multiple" required style="width: 100%" multiple="multiple">
                                            <!--<select name="vendor" class="select2 form-control custom-select" multiple="multiple">-->                                                    
                                                @foreach($model['dtPengikut'] as $val2)
                                                    <option value="{{ $val2['suppId'] }}">{{ $val2['supNama'] }}</option>
                                                }
                                                @endforeach                                             
                                            </select>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-danger waves-effect waves-light">Kirim</button>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-tagsinput/src/bootstrap-tagsinput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/js/dataTablesPlugins/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datetimepicker.min.js')}}"></script>
    <script>            
        $('#tbKegiatanPengadaanDetail').DataTable();
        //$('#tbKegiatanPengadaanMengikuti').DataTable();
        $('#tbKegiatanPengadaanMengikuti').DataTable({
            dom: 'Bfrtip',
            buttons: [
                //'excel', 'pdf', 'print'
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel'
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF'
                },
                {
                    extend:    'print',
                    text:      '<i class="fa fa-print"></i>',
                    titleAttr: 'Print'
                }
            ],
            columnDefs: [                
                {
                    "targets": [2],
                    "data": "2",
                    "className": "dt-body-left",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },   
                {
                    "targets": [4],
                    "data": "4",
                    "className": "dt-head-right",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },     
                {
                    "targets": [9],
                    "data": "9",
                    "className": "dt-head-right",
                    "render": $.fn.dataTable.render.number( '.', ',', 2 )
                },   
            ]
        });
        jQuery(document).ready(function() {  
            $('#tglNego1').datetimepicker({
                autoclose: true,
                format: 'yyyy-mm-dd hh:ii',
                //todayHighlight: true,
                //format: "mm-yyyy",
                //viewMode: "months", 
                //minViewMode: "months"
            });
            $('#tglNego2').datetimepicker({
                autoclose: true,
                format: 'yyyy-mm-dd hh:ii',
                //todayHighlight: true,
                //format: "mm-yyyy",
                //viewMode: "months", 
                //minViewMode: "months"
            });            
            $('#input-group-append-id').click(function() {
                //alert("Bismillah !");
                $('#tglNego1').datetimepicker('show');
            });          
            $('#input-group-append-id2').click(function() {
                //alert("Bismillah !");
                $('#tglNego2').datetimepicker('show');
            });
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

            // For multiselect
            $('#pre-selected-options').multiSelect();
            $('#optgroup').multiSelect({
                selectableOptgroup: true
            });
            $('#public-methods').multiSelect();
        });
    </script>
@endpush
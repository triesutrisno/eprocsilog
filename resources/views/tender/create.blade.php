@extends('layouts.main')

@push('styles')
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" >
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Tambah Data Kegiatan Pengadaan</strong></h4>
                <div class="card-body">
                    <form action="{{ url('/tender/addpost') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="tenderTahun">Tahun : <span class="danger">*</span></label>
                                    <select class="custom-select form-control" required id="tenderTahun" name="tenderTahun">
                                        <option value="">Silakan Pilih</option>
                                        @php 
                                        $now = "2020";
                                        $upto = date("Y") + 1;
                                        for($i=$now;$i<=$upto;$i++){ 
                                        @endphp                                 
                                        <option value="{{ $i }}">{{ $i }}</option>
                                        @php } @endphp
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="comp">Company : <span class="danger">*</span></label>
                                    <select class="custom-select form-control" required id="comp" name="comp">
                                        <option value="">Silakan Pilih</option>
                                        <option value="10100">SILOG</option>
                                        <option value="10200">SID</option>
                                        <option value="10300">VUDS</option>
                                        <option value="10400">VULS</option>
                                        <option value="10500">VUBA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="unitId">Unit : <span class="danger">*</span></label>
                                    <select class="custom-select form-control" required id="unitId" name="unitId">
                                        <option value="">Silakan Pilih</option>
                                        @foreach($dtUnit as $vUnit)
                                            <option value="{{ $vUnit['unitId'] }}">{{ $vUnit['unit'] }}</option>
                                        }
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="tenderNama">Nama Kegiatan : <span class="danger">*</span></label>
                                    <input type="text" class="form-control" id="tenderNama" name="tenderNama" value="{{old('tenderNama')}}"> 
                                </div>
                            </div> 
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="tenderJenis">Jenis Kegiatan : <span class="danger">*</span></label>
                                    <select class="custom-select form-control" required id="tenderJenis" name="tenderJenis">
                                        <option value="">Silakan Pilih</option>
                                        <option value="1">Pengadaan Barang Stok</option>
                                        <option value="2">Pengadaan Barang Non Stok</option>
                                        <option value="3">Pengadaan Jasa</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="mkatId">Kategori : <span class="danger">*</span> </label>
                                    <select name="mkatId" id="mkatId" class="custom-select form-control select2" required>
                                        <option value="">Silakan Pilih</option>
                                        @foreach($dtKat as $val)
                                            <option value="{{ $val['mkatId'] }}">{{ $val['mkatNama'] }}</option>
                                        }
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label for="tenderSpesifikasi">Spesifikasi : <span class="danger">*</span></label>
                                    <textarea name="tenderSpesifikasi" id="tenderSpesifikasi" rows="6" class="form-control" required>{{old('tenderSpesifikasi')}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="tenderCurency">Mata Uang : <span class="danger">*</span></label>
                                    <select name="tenderCurency" id="tenderCurency" class="custom-select form-control select2" required>
                                        <option value="">Silakan Pilih</option>
                                        @foreach($dtCurr as $val2)
                                            <option value="{{ $val2['currKode'] }}">{{ $val2['currKode'] }}</option>
                                        }
                                        @endforeach
                                    </select>
                                </div>
                            </div> 
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="tenderBudget">Anggaran : <span class="danger">*</span></label>
                                    <input type="text" class="form-control" required id="tenderBudget" name="tenderBudget" value="{{old('tenderBudget')}}"> 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="tenderType">Type Kegiatan :</label>
                                    <select class="custom-select form-control" required id="tenderType" name="tenderType">
                                        <option value="">Silakan Pilih</option>
                                        <option value="1">Tender</option>
                                        <option value="2">Non Tender</option>
                                        <option value="3">Repead Order</option>
                                    </select>
                                </div>
                            </div>  
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="tglTutup">Tanggal Tutup : <span class="danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" required id="tglTutup" autocomplete="off" name="tglTutup" value="{{old('tglTutup')}}"> 
                                        <div class="input-group-append" id="input-group-append-id">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="tglAnwijzing">Tanggal Anwijzing : </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control"  id="tglAnwijzing" autocomplete="off" name="tglAnwijzing" value="{{old('tglAnwijzing')}}"> 
                                        <div class="input-group-append" id="input-group-append-id2">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="lokasiAnwijzing">Lokasi Anwijzing : </label>
                                    <input type="text" class="form-control" id="lokasiAnwijzing" name="lokasiAnwijzing" value="{{old('lokasiAnwijzing')}}"> 
                                </div>
                            </div>   
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="tenderFile">Lampiran 1 : <span class="danger"><small> <code>Maximal file 1M and only zip file type.</code></small></span></label>
                                    <input type="file" class="dropify" data-max-file-size="1M" data-allowed-file-extensions="zip" id="tenderFile" name="tenderFile"> 
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="tenderGambar">Lampiran 2 : <span class="danger"><small> <code>Maximal file 1M and only zip file type.</code></small></span></label>
                                    <input type="file" class="dropify" data-max-file-size="1M" data-allowed-file-extensions="zip" id="tenderGambar" name="tenderGambar"> 
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="form-group text-center m-t-20">
                            <div  class="col-lg-2 col-md-2">
                                <button class="btn btn-block btn-success" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('js/validation.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datetimepicker.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#tglTutup').datetimepicker({
                autoclose: true,
                format: 'yyyy-mm-dd hh:ii',
                //todayHighlight: true,
                //format: "mm-yyyy",
                //viewMode: "months", 
                //minViewMode: "months"
            });
            $('#tglAnwijzing').datetimepicker({
                autoclose: true,
                format: 'yyyy-mm-dd hh:ii',
                //todayHighlight: true,
                //format: "mm-yyyy",
                //viewMode: "months", 
                //minViewMode: "months"
            });            
            $('#input-group-append-id').click(function() {
                //alert("Bismillah !");
                $('#tglTutup').datetimepicker('show');
            });          
            $('#input-group-append-id2').click(function() {
                //alert("Bismillah !");
                $('#tglAnwijzing').datetimepicker('show');
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
        
    </script>
@endpush
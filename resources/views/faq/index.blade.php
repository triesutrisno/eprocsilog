@extends('layouts.main')

@push('styles')
    <link href="{{ asset('css/pages/other-pages.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pages/ui-bootstrap-page.css') }}" rel="stylesheet">
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!--
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Beranda</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
            </ol>
        </div>
    </div>
    -->
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-8 col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><strong>Frequently Asked Questions</strong></h4>
                    <div id="accordion2" role="tablist" class="minimal-faq" aria-multiselectable="true">
                        <div class="card m-b-0">
                            <div class="card-header" role="tab" id="headingOne11">
                                <h5 class="mb-0">
                                <a class="link" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne11" aria-expanded="true" aria-controls="collapseOne11">
                                  1. Apa itu eproc SILOG Group ?
                                </a>
                              </h5>
                            </div>
                            <div id="collapseOne11" class="collapse show" role="tabpanel" aria-labelledby="headingOne11">
                                <div class="card-body">
                                    Eproc Silog Group adalah aplikasi berbasis website/internet yang digunakan untuk melakukan proses pengadaan 
                                    barang atau jasa dilingkungan Semen Indonesia Logistik Group                            
                                </div>
                            </div>
                        </div>
                        <div class="card m-b-0">
                            <div class="card-header" role="tab" id="headingTwo22">
                                <h5 class="mb-0">
                                <a class="collapsed link" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo22" aria-expanded="false" aria-controls="collapseTwo22">
                                  2. Bagaimana cara untuk mengakses portal ini ?
                                </a>
                              </h5>
                            </div>
                            <div id="collapseTwo22" class="collapse" role="tabpanel" aria-labelledby="headingTwo22">
                                <div class="card-body">
                                    Silakan kunjungi website dengan alamat <a href="http://eproc.silog.co.id" class="card-title text-blue">http://eproc.silog.co.id<a>
                                </div>
                            </div>
                        </div>
                        <div class="card m-b-0">
                            <div class="card-header" role="tab" id="headingThree33">
                                <h5 class="mb-0">
                                <a class="collapsed link" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree33" aria-expanded="false" aria-controls="collapseThree33">
                                  3. Bagaimana cara mendaftar sebagai vendor / rekanan pada portal ini ?
                                </a>
                              </h5>
                            </div>
                            <div id="collapseThree33" class="collapse" role="tabpanel" aria-labelledby="headingThree33">
                                <div class="card-body">
                                    Silakan klik tombol <a href="http://eproc.silog.co.id/public/register" class="card-title text-blue">Daftar</a> pada halaman depan 
                                    atau silakan masuk ke menu <a href="http://eproc.silog.co.id/public/register" class="card-title text-blue">Pendaftaran Rekanan</a>
                                </div>
                            </div>
                        </div>
                        <div class="card m-b-0">
                            <div class="card-header" role="tab" id="headingOne111">
                                <h5 class="mb-0">
                                <a class="link" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne111" aria-expanded="true" aria-controls="collapseOne111">
                                  4. Dokumen apa saja yang harus diupload sebagai syarat menjadi vendor / rekanan di portal ini ?
                                </a>
                              </h5>
                            </div>
                            <div id="collapseOne111" class="collapse" role="tabpanel" aria-labelledby="headingOne111">
                                <div class="card-body">
                                    Untuk bisa menjadi vendor / rekanan dilingkungan Semen Indonesia Logistik Group harus menyertakan (mengupload) file - file sebagai berikut :
                                    <ul class="list-icons">
                                        <li><i class="fa fa-check text-info"></i> SPT perusahaan</li>
                                        <li><i class="fa fa-check text-info"></i> NPWP</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card m-b-0">
                            <div class="card-header" role="tab" id="headingTwo222">
                                <h5 class="mb-0">
                                <a class="collapsed link" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo222" aria-expanded="false" aria-controls="collapseTwo222">
                                  5. Bagaimana cara mengikuti lelang atau tender diportal ini?
                                </a>
                              </h5>
                            </div>
                            <div id="collapseTwo222" class="collapse" role="tabpanel" aria-labelledby="headingTwo222">
                                <div class="card-body">
                                    Untuk bisa mengikuti lelang atau tender pada portal ini tentunya vendor / rekanan harus terlebih dahulu melakukan login ke portal ini.
                                    Setelah itu silakan pilih lelang atau tender mana yang ingin diikuti dan lakukan penawaran dengan cara menguploadkan file penawaran dan syarat-syarat lainnya.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingThree333">
                                <h5 class="mb-0">
                                <a class="collapsed link" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree333" aria-expanded="false" aria-controls="collapseThree333">
                                  6. Bagaimana cara untuk melakukan penagihan diportal ini ?
                                </a>
                              </h5>
                            </div>
                            <div id="collapseThree333" class="collapse" role="tabpanel" aria-labelledby="headingThree333">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div style="position:fixed;bottom:0;border-bottom:30px;">
                <img src="{{ url('assets/images/gambarFaq.png') }}">
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    
@endpush
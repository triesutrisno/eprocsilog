@extends('layouts.main')

@push('styles')
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Permintaan Pembayaran</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/permintaanpembayaran') }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                </div>                
                @if(session('kode')=="99")
                    <div class="alert alert-success">
                        <i class="fa fa-exclamation-circle"></i>
                        {{ session('pesan') }}
                    </div>
                @elseif(session('kode')=="90")
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-circle"></i>
                        {{ session('pesan') }}
                    </div>
                @endif                 
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="tbMasterNextnumber" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Aksi</th>
                                    <th align="center">No</th>
                                    <th>Comp</th>
                                    <th>No Dok</th>
                                    <th>Type</th>
                                    <th>No Invoice</th>
                                    <th>Status</th>
                                    <th>Keterangan</th>
                                    <th>Supplier</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($model as $dt)                                    
                                    <tr>
                                        @csrf
                                        <td align="center">
                                            <div class="hidden-sm hidden-xs action-buttons">  
                                                <a class="green" href="{{ url('/permintaanpembayaran')}}/detail/{{ $dt['ppId'] }}" title="Detail"><i class="ace-icon fa fa-search bigger-130"></i></a>
                                                @if($dt['status']=='2' || $dt['status']=='4')
                                                    <a class="green" href="{{ url('/permintaanpembayaran')}}/addpp/{{ $dt['ppId'] }}" title="Input No PP"><i class="ace-icon fa fa-plus-circle bigger-130"></i></a>
                                                @endif
                                            </div>
                                        </td>
                                        <td align="center">{{$loop->iteration}}</td>
                                        <td>{{ $dt['comp'] }}</td>
                                        <td>{{ $dt['noDokumen'] }}</td>
                                        <td>{{ $dt['typeDokumen'] }}</td>
                                        <td>{{ $dt['noInvoice'] }}</td>
                                        <td>
                                            @if($dt['status']=='1')
                                                Baru
                                            @elseif($dt['status']=='2')
                                                Lolos Verifikasi
                                            @elseif($dt['status']=='3')
                                                Tidak Lolos Verifikasi
                                            @elseif($dt['status']=='4')
                                                No PP Sudah dibuat
                                            @elseif($dt['status']=='5')
                                                No Ekspedisi Sudah dibuat
                                            @elseif($dt['status']=='6')
                                                Proses Akuntansi
                                            @elseif($dt['status']=='7')
                                                Proses Bendahara
                                            @elseif($dt['status']=='8')
                                                Sudah Terbayar                                                
                                            @endif
                                        </td>
                                        <td>{{ $dt['keterangan'] }}</td>
                                        <td>{{ $dt['supplier']['0']['supNama'] }}</td>
                                    </tr>  
                                @endforeach                                                   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script>
    $('#tbMasterNextnumber').DataTable();
    $(document).ready(function() {
        $('.hapuspp').click(function(){
            var jawab = confirm("Anda yakin akan menghapus data ini ?");
            if (jawab === true) {
//            kita set hapus false untuk mencegah duplicate request
                var hapus = false;
                if (!hapus) {
                    hapus = true;
                    //$.post('hapus.php', {id: $(this).attr('data-id')},
                    var idne = $(this).attr('data-id');
                    var _token = $('input[name="_token"]').val();
                    //alert(idne);
                    $.ajax({
                        url : "{{ url('/permintaanpembayaran') }}/{{ session('user')->username }}/delete/"+idne,
                        method : "POST",
                        data : {_token:_token},
                        success : function(result){
                            //alert(result);
                            //$('#'+dependent).html(result);
                            location.reload();
                        }
                    })
                    hapus = false;
                }
            } else {
                return false;
            }
            
        });
    });    
</script>
@endpush
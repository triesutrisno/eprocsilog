@extends('layouts.main')

@push('styles')    
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Tambah Data Permintaan Pembayaran</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/permintaanpembayaran') }}/{{ session('user')->username }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('/permintaanpembayaran') }}/{{ session('user')->username }} " method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="comp">Company : <span class="danger">*</span></label>
                                    <select class="custom-select form-control" required id="comp" name="comp">
                                        <option value="">Silakan Pilih</option>
                                        <option value="10100">SILOG</option>
                                        <option value="10200">SID</option>
                                        <option value="10300">VUDS</option>
                                        <option value="10400">VULS</option>
                                        <option value="10500">VUBA</option>
                                    </select>
                                </div>
                            </div>                          
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="noInvoice">No Invoice : <span class="danger">*</span></label>
                                    <input type="text" name="noInvoice" id="noInvoice" class="form-control inpNoInvoice" required value="{{old('noInvoice')}}">
                                </div>
                            </div>                           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis">No Dokumen /OP/OJ : <span class="danger">*</span></label>
                                    <input type="text" name="noDokumen" id="noDokumen" class="form-control" required value="{{old('noDokumen')}}">
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jenis">Type Dokumen : <span class="danger">*</span></label>
                                    <!--<input type="text" name="typeDokumen" id="typeDokumen" class="form-control" required value="{{old('typeDokumen')}}">-->
                                    <select class="custom-select form-control" required id="typeDokumen" name="typeDokumen">
                                        <option value="">Silakan Pilih</option>
                                        <option value="OP">PEMBELIAN BARANG</option>
                                        <option value="OJ">PEMBELIAN JASA</option>
                                        <option value="O5">PEMBELIAN IMPORT</option>
                                        <option value="OK">PEMBELIAN KONSINYASI</option>
                                        <option value="O7">PEMBELIAN MINISTORE</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!--<label for="ppFile">Lampiran  : <span class="danger"><small> * <code>Maximal file 2M and only zip file type.</code></small></span></label>
                                    <input type="file" class="dropify required" data-max-file-size="2M" data-allowed-file-extensions="zip" id="ppFile" name="ppFile"> -->
                                    <label for="ppFile">Lampiran : <span class="danger">*</span> </label>
                                    <input type="file" class="dropify required" data-max-file-size="2M" data-allowed-file-extensions="zip" id="ppFile" name="ppFile"> 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keterangan">Keterangan : <span class="danger">*</span></label>
                                    <textarea name="keterangan" id="keterangan" rows="6" class="form-control" required>{{old('keterangan')}}</textarea>
                                </div>
                            </div>    
                        </div>
                        <div class="col-md-12" id="dataOP"></div>                        
                        <!--<a href="{{ url('/permintaanpembayaran') }}/{{ session('user')->username }}" class="btn btn-info waves-effect waves-light">Kembali</a>-->
                        <button type="submit" id="simpan" class="btn btn-success waves-effect waves-light m-r-10">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('js/custom.min.js') }}"></script>
<script src="{{ asset('assets/plugins/wizard/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>  
<script>
    //$('#tbMasterNextnumber').DataTable();
    $(document).ready(function() {
        $('#typeDokumen').change(function(){
            //alert('bismillah');
            var _doco = $('#noDokumen').val();
            var _dcto = $('#typeDokumen').val();
            $.ajax({
                url : "{{ url('getOP') }}/",
                method : "GET",
                data : {doco:_doco, dcto:_dcto},
                success : function(result){
                    //alert(result);
                    $('#dataOP').html(result);
                    //location.reload();
                }
            })            
        });
    });  
        
    function cekNominal(val,id) {
        var openAmount = $('#'+id).attr('data-id');
        if(parseInt(val) > parseInt(openAmount)){
            //alert(openAmount+ ' ' +val);
            alert('Mohon maaf, nilai pengajuan tidak boleh melebihi nilai Open !');
            $('#simpan').prop('disabled', true);
        }else{
            $('#simpan').prop('disabled', false);
        }
    }
</script>
@endpush
@extends('layouts.main')

@push('styles')    
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Kirim Data Keterangan Pembayaran</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/permintaanpembayaran') }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('/permintaanpembayaran/nokomplit') }}/{{ $model[0]['ppId'] }} " method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keterangan">Keterangan : <span class="danger">*</span></label>
                                    <textarea name="keterangan" id="keterangan" rows="6" class="form-control" required>{{old('keterangan')}}</textarea>
                                    <input type="hidden" name="email" value="{{ $model['0']['supplier']['0']['supEmail'] }}" readonly="true">
                                </div>
                            </div>    
                        </div>                      
                        <a href="{{ url('/permintaanpembayaran/detail') }}/{{ $model[0]['ppId'] }}" class="btn btn-warning waves-effect waves-light">Kembali</a>
                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('js/custom.min.js') }}"></script>
<script src="{{ asset('assets/plugins/wizard/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>  
<script>
    //$('#tbMasterNextnumber').DataTable();
    $(document).ready(function() {
        $('#typeDokumen').change(function(){
            //alert('bismillah');
            var _doco = $('#noDokumen').val();
            var _dcto = $('#typeDokumen').val();
            $.ajax({
                url : "{{ url('getOP') }}/",
                method : "GET",
                data : {doco:_doco, dcto:_dcto},
                success : function(result){
                    //alert(result);
                    $('#dataOP').html(result);
                    //location.reload();
                }
            })            
        });
    });    
</script>
@endpush
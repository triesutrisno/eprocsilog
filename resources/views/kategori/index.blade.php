@extends('layouts.main')

@push('styles')
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Master Kategori</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/kategori') }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/kategori/add') }}" class="btn btn-block btn-success">Tambah Data</a>
                    </div>
                </div>
                
                 {{ $pesan2 }}
                 
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="tbMasterKategori" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th align="center">No</th>
                                    <th>Kategori</th>
                                    <th>Status</th>
                                    <th>Create</th>
                                    <th>Update</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th align="center">No</th>
                                    <th>Kategori</th>
                                    <th>Status</th>
                                    <th>Create</th>
                                    <th>Update</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($model as $dt)                                    
                                    <tr>
                                        <td align="center">{{$loop->iteration}}</td>
                                        <td>{{ $dt['mkatNama'] }}</td>
                                        <td>
                                            @if($dt['mkatStatus']=='1')
                                                Aktif
                                            @else
                                                Tidak Aktif
                                            @endif
                                        </td>
                                        <td>{{ $dt['created_at'] }}</td>
                                        <td>{{ $dt['updated_at'] }}</td>
                                        @csrf
                                        <td align="center">
                                            <div class="hidden-sm hidden-xs action-buttons">                                                                                                
                                                <a class="green" href="{{ url('/kategori/edit')}}/{{ $dt['mkatId'] }}" title="Ubah"><i class="ace-icon fa fa-edit bigger-130"></i></a>                                                                                              
                                                <a class="green hapusKategori" href="#" data-id="{{ $dt['mkatId'] }}" title="Hapus"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                                            </div>
                                        </td>
                                    </tr>  
                                @endforeach                                                   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script>
    $('#tbMasterKategori').DataTable();
    $(document).ready(function() {
        $('.hapusKategori').click(function(){
            var jawab = confirm("Anda yakin akan menghapus data ini ?");
            if (jawab === true) {
//            kita set hapus false untuk mencegah duplicate request
                var hapus = false;
                if (!hapus) {
                    hapus = true;
                    //$.post('hapus.php', {id: $(this).attr('data-id')},
                    var idne = $(this).attr('data-id');
                    var _token = $('input[name="_token"]').val();
                    //alert(idne);
                    $.ajax({
                        url : "{{ url('/kategori/delete') }}/"+idne,
                        method : "POST",
                        data : {_token:_token},
                        success : function(result){
                            //alert(result);
                            //$('#'+dependent).html(result);
                            location.reload();
                        }
                    })
                    hapus = false;
                }
            } else {
                return false;
            }
            
        });
    });    
</script>
@endpush
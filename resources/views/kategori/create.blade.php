@extends('layouts.main')

@push('styles')
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Tambah Data Master Kategori</strong></h4>
                <div class="card-body">
                    <form action="{{ url('/kategori/addpost') }}" method="post">
                        @csrf
                        <div class="col-md-10">
                            <div class="form-group">
                                <label for="nmKategori">Kategori :</label>
                                <input type="text" class="form-control" required data-validation-required-message="This field is required" id="nmKategori" name="nmKategori" value="{{old('nmKategori')}}"> 
                            </div>
                        </div>                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="status">Status :</label>
                                <select class="custom-select form-control required" id="status" name="status">
                                    <option value="">Silakan Pilih</option>
                                    <option value="1">Aktif</option>
                                    <option value="2">Tidak Aktif</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div  class="col-lg-2 col-md-2">
                                <button class="btn btn-block btn-success" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<script src="{{ asset('js/custom.min.js') }}"></script>
<script src="{{ asset('js/validation.js') }}"></script>
@endpush
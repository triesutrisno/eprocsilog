@extends('layouts.main')

@push('styles')
    <link href="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
@endpush

@section('container')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->

        @include('partials.alert')

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="col-lg-12">
                        <h4 class="card-title">
                            <strong>Berita Acara Semen Padang Curah baru</strong>
                        </h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('/berita-acara-sp-curah-store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="no_kontrak_perjanjian">No Kontrak Perjanjian :</label>
                                        <input type="text" class="form-control" required
                                            data-validation-required-message="This field is required"
                                            id="no_kontrak_perjanjian" name="no_kontrak_perjanjian"
                                            value="{{ old('no_kontrak_perjanjian') }}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="text-small">Periode SPJ : </label>
                                    <input type="text" name="periode_spj" class="form-control" autocomplete="off">
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="jenis_pelanggan">Customer :</label>
                                        <select class="custom-select form-control required" id="jenis_pelanggan"
                                            name="jenis_pelanggan">
                                            <option value="SIG">SIG</option>
                                            <option value="SBI">SBI</option>
                                            <option value="SG">SG</option>
                                            <option value="SEMEN JUMBO BAG">SEMEN JUMBO BAG</option>
                                            <option value="SEMEN MORTAR">SEMEN MORTAR</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="jenis_plat">Plat :</label>
                                        <select class="custom-select form-control required" id="jenis_plat"
                                            name="jenis_plat">
                                            <option value="PLAT HITAM">Plat Hitam</option>
                                            <option value="PLAT KUNING">Plat Kuning</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="plant">Plant Asal Muatan:</label>
                                        <select class="custom-select form-control required" id="plant" name="plant">
                                            <option value="GRESIK">GRESIK</option>
                                            <option value="TUBAN">TUBAN</option>
                                            <option value="BANYUWANGI">BANYUWANGI</option>
                                            <option value="PRIOK">PRIOK</option>
                                            <option value="CIWANDAN">CIWANDAN</option>
                                            <option value="REMBANG">REMBANG</option>
                                            <option value="KARAWANG">KARAWANG</option>
                                            <option value="AJIBARANG">AJIBARANG</option>
                                            <option value="BOGOR">BOGOR</option>
                                            <option value="CBAWANG">CBAWANG</option>
                                            <option value="PADANG">PADANG</option>
                                            <option value="PALARAN">PALARAN</option>
                                            <option value="CIBUNGUR">CIBUNGUR</option>
                                            <option value="PASOSO">PASOSO</option>
                                            <option value="SIDGRESIK">SIDGRESIK</option>
                                            <option value="SIDTUBAN">SIDTUBAN</option>
                                            <option value="SIDREMBANG">SIDREMBANG</option>
                                            <option value="SIDBANYUWANGI">SIDBANYUWANGI</option>
                                            <option value="SIDCBAWANG">SIDCBAWANG</option>
                                            <option value="SALATIGA">SALATIGA</option>
                                            <option value="SIDSALATIGA">SIDSALATIGA</option>
                                            <option value="SIDCIWANDAN">SIDCIWANDAN</option>
                                            <option value="SIDBOGOR">SIDBOGOR</option>
                                            <option value="SIDCIGADING">SIDCIGADING</option>
                                            <option value="CILACAP">CILACAP</option>
                                            <option value="CIREBON">CIREBON</option>
                                            <option value="LEMPUYANGAN">LEMPUYANGAN</option>
                                            <option value="PRIOK">PRIOK</option>
                                            <option value="DC JAKARTA">DC JAKARTA</option>
                                            <option value="CP SMI Tuban">CP SMI Tuban</option>
                                            <option value="CP SMI Rembang">CP SMI Rembang</option>
                                            <option value="PP SMI Banyuwangi">PP SMI Banyuwangi</option>
                                            <option value="PP SMI Celukan Bawang">PP SMI Celukan Bawang</option>
                                            <option value="DC SMI Maspion">DC SMI Maspion</option>
                                            <option value="DC SMI Jakarta">DC SMI Jakarta</option>
                                            <option value="DC Cirebon SMI">DC Cirebon SMI</option>
                                            <option value="PP SMI Tanjung Priok">PP SMI Tanjung Priok</option>
                                            <option value="PP SMI Ciwandan">PP SMI Ciwandan</option>
                                            <option value="CP Cilacap SBI Virtual Plant SMI">CP Cilacap SBI Virtual Plant
                                                SMI</option>
                                            <option value="CP Narogong SBI Virtual Plant SMI">CP Narogong SBI Virtual Plant
                                                SMI</option>
                                            <option value="CP SBI Tuban">CP SBI Tuban</option>
                                            <option value="CP SBI Rembang">CP SBI Rembang</option>
                                            <option value="CP SBI Cilacap">CP SBI Cilacap</option>
                                            <option value="CP SBI Narogong">CP SBI Narogong</option>
                                            <option value="DC Cirebon SBI">DC Cirebon SBI</option>
                                            <option value="DC Lempuyangan SBI">DC Lempuyangan SBI</option>
                                            <option value="DC SBI Banyuwangi">DC SBI Banyuwangi</option>
                                            <option value="CP Gresik SMI Virtual Plant SBI Masonry">CP Gresik SMI Virtual
                                                Plant SBI Masonry</option>
                                            <option value="PP Ciwandan SMI Virtual Plant SB">PP Ciwandan SMI Virtual Plant
                                                SB</option>
                                            <option value="DC DUMAI">DC DUMAI</option>

                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="status_barang">Status Barang :</label>
                                        <select class="custom-select form-control required" id="status_barang"
                                            name="status_barang">
                                            <option value="KLAIM">Klaim</option>
                                            <option value="NON KLAIM">Non Klaim</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama_pic_supplier">Nama PIC Perusahaan (akan tercetak di print)
                                            :</label>
                                        <input type="text" class="form-control" required
                                            data-validation-required-message="This field is required"
                                            id="nama_pic_supplier" name="nama_pic_supplier"
                                            value="{{ old('nama_pic_supplier') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nama Distributor</label>
                                        <input type="text" class="form-control" required
                                            data-validation-required-message="This field is required"
                                            id="nama_distributor" name="nama_distributor"
                                            value="{{ old('nama_distributor') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="status_barang">Tipe Semen :</label>
                                        <select class="custom-select form-control required" 
                                            id="tipe_semen" name="tipe_semen">
                                            <option value="40">40</option>
                                            <option value="50">50</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Tujuan Bongkar</label>
                                        <input type="text" class="form-control" required
                                            data-validation-required-message="This field is required"
                                            id="tujuan_bongkar" name="tujuan_bongkar"
                                            value="{{ old('tujuan_bongkar') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Upload Excel Data SPJ (max 100 rows)</label>
                                        <input type="file" class="form-control" id="berita_acara_spj"
                                            name="berita_acara_spj" aria-describedby="fileHelp">
                                    </div>
                                </div>
                                <div class="col-md-7 my-auto">
                                    <div class="form-group my-auto">
                                        <a href="{{ url('assets/xls/UPLOAD BERITA ACARA SEMEN PADANG CURAH - TEMPLATE.xlsx') }}">Link Template Upload
                                            Excel ( Format Tanggal dan Qty harus sama persis )</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group text-center m-t-20">
                                        <button class="btn btn-block btn-success" type="submit">Simpan</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection

@push('scripts')
    <script src="{{ asset('js/custom.min.js') }}"></script>
    <script src="{{ asset('js/validation.js') }}"></script>
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script>
        var periode_spj = $('input[name="periode_spj"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        periode_spj.on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format(
                'MM/DD/YYYY'));
        });

        periode_spj.on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    </script>
@endpush

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->

    <title>Surat Keterangan Klaim Charter</title>
    <style>
        html,
        body {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 20px;
            margin: 0px;
            width: 1140px;
            height: 1600px;
        }


        table {
            width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
        }

        th {
            padding: 3px;
            vertical-align: top;
            border: 1px solid black;
            background: gainsboro;

        }

        td {
            padding: 3px;
            vertical-align: top;

        }

        .bordered-table td {
            border: 1px solid black;
        }

        @page {
            size: 'a4';
            margin: 5mm 5mm 5mm 5mm;
            /* change the margins as you want them to be. */
        }

        pre {
            font-family: 'Times New Roman', Times, serif;
        }

        .center {
            text-align: center;
        }

        .nama-ttd {
            margin-top: 50px;
            text-decoration: underline;
            font-weight: bold;
        }

        .box-bottom {
            margin-top: 20px;
            margin-left: 5px;
        }

        .box-bottom-item {
            display: inline-block;
            margin-left: 20px;
            width: 30%;
        }

        .box-bottom-text {
            display: inline-block;
            vertical-align: super;
            height: 30px;
        }

        .box-bottom-box {
            width: 30px;
            height: 15px;
            outline: 2px solid black;
            display: inline-block;
            margin-left: 10px
        }

        .head-row {
            width: 12%;
            font-weight: bold;
        }

        img.bg-paper {
            position: absolute;
            left: 0px;
            top: 0px;
            z-index: -1;
        }
    </style>
</head>

<body onload="window.print()">
    <div style="padding: 100px 0 0 70px;">
        <img class="bg-paper" src="/img/kertas_kop.jpeg" />
        <div style="text-align: center; margin-top: 80px;">
            <h2>SURAT KETERANGAN KLAIM CHARTER</h2>
        </div>
        <table class="table">
            <tr>
                <td class="head-row">Kepada PT</td>
                <td>{{ $data->nama_supplier }}</td>
            </tr>
            <tr>
                <td class="head-row">No Surat</td>
                <td>{{ $data->no_surat_klaim }}</td>
            </tr>
            <tr>
                <td class="head-row">Perihal</td>
                <td>Klaim Charter</td>
            </tr>
        </table>

        <br>
        <span>Terkait adanya klaim, sesuai Berita Acara Penyerahan SPJ dengan rincian :</span>
        <table class="table bordered-table" style="margin-top:20px; text-align:center">
            <thead>
                <tr>
                    <th>NO BA</th>
                    <th>TGL BA</th>
                    <th>KLAIM KANTONG</th>
                    <th>NILAI KANTONG</th>
                    <th>KLAIM SEMEN</th>
                    <th>NILAI SEMEN</th>
                    <th>TOTAL</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $data->no_berita_acara }}</td>
                    <td>{{ \Carbon\Carbon::parse($data->tgl_ba)->format('d-m-Y') }}</td>
                    <td>{{ number_format($data->klaim_kantong, 0, ',', '.') }}</td>
                    <td>Rp {{ number_format($data->harga_kantong, 0, ',', '.') }}</td>
                    <td>{{ number_format($data->klaim_semen, 0, ',', '.') }}</td>
                    <td>Rp {{ number_format($data->harga_semen, 0, ',', '.') }}</td>
                    <td>Rp
                        {{ number_format(
                            $data->klaim_kantong * $data->harga_kantong + $data->klaim_semen * $data->harga_semen,
                            0,
                            ',',
                            '.',
                        ) }}
                    </td>

                    </td>
                </tr>
            </tbody>
        </table>

        <div>
            <p>{!! nl2br($data->keterangan) !!}</p>
        </div>


        {{-- signature --}}

        <div style="width: 100%; margin-top: 60px; display: inline-flex">
            {{-- Signature SILOG --}}
            <div style="display: inline-block; width: 40%">
                <div>PT Semen Indonesia Logistik</div>
                <div>Billing and Reporting</div>
                <div>Tuban, {{ \Carbon\Carbon::parse($data->surat_klaim_approved_at)->format('d-m-Y') }}</div>
                <br><br>
                @if ($data->surat_klaim_approve_status == 1)
                    <div style="height: 90px; margin-left:20px">
                        {!! QrCode::size(90)->generate(
                            'APPROVAL BERITA ACARA SPJ |' .
                                $data->id_berita_acara .
                                '|' .
                                $data->no_berita_acara .
                                '|' .
                                $data->tgl_ba .
                                '|' .
                                $data->nama_supplier .
                                '|' .
                                $data->surat_klaim_approve_status .
                                '|' .
                                $data->surat_klaim_approved_at .
                                '|' .
                                $data->surat_klaim_approved_by .
                                '|',
                        ) !!}
                    </div>
                @else
                    <br><br><br><br><br>
                    <span style="margin-left: 15px">[NOT YET APPROVED]</span>
                @endif

                <div style="margin: 20px 0 0 0px">{{ $data->approver_name }}</div>
            </div>
        </div>
        <div style="position: absolute; bottom: 200px;">
            <span>Tembusan :</span><br>
            <span>Pengadaan Barang dan Jasa PT SILOG</span><br>
            <span>Akuntansi PT SILOG</span>


        </div>
    </div>
</body>

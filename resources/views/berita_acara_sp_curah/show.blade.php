{{-- {{dd($data)}} --}}

@extends('layouts.main')

@push('styles')
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dataTablesCustom.css') }}" rel="stylesheet">
    {{-- <link href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
@endpush

@section('container')
    <div id="module-pages" class="container-fluid">
        <div class="row">
            <!-- column -->
            <div class="col-lg-12">
                <div class="card">
                    <h2 class="card-title col-lg-12"><strong>Berita Acara {{ $data->no_berita_acara }}</strong></h2>

                    @include('partials.alert')

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <table class="table color-bordered-table default-bordered-table">
                                    <tr>
                                        <td width="20%">No Berita Acara</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->no_berita_acara }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Nama Perusahaan</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->nama_supplier }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Customer</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->jenis_pelanggan }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">No Kontrak Perjanjian</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->no_kontrak_perjanjian }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Status Barang</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->status_barang }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Nama Distributor</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->nama_distributor }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Tipe Semen</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->tipe_semen }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Tujuan Bongkar</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->tujuan_bongkar }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Approve Status</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->approve_status == 1 ? 'APPROVED' : ($data->approve_status == -1 ? 'REJECTED' : 'WAITING') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Approved At</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->approved_at }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">No PP</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->no_pp }}</td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Sdh SAP</td>
                                        <td width="5%" align="center">:</td>
                                        <td>{{ $data->is_inputted_sap == 1 ? 'SUDAH' : 'BELUM' }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-12">
                                <h3>SPJ</h3>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <table class="table color-bordered-table default-bordered-table table-striped"
                                        id="spj_table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NO POLISI</th>
                                                <th>NO DO</th>
                                                <th>DO OPCO</th>
                                                <th>NO SPJ</th>
                                                <th>TGL SPJ</th>
                                                <!-- <th>TGL CHECK POINT</th>
                                                <th>TGL TERIMA</th> -->
                                                <!-- <th>WAKTU TEMPUH</th>
                                                <th>LEAD TIME</th>
                                                <th>KETERLAMBATAN</th>
                                                <th>QTY 40</th>
                                                <th>QTY 50</th> -->
                                                <th>QTY TONASE</th>
                                                <th>KLAIM KANTONG</th>
                                                <th>KLAIM SEMEN</th>
                                                <th>STATUS</th>
                                                <th>SELECT</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach (collect($data->spj)->sortBy('id_berita_acara_spj') as $spj)
                                                <tr>
                                                    <td>{{ $spj->id_berita_acara_spj }}</td>
                                                    <td>{{ $spj->no_polisi }}</td>
                                                    <td>{{ $spj->no_do }}</td>
                                                    <td>{{ $spj->do_opco }}</td>
                                                    <td>{{ $spj->no_spj }}</td>
                                                    <td>{{ $spj->tgl_spj }}</td>
                                                    <!-- <td>{{ $spj->tgl_check_point }}</td>
                                                    <td>{{ $spj->tgl_terima }}</td> -->
                                                    <!-- <td>{{ $spj->waktu_tempuh }}</td>
                                                    <td>{{ $spj->lead_time }}</td>
                                                    <td>{{ $spj->keterlambatan }}</td>
                                                    <td>{{ $spj->qty_40 }}</td>
                                                    <td>{{ $spj->qty_50 }}</td> -->
                                                    <td>{{ $spj->qty_tonase }}</td>
                                                    <td>{{ $spj->klaim_kantong }}</td>
                                                    <td>{{ $spj->klaim_semen }}</td>
                                                    <td>{{ $spj->approve_status == 1 ? 'APPROVED' : ($spj->approve_status == -1 ? 'REJECTED' : 'WAITING') }}
                                                    </td>
                                                    <td class=" select-checkbox"></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    {{-- buttons --}}
                                    <div class="btn-group">
                                        @if ($data->approve_status == 0 && session()->get('user')->role == 'administrator')
                                            <form id="approve_form" action="{{ url('/berita-acara-sp-curah-approve') }}"
                                                method="post">
                                                @csrf
                                                <input type="hidden" class="form-control" id="data" name="data">
                                                <input type="hidden" class="form-control" id="id_berita_acara"
                                                    name="id_berita_acara" value="{{ $data->id_berita_acara }}">
                                                <button type="button" id="approve_btn"
                                                    class="btn btn-outline-primary">Approve
                                                    SPJ</button>
                                                <button type="button" id="reject_btn"
                                                    class="btn btn-outline-primary">Reject
                                                    SPJ</button>

                                            </form>
                                        @endif

                                        @if ($data->approve_status == 1 && session()->get('user')->role == 'administrator' && empty($data->no_pp))
                                            <form id="generate_no_pp_form"
                                                action="{{ url('/berita-acara-sp-curah-generate-no-pp') }}"method="post">
                                                @csrf
                                                <input type="hidden" class="form-control" id="id_berita_acara"
                                                    name="id_berita_acara" value="{{ $data->id_berita_acara }}">
                                                <button type="button" id="generate_no_pp"
                                                    class="btn btn-outline-primary">Generate No PP</button>
                                            </form>
                                        @endif

                                        @if (
                                            $data->approve_status == 1 &&
                                                session()->get('user')->role == 'administrator' &&
                                                !empty($data->no_pp) &&
                                                $data->is_inputted_sap != 1)
                                            <form id="set_inputted_sap_form"
                                                action="{{ url('/berita-acara-sp-curah-set-inputted-sap') }}" method="post">
                                                @csrf
                                                <input type="hidden" class="form-control" id="id_berita_acara"
                                                    name="id_berita_acara" value="{{ $data->id_berita_acara }}">
                                                <button type="button" id="set_inputted_sap"
                                                    class="btn btn-outline-primary">Set
                                                    Terinput SAP</button>
                                            </form>
                                        @endif

                                        @if (
                                            $data->approve_status == 1 &&
                                                session()->get('user')->role == 'administrator' &&
                                                empty($data->no_surat_klaim) &&
                                                $data->status_barang == 'KLAIM')
                                            <form id="generate_surat_klaim_form"
                                                action="{{ url('/berita-acara-sp-curah-generate-surat-klaim') }}" method="post">
                                                @csrf
                                                <input type="hidden" class="form-control" id="id_berita_acara"
                                                    name="id_berita_acara" value="{{ $data->id_berita_acara }}">
                                                <input type="hidden" class="form-control" id="surat_klaim_harga_kantong"
                                                    name="surat_klaim_harga_kantong">
                                                <input type="hidden" class="form-control" id="surat_klaim_harga_semen"
                                                    name="surat_klaim_harga_semen">
                                                <input type="hidden" class="form-control" id="surat_klaim_keterangan"
                                                    name="surat_klaim_keterangan">
                                                <button type="button" id="generate_surat_klaim"
                                                    class="btn btn-outline-primary">Generate Surat Klaim</button>
                                            </form>
                                        @endif



                                        <a href="{{ url('/berita-acara-sp-curah-print', $data->id_berita_acara) }}" id="print_btn"
                                            class="btn btn-outline-primary">Print</a>
                                        <a href="{{ url('/berita-acara-sp-curah-excel-spj', $data->id_berita_acara) }}"
                                            id="excel_spj_btn" class="btn btn-outline-primary">Excel SPJ</a>

                                        @if (!empty($data->no_surat_klaim))
                                            <a href="{{ url('/berita-acara-sp-curah-print-surat-klaim', $data->id_berita_acara) }}"
                                                id="print_surat_klaim_btn" class="btn btn-outline-primary">Surat Klaim
                                                @if ($data->surat_klaim_approve_status != 1)
                                                    (Blm Approve)
                                                @endif
                                            </a>
                                        @endif
                                        @if (
                                            $data->approve_status == 1 &&
                                                !empty($data->no_surat_klaim) &&
                                                $data->surat_klaim_approve_status == 0 &&
                                                session()->get('user')->role == 'administrator' &&
                                                (session()->get('user')->is_ka_billing = 1))
                                            <button type="button" id="approve_klaim_btn"
                                                class="btn btn-outline-primary">Approve
                                                Surat Klaim</button>
                                            <button type="button" id="reject_klaim_btn"
                                                class="btn btn-outline-primary">Reject
                                                Surat Klaim</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTablesPlugins/dataTables.select.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script> --}}
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>


    <script>
        $(document).ready(function() {
            @if ($data->approve_status == 0 && session()->get('user')->role == 'administrator')
                $('#spj_table th').each(function() {
                    if (this.textContent == "SELECT") {
                        $(this).html(
                            '<input type="checkbox" id="head_checkbox" class="filled-in chk-col-cyan">' +
                            '<label for="head_checkbox"/>');
                    }
                });
            @endif

            var table = $('#spj_table').DataTable({
                dom: 't',
                "responsive": true,
                "scrollX": true,
                "paging": false,
                columnDefs: [{
                        "targets": [0],
                        "visible": false
                    },
                    {
                        "targets": [10],
                        "data": null,
                        "sortable": false,
                        "className": 'select-checkbox',
                        "render": function(data, type, full, meta) {
                            return ""
                        },
                    }
                ],
                @if ($data->approve_status == 0 && session()->get('user')->role == 'administrator')
                    "select": {
                        "style": "multi",
                    },
                @endif
            });

            $("#head_checkbox").on("click", function(event) {
                if ($("th.select-checkbox").hasClass("selected")) {
                    table.rows().deselect();
                    $("th.select-checkbox").removeClass("selected");
                } else {
                    table.rows().select();
                    $("th.select-checkbox").addClass("selected");
                }
                event.stopImmediatePropagation();
            });


            $("#approve_btn").on("click", function() {
                event.preventDefault();
                var ids = [];
                var selected = table.rows({
                    selected: true
                }).data().toArray();
                selected.forEach(function(item) {
                    ids.push(item[0]);
                });

                if (ids.length <= 0) {
                    Swal.fire("Tidak ada data yang dipilih");
                    return;
                }

                Swal.fire({
                    title: "Konfirmasi",
                    text: "Approve " + ids.length + " data & Reject lainnya ? ",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Batal",
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#data").val(JSON.stringify(ids));
                        $("#approve_form").submit();
                    }
                });
            });

            $("#reject_btn").on("click", function() {
                event.preventDefault();
                Swal.fire({
                    title: "Konfirmasi",
                    text: "Reject seluruh Berita Acara ini ?",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Batal",
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.replace(
                            '{{ url('/berita-acara-sp-curah-reject', $data->id_berita_acara) }}');
                    }
                });
            });

            $("#generate_no_pp").on("click", function() {
                event.preventDefault();
                Swal.fire({
                    title: "Konfirmasi",
                    text: "Konfirmasi Generate ?",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Batal",
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#generate_no_pp_form").submit();
                    }
                });
            });

            $("#set_inputted_sap").on("click", function() {
                event.preventDefault();
                Swal.fire({
                    title: "Konfirmasi",
                    text: "Konfirmasi Set Terinput SAP ?",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Batal",
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#set_inputted_sap_form").submit();
                    }

                });
            });

            $("#generate_surat_klaim").on("click", function() {
                event.preventDefault();
                Swal.fire({
                    title: 'Generate Surat Klaim',
                    confirmButtonText: 'OK',
                    html: '<div style="text-align: left;">' +
                        '<H2>Harga per kantong</H2>' +
                        '<input id="surat_klaim_harga_kantong_modal" class="form-control" type="number">' +
                        '<H2>Harga per semen</H2>' +
                        '<input id="surat_klaim_harga_semen_modal" class="form-control" type="number">' +
                        '<H2>Keterangan</H2>' +
                        '<textarea id="surat_klaim_keterangan_modal" class="form-control" rows="5"></textarea>' +
                        '</div>',
                    preConfirm: function() {
                        return new Promise(function(resolve) {
                            resolve([
                                $('#surat_klaim_harga_kantong_modal').val(),
                                $('#surat_klaim_harga_semen_modal').val(),
                                $('#surat_klaim_keterangan_modal').val()
                            ])
                        })
                    },
                    onOpen: function() {
                        $('#surat_klaim_harga_kantong_modal').focus()
                    }
                }).then((result) => {
                    if (result.value) {

                        $('#surat_klaim_harga_kantong').val(result.value[0]);
                        $('#surat_klaim_harga_semen').val(result.value[1]);
                        $('#surat_klaim_keterangan').val(result.value[2]);

                        $("#generate_surat_klaim_form").submit();

                    } else {
                        return null;
                    }
                })

            });

            $("#approve_klaim_btn").on("click", function() {
                event.preventDefault();
                Swal.fire({
                    title: "Konfirmasi",
                    text: "Approve Surat Klaim ?",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Batal",
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.replace(
                            '{{ url('/berita-acara-sp-curah-approve-surat-klaim', $data->id_berita_acara) }}'
                        );
                    };
                });
            });

            $("#reject_klaim_btn").on("click", function() {
                event.preventDefault();
                Swal.fire({
                    title: "Konfirmasi",
                    text: "Reject Surat Klaim ?",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya",
                    cancelButtonText: "Batal",
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.replace(
                            '{{ url('/berita-acara-sp-curah-reject-surat-klaim', $data->id_berita_acara) }}'
                        );
                    }
                });
            });


        });
    </script>
@endpush

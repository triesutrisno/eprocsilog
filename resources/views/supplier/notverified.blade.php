@extends('layouts.main')

@push('styles')
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!--
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Beranda</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
            </ol>
        </div>
    </div>
    -->
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Verifikasi Rekanan / Vendor</strong></h4>
                <div class="card-body">
                    <form action="{{ url('/postnotverified') }}" method="post">
                        @csrf                        
                        <input type="hidden" name="email" value="{{ $email }}" readonly="true">
                        <input type="text" class="form-control" id="pesanKegagalan" name="pesanKegagalan" value="{{old('pesanKegagalan')}}"> 
                        <div class="form-group text-center m-t-20">
                            <div  class="col-lg-2 col-md-2">
                                <button class="btn btn-block btn-success" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
@endpush
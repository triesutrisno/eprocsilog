@extends('layouts.main')

@push('styles')
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!--
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Beranda</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
            </ol>
        </div>
    </div>
    -->
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Detail Rekanan / Vendor</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/daftarvendor') }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                </div>
                
                <div class="card-body">
                    <!--<div class="col-lg-8 col-xlg-9 col-md-7">-->
                        <!--<div class="card">-->
                            <!-- Nav tabs -->                            
                            <form class="form-horizontal form-material">
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#dataperusahaan" role="tab">Data Perusahaan</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pemimpin" role="tab">Data Penanggung Jawab</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pemilik" role="tab">Data Pemilik</a> </li>
                            </ul>
                            <br />
                            @if($model[0]['supStatus']=='2')
                            <div class="row button-group">
                                <div class="col-lg-3 col-md-4">
                                    <a href="{{ url('/verified') }}/{{ $model[0]['supEmail'] }}" class="btn btn-block btn-success">Lolos Verifikasi</a> 
                                </div>
                                <div class="col-lg-3 col-md-4">
                                    <a href="{{ url('/notverified') }}/{{ $model[0]['supEmail'] }}" class="btn btn-block btn-danger">Tidak Lolos Verifikasi</a>
                                </div>
                            </div>
                            @endif
                            @if($pesan!="")
                                <div class="text-danger">
                                    {{ $pesan }}
                                </div>
                            @endif
                            @if($pesan2!="")
                                <div class="text-danger">
                                    {{ $pesan2 }}
                                </div>
                            @endif
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="dataperusahaan" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row">
                                            <table class="table color-bordered-table default-bordered-table">
                                                <tr>
                                                    <td width="20%">Nama Perusahaan</td>
                                                    <td width="5%" align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['supNama'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Website Perusahaan</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['supWebsite'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['supEmail'] }}</td>
                                                </tr>  
                                                <tr>
                                                    <td>EmailVerifikasi</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['supEmailStatus']=='1' ? 'Sudah Verifikasi': 'Belum Verifikasi' }}</td>
                                                </tr>  
                                                <tr>
                                                    <td>No Telepon</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['supTlp'] }}</td>
                                                </tr> 
                                                <tr>
                                                    <td>Propinsi</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['propinsi']['0']['mpropNama'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kabupaten / Kota</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['kabupaten']['0']['mkabNama'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kecamatan</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['kecamatan']['0']['mkecNama'] }}</td>
                                                </tr>                                                
                                                <tr>
                                                    <td>Alamat</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['supAlamat'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kategori</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['kategori']['0']['mkatNama'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Klasifikasi</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['kualifikasi']['0']['mkualNama'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>SIUP</td>
                                                    <td align="center">:</td>
                                                    <td width="40%">{{ $model[0]['supSIUP'] }}</td>
                                                    <td>
                                                        @if($model[0]['supSIUPFile']!="")
                                                            <a href="{{ asset('../storage/'.$model[0]['supSIUPFile']) }}" target="_blank()">Lampiran</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>NPWP</td>
                                                    <td align="center">:</td>
                                                    <td>{{ $model[0]['supNPWP'] }}</td>
                                                    <td>
                                                        @if($model[0]['supNPWPFile']!="")
                                                            <a href="{{ asset('../storage/'.$model[0]['supNPWPFile']) }}" target="_blank()">Lampiran</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>NPPKP</td>
                                                    <td align="center">:</td>
                                                    <td>{{ $model[0]['supNPPKP'] }}</td>
                                                    <td>
                                                        @if($model[0]['supNPPKPFile']!="")
                                                             <a href="{{ asset('../storage/'.$model[0]['supNPPKPFile']) }}" target="_blank()">Lampiran</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>TDP</td>
                                                    <td align="center">:</td>
                                                    <td>{{ $model[0]['supTDP'] }}</td>
                                                    <td>
                                                        @if($model[0]['supTDPFile']!="")
                                                            <a href="{{ asset('../storage/'.$model[0]['supTDPFile']) }}" target="_blank()">Lampiran</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Akata Notaris</td>
                                                    <td align="center">:</td>
                                                    <td colspan="2">{{ $model[0]['supAkteNoteris'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Surat Keterangan Domisili</td>
                                                    <td align="center">:</td>
                                                    <td colspan="2">{{ $model[0]['supDomisili']=='1' ? 'Ada' : 'Tidak Ada' }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Bank 1</td>
                                                    <td align="center">:</td>
                                                    <td>
                                                        {{ $model[0]['supBank1'] }}
                                                        &nbsp;&nbsp;&nbsp;
                                                        @if($model[0]['supBank1File']!="")
                                                             <a href="{{ asset('../storage/'.$model[0]['supBank1File']) }}" target="_blank()">Lampiran</a>
                                                        @endif
                                                    </td>
                                                    <td>No Rekening : {{ $model[0]['supNorek1'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Bank 2</td>
                                                    <td align="center">:</td>
                                                    <td>
                                                        {{ $model[0]['supBank2'] }}
                                                        &nbsp;&nbsp;&nbsp;
                                                        @if($model[0]['supBank2File']!="")
                                                             <a href="{{ asset('../storage/'.$model[0]['supBank2File']) }}" target="_blank()">Lampiran</a>
                                                        @endif
                                                    </td>
                                                    <td>No Rekening : {{ $model[0]['supNorek2'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td align="center">:</td>
                                                    <td>
                                                        @if($model[0]['supStatus']=='3')
                                                            Lolos Verifikasi
                                                        @elseif($model[0]['supStatus']=='2')
                                                            Sudah Verifikasi Email
                                                        @elseif($model[0]['supStatus']=='1')
                                                            Belum Verifikasi Email
                                                        @else
                                                            Tidak Lolos Verifikasi
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--second tab-->
                                <div class="tab-pane" id="pemimpin" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row">
                                            <table class="table color-bordered-table default-bordered-table">
                                                <tr>
                                                    <td width="20%">Nama Pemimpin</td>
                                                    <td width="5%" align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['pemimpinNama'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jabatan</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['pemimpinJabatan'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email Pemimpin</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['pemimpinEmail'] }}</td>
                                                </tr>  
                                                <tr>
                                                    <td>No. Telepon</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['pemimpinTlp'] }}</td>
                                                </tr> 
                                                <tr>
                                                    <td>No. Handphone</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['pemimpinHp'] }}</td>
                                                </tr> 
                                                <tr>
                                                    <td>No. KTP</td>
                                                    <td align="center">:</td>
                                                    <td width="40%">{{ $model[0]['pemimpinKTP'] }}</td>
                                                    <td>
                                                        @if($model[0]['pemimpinKTPFile']!="")
                                                            <a href="{{ asset('../storage/'.$model[0]['pemimpinKTPFile']) }}" target="_blank()">Lampiran</a>
                                                        @endif
                                                    </td>
                                                </tr>                  
                                                <tr>
                                                    <td>Alamat Rumah</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['pemimpinAlamat'] }}</td>
                                                </tr>                  
                                                <tr>
                                                    <td>Karyawan Silog Group</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pemilik" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row">
                                            <table class="table color-bordered-table default-bordered-table">
                                                <tr>
                                                    <td width="20%">Nama Pemilik</td>
                                                    <td width="5%" align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['PemilikNama'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jabatan</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['PemilikJabatan'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email Pemilik</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['PemilikEmail'] }}</td>
                                                </tr>  
                                                <tr>
                                                    <td>No. Telepon</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['PemilikTlp'] }}</td>
                                                </tr> 
                                                <tr>
                                                    <td>No. Handphone</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['PemilikHp'] }}</td>
                                                </tr> 
                                                <tr>
                                                    <td>No. KTP</td>
                                                    <td align="center">:</td>
                                                    <td width="40%">{{ $model[0]['PemilikKTP'] }}</td>
                                                    <td>
                                                        @if($model[0]['PemilikKTPFile']!="")
                                                            <a href="{{ asset('../storage/'.$model[0]['PemilikKTPFile']) }}" target="_blank()">Lampiran</a>
                                                        @endif
                                                    </td>
                                                </tr>                  
                                                <tr>
                                                    <td>Alamat Rumah</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>{{ $model[0]['PemilikAlamat'] }}</td>
                                                </tr>                  
                                                <tr>
                                                    <td>Karyawan Silog Group</td>
                                                    <td align="center">:</td>
                                                    <td colspan='2'>
                                                        @if(isset($model[0]['pegawai']['0']['NAMA']))
                                                            {{ $model[0]['pegawai']['0']['NAMA'] }}
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        <!--</div>-->
                    <!--</div>-->
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
@endpush
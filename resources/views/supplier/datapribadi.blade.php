@extends('layouts.main')

@push('styles')
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />  
    <link href="{{ asset('assets/plugins/wizard/steps.css') }}" rel="stylesheet">
    <!--alerts CSS -->
    <link href="{{ asset('assets/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" >
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!--
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Beranda</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
            </ol>
        </div>
    </div>
    -->
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body wizard-content">
                    <h4 class="card-title"><strong>Pendaftaran Rekanan / Vendor</strong></h4>    
                    @if(isset($pesan2))
                        <div class="text-danger">
                            {{ $pesan2 }}
                        </div>
                    @endif      
                    <br />
                    <form method="post" action="{{ url('/datapribadi') }}/{{ session('user')->username }}/update" id="formID" class="validation-wizard wizard-circle" enctype="multipart/form-data">
                        @csrf
                        <!-- Step 1 -->
                        <h6>Data Perusahaan</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="namaPerusahaan"> Nama Perusahaan : <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="namaPerusahaan" name="namaPerusahaan" value="{{ $model[0]['supNama'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="websitePerusahaan"> Website :  </label>
                                        <input type="url" class="form-control" id="websitePerusahaan" name="websitePerusahaan" value="{{ $model[0]['supWebsite'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="emailPerusahaan"> Email Perusahaan : <span class="danger">*</span> </label>
                                        {{ $model[0]['supEmail'] }}
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="noTlp">No. Telepon Perusahaan: <span class="danger">*</span> </label>
                                        <input type="tel" class="form-control required" id="noTlp" name="noTlp" value="{{ $model[0]['supTlp'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="propinsiPerusahaan"> Propinsi : <span class="danger">*</span> </label>
                                        <select name="propinsiPerusahaan" id="propinsiPerusahaan" class="custom-select form-control required select2 dinamic" data-dependent="kotaperusahaan">
                                            <option value="">Silakan Pilih</option>
                                            @foreach($dtProp as $val)
                                                <option value="{{ $val['mpropId'] }}" {{ $val['mpropId']==$model[0]['propinsi'][0]['mpropId'] ? 'selected' : '' }}>{{ $val['mpropNama'] }}</option>
                                            }
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="kotaPerusahaan"> Kota / Kabupaten Perusahaan : <span class="danger">*</span> </label>
                                        <select name="kotaPerusahaan" id="kotaperusahaan" class="custom-select form-control required select2 dinamic" data-dependent="kecamatanperusahaan">
                                            <option value="{{ $model[0]['kabupaten'][0]['mkabId'] }}">{{ $model[0]['kabupaten'][0]['mkabNama'] }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="kecamatanPerusahaan"> Kecamatan Perusahaan : <span class="danger">*</span> </label>
                                        <select name="kecamatanPerusahaan" id="kecamatanperusahaan" class="custom-select form-control required select2">
                                            <option value="{{ $model[0]['kecamatan'][0]['mkecId'] }}">{{ $model[0]['kecamatan'][0]['mkecNama'] }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="alamatPerusahaan">Alamat Perusahaan : <span class="danger">*</span></label>
                                        <textarea name="alamatPerusahaan" id="alamatPerusahaan" rows="6" class="form-control required">{{ $model[0]['supAlamat'] }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="kategoriPerusahaan">Kategori : <span class="danger">*</span> </label>
                                        <select name="kategoriPerusahaan" id="kategoriPerusahaan" class="custom-select form-control required select2">
                                            <option value="">Silakan Pilih</option>
                                            @foreach($dtKat as $val2)
                                                <option value="{{ $val2['mkatId'] }}" {{ $val2['mkatId'] == $model[0]['mkatId'] ? 'selected' : '' }}>{{ $val2['mkatNama'] }}</option>
                                            }
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="kualifikasiPerusahaan">Kualifikasi :  </label>
                                        <select class="custom-select form-control required select2" id="kualifikasiPerusahaan" name="kualifikasiPerusahaan">
                                            <option value="">Silakan Pilih</option>
                                            @foreach($dtKual as $val3)
                                                <option value="{{ $val3['mkualId'] }}" {{ $val3['mkualId']==$model[0]['mkualId'] ? 'selected' : '' }}>{{ $val3['mkualNama'] }}</option>
                                            }
                                            @endforeach
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="siupPerusahaan"> Nomor Surat Ijin Usaha Perdagangan (SIUP) : <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="siupPerusahaan" name="siupPerusahaan" value="{{ $model[0]['supSIUP'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranSiup">Lampiran Siup : <span class="danger">*</span></label> 
                                        <a href="{{ asset('../storage/'.$model[0]['supSIUPFile']) }}" target="_blank()" class="danger">[ Lihat ]</a>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="lampiranSiup" name="lampiranSiup"> 
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="npwpPerusahaan"> Nomor Pokok Wajib Pajak (NPWP) : <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="npwpPerusahaan" name="npwpPerusahaan" value="{{ $model[0]['supNPWP'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranNPWP">Lampiran NPWP : <span class="danger">*</span></label>
                                        <a href="{{ asset('../storage/'.$model[0]['supNPWPFile']) }}" target="_blank()" class="danger">[ Lihat ]</a>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="lampiranNPWP" name="lampiranNPWP"> 
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="nppkpPerusahaan"> Nomor Pengukuhan Pengusaha Kena Pajak (NPPKP) : </label>
                                        <input type="text" class="form-control" id="nppkpPerusahaan" name="nppkpPerusahaan" value="{{ $model[0]['supNPPKP'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranNPPKP">Lampiran NPPKP : </label>
                                        <a href="{{ asset('../storage/'.$model[0]['supNPPKPFile']) }}" target="_blank()" class="danger">[ Lihat ]</a>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="lampiranNPPKP" name="lampiranNPPKP"> 
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="tdpPerusahaan"> Nomor Tanda Daftar Perusahaan (TDP) : </label>
                                        <input type="text" class="form-control" id="tdpPerusahaan" name="tdpPerusahaan" value="{{ $model[0]['supTDP'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranTDP">Lampiran TDP : </label>
                                        <a href="{{ asset('../storage/'.$model[0]['supTDPFile']) }}" target="_blank()" class="danger">[ Lihat ]</a>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="lampiranTDP" name="lampiranTDP"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="aktaNotarisPerusahaan">Akta Notaris : </label>
                                        <input type="text" class="form-control" id="aktaNotarisPerusahaan" name="aktaNotarisPerusahaan" value="{{ $model[0]['supAkteNoteris'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="skdPerusahaan">Surat Keterangan Domisili : <span class="danger">*</span> </label>
                                        <select class="custom-select form-control required" id="skdPerusahaan" name="skdPerusahaan">
                                            <option value="">Silakan Pilih</option>
                                            <option value="1" {{ $model[0]['supDomisili']=='1' ? 'selected' : '' }}>Ada</option>
                                            <option value="2" {{ $model[0]['supDomisili']=='2' ? 'selected' : '' }}>Tidak Ada</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nmBank1">Nama Bank 1 : <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="nmBank1" name="nmBank1" value="{{ $model[0]['supBank1'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="norekBank1">No Rekening 1 : <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="norekBank1" name="norekBank1" value="{{ $model[0]['supNorek1'] }}"> 
                                    </div>
                                </div>                                                         
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranRek1">Lampiran Rekening 1 : <span class="danger">*</span></label>
                                        <a href="{{ asset('../storage/'.$model[0]['supBank1File']) }}" target="_blank()" class="danger">[ Lihat ]</a>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="supBank1File" name="supBank1File"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nmBank2">Nama Bank 2 :  </label>
                                        <input type="text" class="form-control" id="nmBank2" name="nmBank2" value="{{ $model[0]['supBank2'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="norekBank2">No Rekening 2 : </label>
                                        <input type="text" class="form-control" id="norekBank2" name="norekBank2" value="{{ $model[0]['supNorek2'] }}"> 
                                    </div>
                                </div>                             
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lampiranRek2">Lampiran Rekening 2 : </label>
                                        <a href="{{ asset('../storage/'.$model[0]['supBank2File']) }}" target="_blank()" class="danger">[ Lihat ]</a>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" data-allowed-file-extensions="pdf" id="supBank2File" name="supBank2File"> 
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 2 -->
                        <h6>Data Penanggung Jawab Perusahaan</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="pemipinPerusahaan">Nama Pemimpin : <span class="danger">*</span></label>
                                        <input type="text" class="form-control required" id="pemipinPerusahaan" name="pemipinPerusahaan" value="{{ $model[0]['pemimpinNama'] }}">
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="jabatanPemimpin">Jabatan : <span class="danger">*</span></label>
                                        <input type="text" class="form-control required" id="jabatanPemimpin" name="jabatanPemimpin" value="{{ $model[0]['pemimpinJabatan'] }}"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="emailPemimpin"> Email : <span class="danger">*</span> </label>
                                        <input type="email" class="form-control required" id="emailPemimpin" name="emailPemimpin" value="{{ $model[0]['pemimpinEmail'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="noTlpPemimpin">No. Telepon : </label>
                                        <input type="tel" class="form-control" id="noTlpPemimpin" name="noTlpPemimpin" value="{{ $model[0]['pemimpinTlp'] }}"> 
                                    </div>
                                </div>                                
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="noHpPemimpin">No. Handphone : <span class="danger">*</span> </label>
                                        <input type="tel" class="form-control required" id="noHpPemimpin" name="noHpPemimpin" value="{{ $model[0]['pemimpinHp'] }}"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="alamatPemimpin">Alamat Rumah : <span class="danger">*</span></label>
                                        <textarea name="alamatPemimpin" id="alamatPemimpin" rows="6" class="form-control required">{{ $model[0]['pemimpinAlamat'] }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="ktpPemimpin"> Nomor KTP Pemimpin: <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="ktpPemimpin" name="ktpPemimpin" value="{{ $model[0]['pemimpinKTP'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pemimpinKTPFile">Lampiran KTP Pemimpin :</label>
                                        <a href="{{ asset('../storage/'.$model[0]['pemimpinKTPFile']) }}" target="_blank()" class="danger">[ Lihat ]</a>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" id="pemimpinKTPFile" name="pemimpinKTPFile"> 
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 3 -->
                        <h6>Data Pemilik Perusahaan</h6>                        
                        <section>
                            <div class="row">
                                <div class="col-md-10">
                                    <p>Apabila pemilik perusahaan lebih dari satu, sebutkan salah satu identitas yang mempunyai modal dominan, sedangkan yang lainnya diisi dalam lebar tersendiri</p>
                                    <br />
                                </div>                                
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="pemilikPerusahaan">Nama Pemilik : <span class="danger">*</span></label>
                                        <input type="text" class="form-control required" id="pemilikPerusahaan" name="pemilikPerusahaan" value="{{ $model[0]['PemilikNama'] }}">
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="jabatanPemilik">Jabatan : <span class="danger">*</span></label>
                                        <input type="text" class="form-control required" id="jabatanPemilik" name="jabatanPemilik" value="{{ $model[0]['PemilikJabatan'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="emailPemilik"> Email : <span class="danger">*</span> </label>
                                        <input type="email" class="form-control required" id="emailPemilik" name="emailPemilik" value="{{ $model[0]['PemilikEmail'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="noTlpPemilik">No. Telepon : </label>
                                        <input type="tel" class="form-control" id="noTlpPemilik" name="noTlpPemilik" value="{{ $model[0]['PemilikTlp'] }}"> 
                                    </div>
                                </div>                                
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="noHpPemilik">No. Handphone : <span class="danger">*</span> </label>
                                        <input type="tel" class="form-control required" id="noHpPemilik" name="noHpPemilik" value="{{ $model[0]['PemilikHp'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="alamatPemilik">Alamat Rumah : <span class="danger">*</span></label>
                                        <textarea name="alamatPemilik" id="alamatPemilik" rows="6" class="form-control required">{{ $model[0]['PemilikAlamat'] }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="ktpPemilik"> Nomor KTP Pemilik: <span class="danger">*</span> </label>
                                        <input type="text" class="form-control required" id="ktpPemilik" name="ktpPemilik" value="{{ $model[0]['PemilikKTP'] }}"> 
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="PemilikKTPFile">Lampiran KTP Pemilik :</label>
                                        <a href="{{ asset('../storage/'.$model[0]['PemilikKTPFile']) }}" target="_blank()" class="danger">[ Lihat ]</a>
                                        <input type="file" class="dropify" data-height="25" data-max-file-size="1M" id="PemilikKTPFile" name="PemilikKTPFile"> 
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 4 -->
                        <h6>Lain - Lain</h6>
                        <section>
                            <div class="row">
                                <div class="col-md-10">
                                <br />
                                    <ul class="search-listing">
                                        <li>
                                            <h3>KETENTUAN DAN PERSYARATAN YANG BERLAKU</h3>
                                            <p>
                                               Calon supplier/pemasok dengan ini menyatakan tanpa paksaan maupun tekanan dari pihak manapun untuk tunduk,
                                               patuh dan terikat terhadap segala ketentuan-ketentuan maupun kebijakan-kebijakan dan/atau sanksi-sanksi yang
                                               berlaku pada PT Semen Indonesia Logistik yang telah ada maupun akan dan/atau mungkin timbul dikemudian hari.
                                            </p>
                                        </li>
                                        <li>
                                            <h3>TEMPAT KEDUDUKAN HUKUM</h3>
                                            <p>
                                                Apabila terjadi perselisihan dengan kedua belah pihak, maka kedua belah pihak akan menyelesaikan dan menentukan tempat
                                                kedudukan hukum yang tepat di Kantor Panitera Pengadilan Tinggi Gresik
                                            </p>
                                        </li>
                                        <li>
                                            <h3>PERNYATAAN</h3>
                                            <p>
                                                Dengan ini saya calon supplier/pemasok menyatakan dengan sebenar-benarnya, bahwa informasi diatas adalah sesuai
                                                dengan kondisi yang ada. Dan apabila saya memberikan keterangan yang tidak sesuai dengan kenyataan yang ada, maka
                                                saya bersedia dituntut sesuai hukum yang berlaku.
                                                <br />
                                                Saya dengan ini memberikan kuasa pada PT Semen Indonesia Logistik untuk menghubungi pada pihak manapun guna memperoleh
                                                dan memverifikasi keterangan yang diperlukan.
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password"> Password *: <span class="danger"></span> </label>
                                        <input type="password" class="form-control required" id="password" name="password"> 
                                    </div>
                                </div>
                                <!--
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="repassword"> Repassword: <span class="danger">*</span> </label>
                                        <input type="password" class="form-control required" id="repassword" name="repassword">   
                                    </div>
                                </div>
                                -->
                                <div class="col-md-10">                                    
                                    <div class="form-group">
                                        <label>Unit :</label>
                                            <div class="c-inputs-stacked">
                                                @foreach($dtUnit as $vUnit)
                                                <label class="inline custom-control custom-checkbox block">
                                                    <input type="checkbox" name="unit[{{ $vUnit['unitId'] }}]" class="custom-control-input"><span class="custom-control-label ml-0">{{$vUnit['unit']}}</span>
                                                </label>                                                
                                                @endforeach
                                            </div>
                                    </div>                                    
                                </div>
                                <div class="col-md-10">
                                    <div class="c-inputs-stacked">
                                        <label class="inline custom-control custom-checkbox block">
                                            <input type="checkbox" name="setuju" class="custom-control-input required"> <span class="custom-control-label ml-0">Setuju *</span>
                                        </label>                                        
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
    <script src="{{ asset('assets/plugins/wizard/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/wizard/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/wizard/steps.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <!--<script src="{{ asset('js/jquery-latest.js') }}"></script>
    <script src="{{ asset('js/jquery.chained.js') }}"></script>-->

    <script>
    
     $(document).ready(function() {
         $(".dinamic").change(function(){
             //alert('Alhamdulillah');
             if($(this).val() != ''){
                 var select = $(this).attr('id');
                 var value = $(this).val();
                 var dependent = $(this).data('dependent');
                 var _token = $('input[name="_token"]').val();
                 $.ajax({
                     url : "{{ url('/') }}/"+dependent,
                     method : "POST",
                     data : {select:select, value:value, _token:_token, dependent:dependent},
                     success : function(result){
                         //alert(result);
                         $('#'+dependent).html(result);
                     }
                 })
             }
         });
         $(".select2").select2();
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
@endpush
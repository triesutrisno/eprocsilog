@extends('layouts.main')

@push('styles')
@endpush

@section('container')

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!--
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Beranda</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
            </ol>
        </div>
    </div>
    -->
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <h4 class="card-title"><strong>Daftar Rekanan / Vendor</strong></h4>
                <div class="row button-group">
                    <div class="col-lg-2 col-md-4">
                        <a href="{{ url('/daftarvendor') }}" class="btn btn-block btn-info">Lihat Data</a> 
                    </div>
                </div>
                
                @if($pesan!="")
                    {{ $pesan }}
                @else
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="tbSupplier" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th align="center">No</th>
                                    <th>Aksi</th>
                                    <th>Status</th>
                                    <th>Nama Supplier</th>
                                    <th>SIUP</th>
                                    <th>NPWP</th>
                                    <th>Kategori</th>
                                    <th>Pemilik</th>
                                    <th>Kota</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th align="center">No</th>
                                    <th>Aksi</th>
                                    <th>Status</th>
                                    <th>Nama Supplier</th>
                                    <th>SIUP</th>
                                    <th>NPWP</th>
                                    <th>Kategori</th>
                                    <th>Pemilik</th>
                                    <th>Kota</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($model as $dt)                                    
                                    <tr>
                                        <td align="center">{{$loop->iteration}}</td>
                                        <td align="center">
                                            <div class="hidden-sm hidden-xs action-buttons">                                                                                                
                                                <a class="green" href="{{ url('/detailsupplier')}}/{{ $dt['supId'] }}" title="Detail"><i class="ace-icon fa fa-search bigger-130"></i></a>
                                            </div> 
                                        </td>
                                        <td>
                                            @if($dt['supStatus']=='3')
                                                Lolos Verifikasi
                                            @elseif($dt['supStatus']=='2')
                                                Sudah Verifikasi Email
                                            @elseif($dt['supStatus']=='1')
                                                Belum Verifikasi Email
                                            @else
                                                Tidak Lolos Verifikasi
                                            @endif
                                        </td>
                                        <td>{{ $dt['supNama'] }}</td>
                                        <td>{{ $dt['supSIUP'] }}</td>
                                        <td>{{ $dt['supNPWP'] }}</td>
                                        <td>{{ $dt['kategori'][0]['mkatNama'] }}</td>
                                        <td>{{ $dt['PemilikNama'] }}</td>
                                        <td>{{ $dt['kabupaten'][0]['mkabNama'] }}</td>
                                    </tr>  
                                @endforeach                                                   
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@push('scripts')
<!-- This is data table -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script>
    $('#tbSupplier').DataTable();
</script>
@endpush
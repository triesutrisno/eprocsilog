<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->

    <title>Berita Acara Penyerahan Dokumen Surat Jalan</title>
    <style>
        html,
        body {
            height: 100%;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        body {
            margin: 0px;
            padding: 0px;
        }

        table {
            width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
        }

        th {
            padding: 3px;
            vertical-align: top;
            border: 1px solid black;
            background: gainsboro;

        }

        td {
            padding: 3px;
            vertical-align: top;
            border: 1px solid black;

        }

        @page {
            size: 'a4';
            margin: 5mm 5mm 5mm 5mm;
            /* change the margins as you want them to be. */
        }

        pre {
            font-family: 'Times New Roman', Times, serif;
        }

        .center {
            text-align: center;
        }

        .nama-ttd {
            margin-top: 50px;
            text-decoration: underline;
            font-weight: bold;
        }

        .box-bottom {
            margin-top: 20px;
            margin-left: 5px;
        }

        .box-bottom-item {
            display: inline-block;
            margin-left: 20px;
            width: 30%;
        }

        .box-bottom-text {
            display: inline-block;
            vertical-align: super;
            height: 30px;
        }

        .box-bottom-box {
            width: 30px;
            height: 15px;
            outline: 2px solid black;
            display: inline-block;
            margin-left: 10px
        }

        .head-row {
            width: 10%;
            font-weight: bold;
        }
    </style>
</head>

<body onload="window.print()">
    <div style="padding: 20px; border: 1px solid black;">
        <table class="table color-bordered-table default-bordered-table">
            <tr>
                <td colspan="2" align="center"><span>
                        <h2>BERITA ACARA PENYERAHAN DOKUMEN SURAT JALAN<br>{{ $data->nama_supplier }}</h2>
                    </span></td>
            </tr>
            <tr>
                <td class="head-row">No Berita Acara</td>
                <td>{{ $data->no_berita_acara }}</td>
            </tr>
            <tr>
                <td class="head-row">Tgl Berita Acara</td>
                <td>{{ \Carbon\Carbon::parse($data->created_at)->format('d-m-Y') }}</td>
            </tr>
            <tr>
                <td class="head-row">Tgl Periode SPJ</td>
                <td>{{ $data->periode_spj_awal . ' - ' . $data->periode_spj_akhir }}</td>
            </tr>
            <tr>
                <td class="head-row">Customer</td>
                <td>{{ $data->jenis_pelanggan }}</td>
            </tr>
            <tr>
                <td class="head-row">Nama Distributor</td>
                <td>{{ $data->nama_distributor }}</td>
            </tr>
            <tr>
                <td class="head-row">Tipe Semen</td>
                <td>{{ $data->tipe_semen }}</td>
            </tr>
            <tr>
                <td class="head-row">Tujuan Bongkar</td>
                <td>{{ $data->tujuan_bongkar }}</td>
            </tr>
            <tr>
                <td class="head-row">Jenis Plat Nomor</td>
                <td>{{ $data->jenis_plat }}</td>
            </tr>
            <tr>
                <td class="head-row">Status Barang</td>
                <td>{{ $data->status_barang }}</td>
            </tr>
            <tr>
                <td class="head-row">Asal Muatan</td>
                <td>{{ $data->plant }}</td>
            </tr>
            <tr>
                <td class="head-row">Approve Status</td>
                <td>{{ $data->approve_status == 1 ? 'APPROVED' : ($data->approve_status == -1 ? 'REJECTED' : 'WAITING') }}
                </td>
            </tr>
            <tr>
                <td class="head-row">Approved At</td>
                <td>{{ $data->approved_at }}</td>
            </tr>
            <tr>
                <td class="head-row">No PP EPROC</td>
                <td>{{ $data->no_pp }}</td>
            </tr>
        </table>

        {{-- spj list --}}

        <table class="table" style="margin-top:20px; text-align:center">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>NO POLISI</th>
                    <th>NO DO</th>
                    <th>DO OPCO</th>
                    <th>NO SPJ</th>
                    <th>TGL SPJ</th>
                    <th>TGL CHECK POINT</th>
                    <th>TGL TERIMA</th>
                    <th>WAKTU TEMPUH</th>
                    <th>LEAD TIME</th>
                    <th>KETERLAMBATAN</th>
                    <th>QTY 40</th>
                    <th>QTY 50</th>
                    <th>QTY TONASE</th>
                    <th>KLAIM KANTONG</th>
                    <th>KLAIM SEMEN</th>
                </tr>
            </thead>
            <tbody>
                @php $total_qty_tonase = 0; @endphp
                @foreach (collect($data->spj)->where('approve_status', '!=', -1)->sortBy('id_berita_acara_spj') as $spj)
                @php $total_qty_tonase += $spj->qty_tonase; @endphp
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $spj->no_polisi }}</td>
                    <td>{{ $spj->no_do }}</td>
                    <td>{{ $spj->do_opco }}</td>
                    <td>{{ $spj->no_spj }}</td>
                    <td>{{ \Carbon\Carbon::parse($spj->tgl_spj)->format('d/m/Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($spj->tgl_check_point)->format('d/m/Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($spj->tgl_terima)->format('d/m/Y') }}</td>
                    <td>{{ $spj->waktu_tempuh }}</td>
                    <td>{{ $spj->lead_time }}</td>
                    <td>{{ $spj->keterlambatan }}</td>
                    <td>{{ $spj->qty_40 }}</td>
                    <td>{{ $spj->qty_50 }}</td>
                    <td>{{ $spj->qty_tonase }}</td>
                    <td>{{ $spj->klaim_kantong }}</td>
                    <td>{{ $spj->klaim_semen }}</td>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="13" style="font-weight: bold;">Total Tonase</td>
                    <td style="font-weight: bold;">{{ $total_qty_tonase }}</td>
                    <td colspan="2"></td>
                </tr>
            </tfoot>
        </table>

        {{-- signature --}}

        {{-- Signature DISTRIBUTOR --}}
        <div style="width: 100%; margin-top: 20px; margin-left: 10px; display: inline-flex">
            <div style="display: inline-block; width: 40%">
                <div>Distributor Semen Indonesia Logistik</div>
                <div>{{$data->nama_distributor}}</div>
                <br>
                @if ($data->approve_status == 1)
                <div style="height: 90px; margin-left:20px">
                    {!! QrCode::size(90)->generate(
                    'APPROVAL BERITA ACARA SPJ |' .
                    $data->id_berita_acara .
                    '|' .
                    $data->no_berita_acara .
                    '|' .
                    $data->created_at .
                    '|' .
                    $data->nama_distributor .
                    '|' .
                    $data->approve_status .
                    '|' .
                    $data->approved_at .
                    '|' .
                    $data->approved_by .
                    '|',
                    ) !!}
                </div>
                @else
                <br><br><br><br><br<
                <span style="margin-left: 15px"></span>
                @endif

                <!-- distributor name ?? -->
                <!-- <div style="margin: 20px 0 0 10px">{{ $data->approver_name }}</div> -->
            </div>

            {{-- Signature SILOG --}}
            <div style="display: inline-block; width: 40%">
                <div>PT SEMEN INDONESIA LOGISTIK</div>
                <br><br>
                @if ($data->approve_status == 1)
                <div style="height: 90px; margin-left:20px">
                    {!! QrCode::size(90)->generate(
                    'APPROVAL BERITA ACARA SPJ |' .
                    $data->id_berita_acara .
                    '|' .
                    $data->no_berita_acara .
                    '|' .
                    $data->created_at .
                    '|' .
                    $data->approver_name .
                    '|' .
                    $data->approve_status .
                    '|' .
                    $data->approved_at .
                    '|' .
                    $data->approved_by .
                    '|',
                    ) !!}
                </div>
                @else
                <br><br><br><br><br>
                <span style="margin-left: 15px"></span>
                @endif

                <div style="margin: 20px 0 0 10px">{{ $data->approver_name }}</div>
            </div>

            {{-- Signature Vendor --}}
            <div style="display: inline-block; width: 40%; text-align:center">
                <div>{{ $data->nama_supplier }}</div>
                <br><br>
                @if ($data->approve_status == 1)
                <div style="height: 90px;">
                    {!! QrCode::size(90)->generate(
                    'APPROVAL BERITA ACARA SPJ |' .
                    $data->id_berita_acara .
                    '|' .
                    $data->no_berita_acara .
                    '|' .
                    $data->created_at .
                    '|' .
                    $data->nama_supplier .
                    '|' .
                    $data->approve_status .
                    '|' .
                    $data->approved_at .
                    '|' .
                    $data->nama_pic_supplier .
                    '|',
                    ) !!}
                </div>
                @else
                <br><br><br><br><br>
                <span style="margin-left: 15px"></span>
                @endif

                <div style="margin-top: 20px;">{{ $data->nama_pic_supplier }}</div>
            </div>
        </div>
</body>